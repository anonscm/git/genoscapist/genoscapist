# Genoscapist

Genoscapist is a web-tool generating high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations.

# Installation

## Requirements

* [Java 8](https://www.java.com/fr/download/)
* [GWT 2.8.2](http://www.gwtproject.org/download.html)
* [GWT-Ext](http://gwt-ext.com/download/)
* [GWT Servlet](https://mvnrepository.com/artifact/com.google.gwt/gwt-servlet)
* [lib-gwt-svg library (SVG graphics)](https://www.vectomatic.org/libs/lib-gwt-svg)
* [JDBC driver](https://jdbc.postgresql.org/download.html)

## Quick start

We recommend that you install [Eclipse](https://www.eclipse.org/) and import the project from the [Git repository](https://sourcesup.renater.fr/scm/?group_id=5016) along with all of the above dependencies.

# Authors

* Sandra Dérozier, MaIAGE, INRAE, Université Paris-Saclay, 78352, Jouy-en-Josas, France
* Cyprien Guérin, MaIAGE, INRAE, Université Paris-Saclay, 78352, Jouy-en-Josas, France
* Pierre Nicolas, MaIAGE, INRAE, Université Paris-Saclay, 78352, Jouy-en-Josas, France
* Ulrike Mäder, Interfaculty Institute for Genetics and Functional Genomics, University Medicine Greifswald, Greifswald, Germany

# License

The source code is placed under the [CeCILL-B licence](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) and is available on a [Git repository](https://sourcesup.renater.fr/scm/?group_id=5016).

# Issues, Questions, Remarks

If you have any question or issue with installing, using or understanding Genoscapist, please do not hesitate to send an email to [genoscapist@inrae.fr](mailto:genoscapist@inrae.fr).