/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.inrae.jouy.client.Config;
import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.client.RPCall.CallForBDInfo;
import fr.inrae.jouy.shared.Axis;
import fr.inrae.jouy.shared.Cluster;
import fr.inrae.jouy.shared.DeletedRegion;
import fr.inrae.jouy.shared.Experience;
import fr.inrae.jouy.shared.Expression;
import fr.inrae.jouy.shared.Feature;
import fr.inrae.jouy.shared.GeneItem;
import fr.inrae.jouy.shared.Promoter;
import fr.inrae.jouy.shared.Region;
import fr.inrae.jouy.shared.RegionUpRho;
import fr.inrae.jouy.shared.Regulator;
import fr.inrae.jouy.shared.Segment;
import fr.inrae.jouy.shared.Sequence;
import fr.inrae.jouy.shared.Species;
import fr.inrae.jouy.shared.Terminator;
import fr.inrae.jouy.shared.Transterm;

public class CallForBDInfoImpl extends RemoteServiceServlet implements CallForBDInfo {

	private static final long serialVersionUID = 1L;

	private static final String DB_URL = DatabaseConfBD.CONF_DB_URL;
	private static final String DRIVER = DatabaseConfBD.CONF_DRIVER;
	private static final String USERNAME = DatabaseConfBD.CONF_USERNAME;
	private static final String PASSWORD = DatabaseConfBD.CONF_PASSWORD;
	private static final String SCHEMA = DatabaseConfBD.CONF_SCHEMA;

	/** Get Sequence Length **/
	public int getSeqLength() {
		
		int length = -1;
		
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT length FROM " + SCHEMA + ".sequences";
				
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				length = rs.getInt("length");
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getSeqLength");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getSeqLength rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getSeqLength statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getSeqLength conn.close : " + ex + "\n" + ex.getMessage());
			}
		}
		
		return length;
	}
	
	/** Get Type of Feature **/
	public String getTypeFeature(final String feat) {

		String type = new String();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT type" + " FROM " + SCHEMA + ".features" + " WHERE id_feat = '" + feat + "'";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				type = rs.getString("type");
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getTypeFeature");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getTypeFeature rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getTypeFeature statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getTypeFeature conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return type;
	}

	/** Get Gene Feature **/
	public GeneItem getGeneItem(String typeFeat, String geneName) {

		final GeneItem gene = new GeneItem();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		String command = "";

		if (typeFeat.contains("segment")) {

			command = "SELECT f.id, f.id_feat, f.start, f.stop, f.complement, f.clustera, f.clusterb, f.clusterc, q.value "
					+ " FROM " + SCHEMA + ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.id = q.feature_id"
					+ " AND q.value ='" + geneName + "'" + " AND f.type in ('CDS','TSV')" + " AND q.type = 'name'"
					+ " ORDER BY f.start";
		} else {

			command = "SELECT f.id, f.id_feat, f.start, f.stop, f.complement, f.clustera, f.clusterb, f.clusterc, q.value "
					+ " FROM " + SCHEMA + ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.id = q.feature_id"
					+ " AND f.id_feat ='" + geneName + "'" + " AND f.type in ('CDS','TSV')" + " AND q.type = 'name'"
					+ " ORDER BY f.start";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			rs.next();

			int id = rs.getInt("id");
			gene.setGeneId(id);
			gene.setLocusTag(rs.getString("id_feat"));
			gene.setName(rs.getString("value"));
			gene.setStart(rs.getInt("start"));
			gene.setEnd(rs.getInt("stop"));
			gene.setStrand(rs.getInt("complement"));
			gene.setClusterA(rs.getString("clustera"));
			gene.setClusterB(rs.getString("clusterb"));
			gene.setClusterC(rs.getString("clusterc"));

		} catch (Exception ex) {
		
			System.err.println("problem in getGeneItem()");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getGeneItem() rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getGeneItem() statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getGeneItem() conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return gene;
	}

	/** Get Axis List **/
	public List<Axis> getAxis(Sequence s, long from, long to) {

		final List<Axis> axisList = new ArrayList<Axis>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT id, complement, color, x1, y1, x2, y2" + " FROM " + SCHEMA + ".axis"
					+ " WHERE sequence_id = '" + s.getId() + "'" + " AND x1 <= " + to + " AND x2 >= " + from
					+ " ORDER BY x1";
		}

		else {

			command = "SELECT a.id, a.complement, a.color, a.x1, a.y1, a.x2, a.y2" + " FROM " + SCHEMA + ".axis a, "
					+ SCHEMA + ".sequences s" + " WHERE s.id = a.sequence_id" + " AND s.name = '" + s.getName() + "'"
					+ " AND x1 <= " + to + " AND x2 >= " + from + " ORDER BY x1";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Axis axis = new Axis();

				axis.setGeneId(rs.getInt("id"));
				axis.setStrand(rs.getInt("complement"));
				axis.setColor(rs.getString("color"));
				axis.setX1(rs.getDouble("x1"));
				axis.setY1(rs.getDouble("y1"));
				axis.setX2(rs.getDouble("x2"));
				axis.setY2(rs.getDouble("y2"));

				axisList.add(axis);
			}

		} catch (Exception ex) {
		
			System.err.println("problem in getAxis");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getAxis rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getAxis statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getAxis conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return axisList;
	}

	/** Get Promoters List **/
	public List<Promoter> getPromoter(Sequence s, long from, long to) {

		final List<Promoter> promoterList = new ArrayList<Promoter>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.sequence_id = '" + s.getId() + "'"
					+ " AND f.id = q.feature_id" + " AND f.type = 'promoter'" + " AND q.type = 'center'"
					+ " AND f.stop >= " + from + " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".sequences s, " + SCHEMA + ".qualifiers q" + " WHERE s.name = '"
					+ s.getName() + "'" + " AND f.sequence_id = s.id AND f.id = q.feature_id"
					+ " AND f.type = 'promoter' AND q.type = 'center'" + " AND f.stop >= " + from + " AND f.start <= "
					+ to + " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Promoter promoter = new Promoter();

				promoter.setName(rs.getString("id_feat"));
				promoter.setStart(rs.getInt("start"));
				promoter.setEnd(rs.getInt("stop"));
				promoter.setStrand(rs.getInt("complement"));
				promoter.setColor(rs.getString("color"));
				promoter.setCenter(rs.getInt("value"));

				promoterList.add(promoter);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getPromoter");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getPromoter rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getPromoter statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getPromoter conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return promoterList;
	}

	/** Get Terminators List **/
	public List<Terminator> getTerminator(Sequence s, long from, long to) {

		final List<Terminator> terminatorList = new ArrayList<Terminator>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.sequence_id = '" + s.getId() + "'"
					+ " AND f.id = q.feature_id" + " AND f.type = 'terminator'" + " AND q.type = 'center'"
					+ " AND f.stop >= " + from + " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".sequences s, " + SCHEMA + ".qualifiers q" + " WHERE s.name = '"
					+ s.getName() + "'" + " AND f.sequence_id = s.id AND f.id = q.feature_id"
					+ " AND f.type = 'terminator' AND q.type = 'center'" + " AND f.stop >= " + from + " AND f.start <= "
					+ to + " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Terminator terminator = new Terminator();

				terminator.setName(rs.getString("id_feat"));
				terminator.setStart(rs.getInt("start"));
				terminator.setEnd(rs.getInt("stop"));
				terminator.setStrand(rs.getInt("complement"));
				terminator.setColor(rs.getString("color"));
				terminator.setCenter(rs.getInt("value"));

				terminatorList.add(terminator);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getTerminator");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getTerminator rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getTerminator statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getTerminator conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return terminatorList;
	}

	/** Get Transterm List **/
	public List<Transterm> getTransterm(Sequence s, long from, long to) {

		final List<Transterm> transtermList = new ArrayList<Transterm>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.sequence_id = '" + s.getId() + "'"
					+ " AND f.id = q.feature_id" + " AND f.type = 'transtermHP'" + " AND q.type = 'thickness'"
					+ " AND f.stop >= " + from + " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}
		
		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".sequences s, " + SCHEMA + ".qualifiers q" + " WHERE s.name = '"
					+ s.getName() + "'" + " AND f.sequence_id = s.id AND f.id = q.feature_id"
					+ " AND f.type = 'transtermHP' AND q.type = 'thickness'" + " AND f.stop >= " + from
					+ " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Transterm transterm = new Transterm();

				transterm.setName(rs.getString("id_feat"));
				transterm.setStart(rs.getInt("start"));
				transterm.setEnd(rs.getInt("stop"));
				transterm.setStrand(rs.getInt("complement"));
				transterm.setColor(rs.getString("color"));
				transterm.setThickness(rs.getDouble("value"));

				transtermList.add(transterm);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getTransterm");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getTransterm rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getTransterm statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getTransterm conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return transtermList;
	}

	/** Get Regulators List **/
	public List<Regulator> getRegulator(Sequence s, long from, long to) {

		final List<Regulator> regulatorList = new ArrayList<Regulator>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.sequence_id = '" + s.getId() + "'"
					+ " AND f.id = q.feature_id" + " AND f.type = 'Regulator'" + " AND q.type = 'thickness'"
					+ " AND f.stop >= " + from + " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".sequences s, " + SCHEMA + ".qualifiers q" + " WHERE s.name = '"
					+ s.getName() + "'" + " AND f.sequence_id = s.id AND f.id = q.feature_id"
					+ " AND f.type = 'Regulator' AND q.type = 'thickness'" + " AND f.stop >= " + from
					+ " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Regulator regulator = new Regulator();

				regulator.setName(rs.getString("id_feat"));
				regulator.setStart(rs.getInt("start"));
				regulator.setEnd(rs.getInt("stop"));
				regulator.setStrand(rs.getInt("complement"));
				regulator.setColor(rs.getString("color"));
				regulator.setThickness(rs.getDouble("value"));

				regulatorList.add(regulator);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getRegulator");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegulator rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegulator statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegulator conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return regulatorList;
	}

	/** Get Segments List **/
	public List<Segment> getSegment(Sequence s, long from, long to) {

		final List<Segment> segmentList = new ArrayList<Segment>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.sequence_id = '" + s.getId() + "'"
					+ " AND f.id = q.feature_id" + " AND f.type = 'segment'" + " AND q.type = 'filled'"
					+ " AND f.stop >= " + from + " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, q.value" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".sequences s, " + SCHEMA + ".qualifiers q" + " WHERE s.name = '"
					+ s.getName() + "'" + " AND f.sequence_id = s.id AND f.id = q.feature_id"
					+ " AND f.type = 'segment' AND q.type = 'filled'" + " AND f.stop >= " + from + " AND f.start <= "
					+ to + " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Segment segment = new Segment();

				segment.setName(rs.getString("id_feat"));
				segment.setStart(rs.getInt("start"));
				segment.setEnd(rs.getInt("stop"));
				segment.setStrand(rs.getInt("complement"));
				segment.setColor(rs.getString("color"));
				segment.setFilled(rs.getString("value"));
				
				segmentList.add(segment);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getSegment");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getSegment rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getSegment statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getSegment conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return segmentList;
	}

	/** Get RegionUpRho List **/
	public List<RegionUpRho> getRegionUpRho(Sequence s, long from, long to) {

		final List<RegionUpRho> regionList = new ArrayList<RegionUpRho>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color" + " FROM " + SCHEMA
					+ ".features f" + " WHERE f.sequence_id = '" + s.getId() + "'" + " AND f.type = 'rho_up_region'"
					+ " AND f.stop >= " + from + " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".sequences s" + " WHERE s.name = '" + s.getName() + "'"
					+ " AND f.sequence_id = s.id" + " AND f.type = 'rho_up_region'" + " AND f.stop >= " + from
					+ " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				RegionUpRho region = new RegionUpRho();

				region.setName(rs.getString("id_feat"));
				region.setStart(rs.getInt("start"));
				region.setEnd(rs.getInt("stop"));
				region.setStrand(rs.getInt("complement"));
				region.setColor(rs.getString("color"));

				regionList.add(region);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getRegionUpRho");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegionUpRho rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegionUpRho statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegionUpRho conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return regionList;
	}

	/** Get Features List **/
	public List<Feature> getFeatures(Sequence s, String type, long from, long to) {

		final List<Feature> featList = new ArrayList<Feature>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT id, start, stop, complement, id_feat, color, location, type" + " FROM " + SCHEMA
					+ ".features" + " WHERE sequence_id = '" + s.getId() + "'" + " AND type = '" + type + "'"
					+ " AND stop >= " + from + " AND start <= " + to + "ORDER BY ID, START";
		}

		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color, f.location, f.type" + " FROM "
					+ SCHEMA + ".features f, " + SCHEMA + ".sequences s" + " WHERE s.name = '" + s.getName() + "'"
					+ " AND f.sequence_id = s.id" + " AND type = '" + type + "'" + " AND stop >= " + from
					+ " AND start <= " + to + "ORDER BY ID, START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				long start = rs.getLong("start");
				long stop = rs.getLong("stop");
				String typeFeat = rs.getString("type");

				Feature feat = new Feature();
				feat.setId(rs.getInt("id"));
				feat.setStart(start);
				feat.setStop(stop);
				feat.setSens(rs.getShort("complement"));
				feat.setId_feat(rs.getString("id_feat"));
				feat.setColor(rs.getInt("color"));
				feat.setLocation(rs.getString("location"));
				feat.setType(typeFeat);
				feat.setSequence(s);

				featList.add(feat);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getFeatures");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getFeatures rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getFeatures statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getFeatures conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return featList;
	}

	/** Get Qualifiers Informations **/
	public String getQualifiersInformation(GeneItem gene) {

		String txt = "";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		String command = "SELECT q.type, q.value" + " FROM " + SCHEMA + ".features f, " + SCHEMA + ".qualifiers q"
				+ " WHERE f.id = q.feature_id" + " AND f.id_feat = '" + gene.getLocusTag() + "'"
				+ " AND q.type = 'Seed Annotation'";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = statement.executeQuery(command);

			if (rs.next() != false) {

				rs.beforeFirst();

				while (rs.next()) {
					
					String value = rs.getString("value").replaceAll("\"", "");
					
					txt += "<h3>Additional informations</h3>";
					
					if ( value.contains("NA") == false ) {
					
						txt += "<b><i>" + rs.getString("type") + "</i></b> : " 
						    + value
							+ "<br/>";
					}
					
					if ( txt.endsWith("</h3>") == false ) {
						
						txt += "<br/>";
					}
				}
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getQualifiersInformation");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getQualifiersInformation rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println(
						"problem in getQualifiersInformation() statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getQualifiersInformation() conn.close : " + ex + "\n" + ex.getMessage());
			}
		}
		return txt;
	}

	/** Get GenBank Qualifiers **/
	public HashMap<String, String> getQualifiersGenbank(GeneItem gene) {

		HashMap<String, String> hashQual = new HashMap<String, String>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		String command = "SELECT q.type, q.value" + " FROM " + SCHEMA + ".features f, " + SCHEMA + ".qualifiers q"
				+ " WHERE f.id = q.feature_id" + " AND f.id_feat = '" + gene.getLocusTag() + "'"
				+ " AND q.type != 'name' AND q.type != 'Seed Annotation'";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				hashQual.put(rs.getString("type"), rs.getString("value"));
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getQualifiersGenbank()");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getQualifiersGenbank() rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err
						.println("problem in getQualifiersGenbank() statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getQualifiersGenbank() conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return hashQual;
	}

	/** Get Genes List **/
	public ArrayList<GeneItem> getAllGeneItem() {

		final ArrayList<GeneItem> geneList = new ArrayList<GeneItem>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		String command = "SELECT f.id, f.id_feat, f.start, f.stop, f.complement, f.clustera, f.clusterb, f.clusterc, q.value "
				+ " FROM " + SCHEMA + ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.id = q.feature_id"
				+ " AND f.type in ('CDS','TSV')" + " AND q.type = 'name'" + " ORDER BY f.start";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				GeneItem gene = new GeneItem();

				int id = rs.getInt("id");
				gene.setGeneId(id);
				gene.setLocusTag(rs.getString("id_feat"));
				gene.setName(rs.getString("value"));
				gene.setStart(rs.getInt("start"));
				gene.setEnd(rs.getInt("stop"));
				gene.setStrand(rs.getInt("complement"));
				gene.setClusterA(rs.getString("clustera"));
				gene.setClusterB(rs.getString("clusterb"));
				gene.setClusterC(rs.getString("clusterc"));

				geneList.add(gene);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getAllGeneItem()");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllGeneItem() rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllGeneItem() statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllGeneItem() conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return geneList;
	}

	/** Get Clusters list **/
	public ArrayList<Cluster> getClusters() {

		final ArrayList<Cluster> clusterList = new ArrayList<Cluster>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT id, name " + " FROM " + SCHEMA + ".cluster" + " ORDER BY name";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Cluster cluster = new Cluster();

				cluster.setId(rs.getInt("id"));
				cluster.setName(rs.getString("name"));

				clusterList.add(cluster);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getClusters");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getClusters rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getClusters statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getClusters conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return clusterList;
	}

	/** Get Genes List for a Cluster **/
	public ArrayList<GeneItem> getGenesWithCluster(Cluster cluster) {

		final ArrayList<GeneItem> geneList = new ArrayList<GeneItem>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		
		String command = "SELECT f.id, f.id_feat, f.start, f.stop, f.complement, q.value, f.clustera, f.clusterb, f.clusterc " + " FROM " + SCHEMA
				+ ".cluster c," + SCHEMA + ".features f, " + SCHEMA + ".feat_cluster fc, " + SCHEMA + ".qualifiers q"
				+ " WHERE f.id = fc.feature_id AND fc.cluster_id = c.id AND f.id = q.feature_id" + " AND c.name = '"
				+ cluster.getName() + "'" + " AND q.type = 'name'" + " ORDER BY f.start";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				GeneItem gene = new GeneItem();

				int id = rs.getInt("id");
				gene.setGeneId(id);
				gene.setLocusTag(rs.getString("id_feat"));
				gene.setName(rs.getString("value"));
				gene.setStart(rs.getInt("start"));
				gene.setEnd(rs.getInt("stop"));
				gene.setStrand(rs.getInt("complement"));
				gene.setClusterA(rs.getString("clustera"));
				gene.setClusterB(rs.getString("clusterb"));
				gene.setClusterC(rs.getString("clusterc"));

				geneList.add(gene);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getGenesWithCluster");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getGenesWithCluster rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getGenesWithCluster statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getGenesWithCluster conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return geneList;
	}

	/** Get Genes List for Positions **/
	public ArrayList<GeneItem> getGenesWithPosition(String position) {

		final ArrayList<GeneItem> geneList = new ArrayList<GeneItem>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		String command = "SELECT f.id, f.id_feat, f.start, f.stop, f.complement, q.value, f.clustera, f.clusterb, f.clusterc" + " FROM " + SCHEMA
				+ ".features f, " + SCHEMA + ".qualifiers q" + " WHERE (f.id_feat IN" + " (SELECT id_feat FROM "
				+ SCHEMA + ".features WHERE type IN ('CDS', 'TSV') AND start > " + position + " ORDER BY stop limit 1)"
				+ " OR f.id_feat IN" + " (SELECT id_feat FROM " + SCHEMA
				+ ".features WHERE type IN ('CDS', 'TSV') AND start <= " + position + "AND stop >= " + position + ")"
				+ " OR f.id_feat IN" + " (SELECT id_feat FROM " + SCHEMA
				+ ".features WHERE type IN ('CDS', 'TSV') AND stop < " + position + " ORDER BY stop DESC limit 1))"
				+ " AND f.id = q.feature_id" + " AND q.type = 'name'" + " ORDER BY f.start";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				GeneItem gene = new GeneItem();

				int id = rs.getInt("id");
				gene.setGeneId(id);
				gene.setLocusTag(rs.getString("id_feat"));
				gene.setName(rs.getString("value"));
				gene.setStart(rs.getInt("start"));
				gene.setEnd(rs.getInt("stop"));
				gene.setStrand(rs.getInt("complement"));
				
				gene.setClusterA(rs.getString("clustera"));
				gene.setClusterB(rs.getString("clusterb"));
				gene.setClusterC(rs.getString("clusterc"));

				geneList.add(gene);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getGenesWithPosition");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getGenesWithPosition rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getGenesWithPosition statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getGenesWithPosition conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return geneList;
	}

	/** Get Expression **/
	public Expression getExpression(Experience e, int start, int stop, int SeqSize) {

		final Expression exp = new Expression();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
				
		if ( start > 100 )					{ start -= 100; }
		else								{ start = 1; }
		if ( stop <= ( SeqSize - 100 ) )	{ stop += 100; }
		else								{ stop = SeqSize; }
		
		String command = "SELECT position, signal" + " FROM " + SCHEMA + ".expression" + " WHERE exp_seq_id = "
				+ e.getId() + " AND position >= " + start + " AND position <= " + stop + " ORDER BY position";
				
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = statement.executeQuery(command);

			rs.last();
			int row = rs.getRow();
			rs.beforeFirst();

			int[] position = new int[row];
			double[] signal = new double[row];
			int i = -1;

			while (rs.next()) {

				i += 1;

				int pos = rs.getInt("position");
				double sig = rs.getDouble("signal");

				position[i] = pos;
				signal[i] = sig;
			}

			exp.setPosition(position);
			exp.setSignal(signal);
			exp.setSens(e.getStrand());

		} catch (Exception ex) {
			
			System.err.println("problem in getExpression");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getExpression rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getExpression statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getExpression conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return exp;
	}

	/** Get Accessions List for a Specie **/
	public List<Sequence> getAccessions(Species sp) {

		final List<Sequence> seqList = new ArrayList<Sequence>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (sp.getId() > 0) {

			command = "SELECT id, name, length" + " FROM " + SCHEMA + ".sequences" + " WHERE species_id = '"
					+ sp.getId() + "'";
		}
		else {

			command = "SELECT s.id, s.name, s.length" + " FROM " + SCHEMA + ".sequences s, " + SCHEMA + ".species sp"
					+ " WHERE s.species_id = sp.id" + " AND sp.name = '" + sp.getName() + "'";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Sequence seq = new Sequence();

				seq.setId(rs.getInt("id"));
				seq.setName(rs.getString("name"));
				seq.setLength(rs.getInt("length"));

				seqList.add(seq);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getAccessions");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getAccessions rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getAccessions statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getAccessions conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return seqList;
	}

	/** Get Specie **/
	public Species getSpecie(String name) {

		final Species specie = new Species();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT id, name " + " FROM " + SCHEMA + ".species";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			rs.next();

			specie.setId(rs.getInt("id"));
			specie.setName(rs.getString("name"));

		} catch (Exception ex) {
			
			System.err.println("problem in getTypeFeatures");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getTypeFeatures rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getTypeFeatures statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getTypeFeatures conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return specie;
	}

	/** Get Experience **/
	public Experience getExperience(String name, int strand, String normalisation) {

		final Experience exp = new Experience();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";
		
		command = "SELECT es.id, es.sequence_id, es.chip_id, es.experience, es.project, es.normalization, es.strand, es.color, es.info, ei.description" + " FROM "
				+ SCHEMA + ".exp_seq es, " + SCHEMA + ".exp_info ei" + " WHERE es.exp_info_id = ei.id AND es.strand = '" + strand + "'" + " AND es.normalization = '" + normalisation
				+ "'" + " AND es.project != 'Rho'" + " AND es.experience = '" + name + "'";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				exp.setId(rs.getInt("id"));
				exp.setSeq(rs.getInt("sequence_id"));
				exp.setChip(rs.getString("chip_id"));
				exp.setExp(rs.getString("experience"));
				exp.setProjet(rs.getString("project"));
				exp.setNormalization(rs.getString("normalization"));
				exp.setStrand(rs.getInt("strand"));
				exp.setColor(rs.getString("color"));
				exp.setColorSelect(exp.getColor());
				exp.setInfo(rs.getInt("info"));
				exp.setDescription(rs.getString("description"));
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getExperience");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getExperience rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getExperience statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getExperience conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return exp;
	}

	/** Get Experiences List **/
	public List<Experience> getAllExperience(String normalization) {

		final List<Experience> expList = new ArrayList<Experience>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT es.id, es.sequence_id, es.chip_id, es.experience, es.project, es.normalization, es.strand, es.color, es.info , ei.description" + " FROM "
				+ SCHEMA + ".exp_seq es, " + SCHEMA + ".exp_info ei " + " WHERE es.exp_info_id = ei.id AND es.strand = 1 AND es.normalization = '" + normalization
				+ "' AND es.project != 'Rho' order by es.strand DESC";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Experience exp = new Experience();

				exp.setId(rs.getInt("id"));
				exp.setSeq(rs.getInt("sequence_id"));
				exp.setChip(rs.getString("chip_id"));
				exp.setExp(rs.getString("experience"));
				exp.setProjet(rs.getString("project"));
				exp.setNormalization(rs.getString("normalization"));
				exp.setStrand(rs.getInt("strand"));
				exp.setColor(rs.getString("color"));
				exp.setColorSelect(exp.getColor());
				exp.setInfo(rs.getInt("info"));
				exp.setDescription(rs.getString("description"));

				expList.add(exp);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getAllExperience");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return expList;
	}

	/** Get Default Experiences List **/
	public List<Experience> getAllExperienceDefault() {

		final List<Experience> expList = new ArrayList<Experience>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT es.id, es.sequence_id, es.chip_id, es.experience, es.project, es.normalization, es.strand, ei.description" + " FROM " + SCHEMA
				+ ".exp_seq es, " + SCHEMA + ".exp_info ei" + " WHERE es.exp_info_id = ei.id AND es.info = 1 AND es.normalization = 'CustomCDS' order by es.strand DESC";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Experience exp = new Experience();

				exp.setId(rs.getInt("id"));
				exp.setSeq(rs.getInt("sequence_id"));
				exp.setChip(rs.getString("chip_id"));
				exp.setExp(rs.getString("experience"));
				exp.setProjet(rs.getString("project"));
				exp.setNormalization(rs.getString("normalization"));
				exp.setStrand(rs.getInt("strand"));
				exp.setDescription(rs.getString("description"));

				expList.add(exp);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getAllExperience");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return expList;
	}

	/** Get Default Experiences List with Normalization **/
	public List<Experience> getAllDefaultExperience(String normalization) {

		final List<Experience> expList = new ArrayList<Experience>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT es.id, es.sequence_id, es.chip_id, es.experience, es.project, es.normalization, es.strand, es.color, ei.description" + " FROM "
				+ SCHEMA + ".exp_seq es, " + SCHEMA + ".exp_info ei " + " WHERE es.exp_info_id = ei.id AND es.info = 1 AND es.normalization = '" + normalization
				+ "' order by strand DESC";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Experience exp = new Experience();

				exp.setId(rs.getInt("id"));
				exp.setSeq(rs.getInt("sequence_id"));
				exp.setChip(rs.getString("chip_id"));
				exp.setExp(rs.getString("experience"));
				exp.setProjet(rs.getString("project"));
				exp.setNormalization(rs.getString("normalization"));
				exp.setStrand(rs.getInt("strand"));
				exp.setColor(rs.getString("color"));
				exp.setColorSelect(rs.getString("color"));
				exp.setDescription(rs.getString("description"));

				expList.add(exp);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getAllDefaultExperience");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllDefaultExperience rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err
						.println("problem in getAllDefaultExperience statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllDefaultExperience conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		Genoscapist.appControler.setListAllDefaultExperience(expList);

		return expList;
	}

	/** Get Rho Experiences List **/
	public List<Experience> getAllExperienceRho(String normalization) {

		final List<Experience> expList = new ArrayList<Experience>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "SELECT es.id, es.sequence_id, es.chip_id, es.experience, es.project, es.normalization, es.strand" + " FROM " + SCHEMA
				+ ".exp_seq es" + " WHERE es.project = 'Rho' AND es.normalization = '" + normalization + "'"
				+ " ORDER BY es.experience ASC, es.strand DESC";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Experience exp = new Experience();

				exp.setId(rs.getInt("id"));
				exp.setSeq(rs.getInt("sequence_id"));
				exp.setChip(rs.getString("chip_id"));
				exp.setExp(rs.getString("experience"));
				exp.setProjet(rs.getString("project"));
				exp.setNormalization(rs.getString("normalization"));
				exp.setStrand(rs.getInt("strand"));

				expList.add(exp);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getAllExperience");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getAllExperience conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return expList;
	}
	
	/** Get Reverse Experiences List **/
	public List<Experience> getReverseExperience(String normalization, List<Experience> listExp) {

		final List<Experience> listExp2 = new ArrayList<Experience>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		for (int i = 0; i < listExp.size(); i++) {

			command = "SELECT es.id, es.sequence_id, es.chip_id, es.experience, es.project, es.normalization, es.strand, es.color, es.info, ei.description"
					+ " FROM " + SCHEMA + ".exp_seq es, " + SCHEMA + ".exp_info ei" + " WHERE es.exp_info_id = ei.id" + " AND es.normalization = '" + normalization
					+ "'" + " AND es.project != 'Rho'" + " AND es.experience = '" + listExp.get(i).getExp() + "'";

			try {
				Class.forName(DRIVER);
				conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

				statement = conn.createStatement();
				rs = statement.executeQuery(command);

				while (rs.next()) {
					
					Experience exp = new Experience();

					exp.setId(rs.getInt("id"));
					exp.setSeq(rs.getInt("sequence_id"));
					exp.setChip(rs.getString("chip_id"));
					exp.setExp(rs.getString("experience"));
					exp.setProjet(rs.getString("project"));
					exp.setNormalization(rs.getString("normalization"));
					exp.setStrand(rs.getInt("strand"));
					exp.setColor(rs.getString("color"));
					exp.setColorSelect(listExp.get(i).getColorSelect());
					exp.setInfo(rs.getInt("info"));
					exp.setDescription(rs.getString("description"));
					
					listExp2.add(exp);
				}

			} catch (Exception ex) {
				
				System.err.println("problem in getExperience");
				System.err.println(ex + "\n" + ex.getMessage());
				ex.printStackTrace();

			} finally {
				try {
					rs.close();
				} catch (Exception ex) {
					System.err.println("problem in getExperience rs.close : " + ex + "\n" + ex.getMessage());
				}
				try {
					statement.close();
				} catch (Exception ex) {
					System.err.println("problem in getExperience statement.close : " + ex + "\n" + ex.getMessage());
				}
				try {
					conn.close();
				} catch (Exception ex) {
					System.err.println("problem in getExperience conn.close : " + ex + "\n" + ex.getMessage());
				}
			}
		}

		return listExp2;
	}

	/** Get Refresh Normalization Experiences List **/
	public List<Experience> getExpRefreshNorm(List<Experience> listExp, String norm) {

		final List<Experience> listExp2 = new ArrayList<Experience>();

		Genoscapist.appControler.listAllSelectedExperience.clear();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		for (int i = 0; i < listExp.size(); i++) {

			Experience exp = new Experience();
			
			command = "SELECT es.id, es.sequence_id, es.chip_id, es.experience, es.project, es.normalization, es.strand, es.color, es.info, ei.description"
					+ " FROM " + SCHEMA + ".exp_seq es, " + SCHEMA + ".exp_info ei" + " WHERE es.exp_info_id = ei.id AND es.strand = '" + listExp.get(i).getStrand() + "'"
					+ " AND es.normalization = '" + norm + "'" + " AND es.project != 'Rho'" + " AND es.experience = '"
					+ listExp.get(i).getExp() + "'";

			try {
				Class.forName(DRIVER);
				conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

				statement = conn.createStatement();
				rs = statement.executeQuery(command);

				while (rs.next()) {

					exp.setId(rs.getInt("id"));
					exp.setSeq(rs.getInt("sequence_id"));
					exp.setChip(rs.getString("chip_id"));
					exp.setExp(rs.getString("experience"));
					exp.setProjet(rs.getString("project"));
					exp.setNormalization(rs.getString("normalization"));
					exp.setStrand(rs.getInt("strand"));
					exp.setColor(rs.getString("color"));
					exp.setColorSelect(exp.getColor());
					exp.setInfo(rs.getInt("info"));
					exp.setDescription(rs.getString("description"));
				}

			} catch (Exception ex) {
				
				System.err.println("problem in getExpRefreshNorm");
				System.err.println(ex + "\n" + ex.getMessage());
				ex.printStackTrace();

			} finally {
				try {
					rs.close();
				} catch (Exception ex) {
					System.err.println("problem in getExpRefreshNorm rs.close : " + ex + "\n" + ex.getMessage());
				}
				try {
					statement.close();
				} catch (Exception ex) {
					System.err.println("problem in getExpRefreshNorm statement.close : " + ex + "\n" + ex.getMessage());
				}
				try {
					conn.close();
				} catch (Exception ex) {
					System.err.println("problem in getExpRefreshNorm conn.close : " + ex + "\n" + ex.getMessage());
				}
			}
			listExp2.add(exp);
		}

		return listExp2;
	}

	/** Get Regions List **/
	public List<Region> getRegion(Sequence s, long from, long to) {

		final List<Region> regionList = new ArrayList<Region>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat" + " FROM " + SCHEMA + ".features f"
					+ " WHERE f.sequence_id = '" + s.getId() + "'" + " AND f.type = 'blank'" + " AND f.stop >= " + from
					+ " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}

		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat" + " FROM " + SCHEMA + ".features f, "
					+ SCHEMA + ".sequences s" + " WHERE s.name = '" + s.getName() + "'" + " AND f.sequence_id = s.id"
					+ " AND f.type = 'blank'" + " AND f.stop >= " + from + " AND f.start <= " + to
					+ " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				Region region = new Region();

				region.setName(rs.getString("id_feat"));
				region.setStart(rs.getInt("start"));
				region.setEnd(rs.getInt("stop"));
				region.setStrand(rs.getInt("complement"));

				regionList.add(region);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getRegion");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegion rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegion statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getRegion conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return regionList;
	}
	
	/** Get Conditions **/

	public String getCondition(GeneItem gene, int level) {

		String table = "";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = null;

		if (level == 1) {

			command = "SELECT c.name, fc.value FROM " + SCHEMA + ".feat_condition fc, " + SCHEMA + ".condition c, "
					+ SCHEMA + ".features f " + "WHERE f.id = fc.feature_id and c.id = fc.condition_id "
					+ "AND f.id_feat = '" + gene.getLocusTag() + "'" + "AND fc.expression = " + level + " ORDER BY fc.value DESC";
		}

		else {

			command = "SELECT c.name, fc.value FROM " + SCHEMA + ".feat_condition fc, " + SCHEMA + ".condition c, "
					+ SCHEMA + ".features f " + "WHERE f.id = fc.feature_id and c.id = fc.condition_id "
					+ "AND f.id_feat = '" + gene.getLocusTag() + "'" + "AND fc.expression = " + level + " ORDER BY fc.value";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = statement.executeQuery(command);

			int size = 0;
			if (rs != null) {
				rs.last(); // moves cursor to the last row
				size = rs.getRow(); // get row id
				rs.beforeFirst();
			}

			if (rs.next() != false) {

				rs.beforeFirst();

				if (level == 1) {
					table += "<h2><div class='h2-withborder'>Details about expression for " + gene.getLocusTag() + "</div></h2>";
					table += "<h3>Expression clusters: ";
					table += "<a href = '" + Config.url + "#&cluster=" + gene.getClusterA() + "'>" + gene.getClusterA() + "</a> ";
					table += "<a href = '" + Config.url + "#&cluster=" + gene.getClusterB() + "'>" + gene.getClusterB() + "</a> ";
					table += "<a href = '" + Config.url + "#&cluster=" + gene.getClusterC() + "'>" + gene.getClusterC() + "</a></h3>";
					
					table += "<h3>Highest Expression Conditions</h3>";
				} else {
					table += "<h3>Lowest Expression Conditions</h3>";
				}

				table += "<table width=\"90%\" text-align=\"center\" align=\"center\" cellpadding=\"2\"><thead class='color'><tr>";

				
				while (rs.next()) {
					table += "<th>" + rs.getString("name") + "</th>";
				}

				table += "</tr></thead><tbody align=\"center\"><tr>";

				rs.beforeFirst();
				while (rs.next()) {

					if (rs.getString("value").length() < 4) {
						table += "<th class='yellow'>" + rs.getString("value") + "</th>";
						
					} else {
						table += "<th class='yellow'>" + rs.getString("value").substring(0, 4) + "</th>";
					}
				}
				table += "</tr></tbody></table>";
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getCondition()");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getCondition() rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getCondition() statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getCondition() conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return table;
	}
	
	/** Get Segments **/

	public String getSegment(String locustag, int level) {

		String table = "";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = null;

		if (level == 1) {

			command = "SELECT s.name, fs.value FROM " + SCHEMA + ".feat_segment fs, " + SCHEMA + ".segment s, " + SCHEMA
					+ ".features f " + "WHERE f.id = fs.feature_id and s.id = fs.segment_id " + "AND f.id_feat = '"
					+ locustag + "'" + "AND fs.correlation = " + level + " ORDER BY fs.value DESC";
		}

		else {
			command = "SELECT s.name, fs.value FROM " + SCHEMA + ".feat_segment fs, " + SCHEMA + ".segment s, " + SCHEMA
					+ ".features f " + "WHERE f.id = fs.feature_id and s.id = fs.segment_id " + "AND f.id_feat = '"
					+ locustag + "'" + "AND fs.correlation = " + level + " ORDER BY fs.value";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = statement.executeQuery(command);

			if (rs.next() != false) {

				rs.beforeFirst();

				table += "<table width=\"90%\" text-align=\"center\" align=\"center\" cellpadding=\"2\"><thead class='color'><tr>";

				if (level == 1) {
					table += "<h3>Most Positively Correlated Segments</h3>";
				} else {
					table += "<h3>Most Negatively Correlated Segments</h3>";
				}

				String th1 = "";
				String th2 = "";
				
				while (rs.next()) {

					String name = rs.getString("name");
					String value = rs.getString("value");
					if ( value.contains("-") == true && value.length() > 4 ) { value = value.substring(0, 5); }
					else if ( value.length() >= 4 ) { value = value.substring(0, 4); }
									
					th1 += "<th><a class='color' href=\"" + Config.url + "#&id=" + name + "\">" + name + "</a></th>";
					th2 += "<th class='yellow'>"+ value + "</th>";
				}
				
				th1 += "</tr></thead><tbody align=\"center\"><tr>";
				th2 += "</tr></tbody></table>";

				table += th1; table += th2;
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getSegment()");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getSegment() rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getSegment() statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getSegment() conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return table;
	}
	
	/** Get Gene **/

	public GeneItem getGeneItem(Segment segment) {

		final GeneItem gene = new GeneItem();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		String command = "SELECT f.id, f.id_feat, f.start, f.stop, f.complement, f.clustera, f.clusterb, f.clusterc, q.value "
				+ " FROM " + SCHEMA + ".features f, " + SCHEMA + ".qualifiers q" + " WHERE f.id = q.feature_id"
				+ " AND f.start = " + segment.getStart() + " AND f.stop = " + segment.getEnd() + " AND f.complement = "
				+ segment.getStrand() + " AND f.type in ('CDS','TSV')" + " AND q.type = 'name'" + " ORDER BY f.start";

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			rs.next();

			int id = rs.getInt("id");
			gene.setGeneId(id);
			gene.setLocusTag(rs.getString("id_feat"));
			gene.setName(rs.getString("id_feat"));
			gene.setStart(rs.getInt("start"));
			gene.setEnd(rs.getInt("stop"));
			gene.setStrand(rs.getInt("complement"));
			gene.setClusterA(rs.getString("clustera"));
			gene.setClusterB(rs.getString("clusterb"));
			gene.setClusterC(rs.getString("clusterc"));

		} catch (Exception ex) {
			
			System.err.println("problem in getGeneItem() (with segment)");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getGeneItem() (with segment) rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println(
						"problem in getGeneItem() (with segment) statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err
						.println("problem in getGeneItem() (with segment) conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return gene;
	}
	
	/** Get Deleted Region List **/
	
	public List<DeletedRegion> getDeletedRegion(Sequence s, long from, long to) {

		final List<DeletedRegion> deletedregionList = new ArrayList<DeletedRegion>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		String command = "";

		if (s.getId() > 0) {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color" + " FROM " + SCHEMA
					+ ".features f WHERE f.sequence_id = '" + s.getId() + "'"
					+ " AND f.type = 'DeletedRegion'"
					+ " AND f.stop >= " + from + " AND f.start <= " + to + " ORDER BY f.ID, f.START";
		}
		
		else {

			command = "SELECT f.id, f.start, f.stop, f.complement, f.id_feat, f.color" + " FROM " + SCHEMA
					+ ".features f, " + SCHEMA + ".sequences s WHERE s.name = '"
					+ s.getName() + "'" + " AND f.sequence_id = s.id"
					+ " AND f.type = 'segment' AND f.stop >= " + from + " AND f.start <= "
					+ to + " ORDER BY f.ID, f.START";
		}

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			while (rs.next()) {

				DeletedRegion deletedregion = new DeletedRegion();

				deletedregion.setName(rs.getString("id_feat"));
				deletedregion.setStart(rs.getInt("start"));
				deletedregion.setEnd(rs.getInt("stop"));
				deletedregion.setStrand(rs.getInt("complement"));
				deletedregion.setColor(rs.getString("color"));

				deletedregionList.add(deletedregion);
			}

		} catch (Exception ex) {
			
			System.err.println("problem in getDeletedRegion");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();

		} finally {
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in getDeletedRegion rs.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in getDeletedRegion statement.close : " + ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in getDeletedRegion conn.close : " + ex + "\n" + ex.getMessage());
			}
		}

		return deletedregionList;
	}}