/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.gfx;

import org.vectomatic.dom.svg.OMSVGDocument;
import org.vectomatic.dom.svg.OMSVGSVGElement;

public class BioContext {

	private float  bpPerPixel = 0;
    private long   bpPerLine  = 0;
    private int    maxWidth   = 0;
    private OMSVGDocument document;
    private OMSVGSVGElement canvas;

    public BioContext( ) { }
    public BioContext( float b ) { setBpPerPixel( b ); }

    public void  setBpPerPixel( float b ) { bpPerPixel = b; }
    public float getBpPerPixel( ) { return bpPerPixel; }

    public void setBpPerLine( long b ) { bpPerLine = b; }
    public long getBpPerLine() { return bpPerLine; }

    public void setMaxWidth( int b ) { maxWidth = b; }
    public int  getMaxWidth( ) { return maxWidth; }

    public String toString() { return "BioContext : Scale = "+bpPerPixel; }
    public void setDocument(OMSVGDocument doc) {document = doc;     }
    public OMSVGDocument getDocument() { return document; }
    public void setCanvas(OMSVGSVGElement canvas) { this.canvas = canvas; }
    public OMSVGSVGElement getCanvas() { return canvas; }
}