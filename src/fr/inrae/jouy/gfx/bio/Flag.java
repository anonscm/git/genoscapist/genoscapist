/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.gfx.bio;

import org.vectomatic.dom.svg.OMSVGGElement;
import org.vectomatic.dom.svg.OMSVGLength;
import org.vectomatic.dom.svg.OMSVGPointList;
import org.vectomatic.dom.svg.OMSVGPolygonElement;
import org.vectomatic.dom.svg.OMSVGRectElement;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.OMSVGTextElement;
import org.vectomatic.dom.svg.utils.SVGConstants;

import fr.inrae.jouy.gfx.BioContext;
import fr.inrae.jouy.shared.Promoter;
import fr.inrae.jouy.shared.Terminator;

public class Flag {

	OMSVGGElement element;
	
	/** Graphic object for Promoter **/
	public Flag(final Promoter promoter, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {

		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
		
		int tabStart = (int) promoter.getStart();
		int tabStop = (int) promoter.getEnd();
				
		from = from + ( (long) basePerLine * posLine );
		
		float startX = 0;
		float stopX = 0;
		
		// Start in
		if ( tabStart <= finFenetre && tabStart >= from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;
			
			// Left points
			OMSVGTextElement points;
			if ( promoter.getStrand() == 1 ) {		
				points = new OMSVGTextElement(startX+15, startY-2, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}		
			else {			
				points = new OMSVGTextElement(startX+15, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		// Stop in
		if ( tabStop <= finFenetre && tabStop >= from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1140;
			
			// Right points
			OMSVGTextElement points;
			if ( promoter.getStrand() == 1 ) {
				points = new OMSVGTextElement(stopX+35, startY-2, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			else {
				points = new OMSVGTextElement(stopX+35, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		
		OMSVGPolygonElement polygon = new OMSVGPolygonElement();
		OMSVGPointList points = polygon.getPoints();
					
		OMSVGRectElement rect;
					
		int center = promoter.getCenter();
		float startFlag = ((center - from) * width) / basePerLine;
					
		float pos = 0;
		
		// Strand +
		if ( promoter.getStrand() == 1 ) {
					
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY));					// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY-20));				// Flag top
			points.appendItem(canvas.createSVGPoint(startFlag+30+10, startY-20));			// Flag point
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY-12));				// Flag end
						
			pos = startFlag+30;
					
			// Flag base
			rect = new OMSVGRectElement(startX+30, startY-5, stopX-startX+1, 4, 0, 0);
		}
					
		// Strand -
		else {
						
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY));					// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY+20));				// Flag top
			points.appendItem(canvas.createSVGPoint(startFlag+30-10, startY+20));			// Flag point
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY+12));				// Flag end
						
			pos = startFlag+30-10;
						
			// Flag base
			rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, 5, 0, 0);
		}
		
		if ( center <= finFenetre && center >= from ) {
			element.appendChild(polygon);
		}
		
		element.appendChild(rect);
					
		element.setId("promoter_"+promoter.getName());
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
					
		float posy = 273;
		
		if ( promoter.getStrand() == -1 ) {
			posy = 370;
		}
					
		OMSVGTextElement text = new OMSVGTextElement(pos, posy, OMSVGLength.SVG_LENGTHTYPE_PX, promoter.getName());
		text.setId("name_" + promoter.getName());
		text.getStyle().setSVGProperty(SVGConstants.CSS_FONT_SIZE_PROPERTY, "9");
		text.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "none");
		text.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "none");
		canvas.appendChild(text);
	}
	
	/** Graphic object for Terminator **/
	public Flag(final Terminator terminator, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {

		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
		
		int tabStart = (int) terminator.getStart();
		int tabStop = (int) terminator.getEnd();
				
		from = from + ( (long) basePerLine * posLine );
		
		float startX = 0;
		float stopX = 0;
		
		// Start in
		if ( tabStart <= finFenetre && tabStart >= from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;
			
			// Left points
			OMSVGTextElement points;
			if ( terminator.getStrand() == 1 ) {
				points = new OMSVGTextElement(startX+15, startY-2, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
									
			else {
				points = new OMSVGTextElement(startX+15, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		// Stop in
		if ( tabStop <= finFenetre && tabStop >= from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1140;
			
			// Right points
			OMSVGTextElement points;
			if ( terminator.getStrand() == 1 ) {
				points = new OMSVGTextElement(stopX+35, startY-2, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			else {
				points = new OMSVGTextElement(stopX+35, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		
		OMSVGPolygonElement polygon = new OMSVGPolygonElement();
		OMSVGPointList points = polygon.getPoints();
					
		OMSVGRectElement rect = new OMSVGRectElement();
					
		int center = terminator.getCenter();
		float startFlag = ((center - from) * width) / basePerLine;
					
		float pos = 0;
					
		// Strand +
		if ( terminator.getStrand() == 1 ) {
					
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY));					// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY-15));				// Flag top
			points.appendItem(canvas.createSVGPoint(startFlag+30-10, startY-15));			// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30-10, startY-7));			// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY-7));				// Flag base
						
			pos = startFlag+30-10;
			
			// Flag base
			rect = new OMSVGRectElement(startX+30, startY-5, stopX-startX+1, 4, 0, 0);
		}
					
		// Strand -
		else {
					
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY));					// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY+15));				// Flag top
			points.appendItem(canvas.createSVGPoint(startFlag+30+10, startY+15));			// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30+10, startY+7));			// Flag base
			points.appendItem(canvas.createSVGPoint(startFlag+30, startY+7));				// Flag base
						
			pos = startFlag+30;
						
			// Flag base
			rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, 5, 0, 0);
		}
										
		if ( center <= finFenetre && center >= from ) {
			element.appendChild(polygon);
		}
		element.appendChild(rect);
		element.setId("terminator_"+terminator.getName());
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
					
		float posy = 273;
		if ( terminator.getStrand() == -1 ) {
			posy = 370;
		}
		OMSVGTextElement text = new OMSVGTextElement(pos, posy, OMSVGLength.SVG_LENGTHTYPE_PX, terminator.getName());
		text.setId("name_" + terminator.getName());
		text.getStyle().setSVGProperty(SVGConstants.CSS_FONT_SIZE_PROPERTY, "9");
		text.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "none");
		text.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "none");
		canvas.appendChild(text);
	}
	
	public OMSVGGElement getFlag() {
		
		return element;
	}
}