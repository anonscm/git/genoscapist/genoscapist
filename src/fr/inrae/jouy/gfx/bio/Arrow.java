/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.gfx.bio;

import org.vectomatic.dom.svg.OMSVGGElement;
import org.vectomatic.dom.svg.OMSVGLength;
import org.vectomatic.dom.svg.OMSVGPointList;
import org.vectomatic.dom.svg.OMSVGPolygonElement;
import org.vectomatic.dom.svg.OMSVGRectElement;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.OMSVGTextElement;
import org.vectomatic.dom.svg.utils.SVGConstants;

import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.gfx.BioContext;
import fr.inrae.jouy.shared.Feature;

public class Arrow {
	
	OMSVGGElement element;

	/** Graphic object for CDS **/
	public Arrow(final Feature cds, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {
		
		int ajout_y = 20;
		int decal = 25;
		
		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
				
		int tabStart = (int) cds.getStart();
		int tabStop = (int) cds.getStop();
				
		from = from + ( (long) basePerLine * posLine );

		float pos = 0;
				
		// 1st case: start int & stop out
		if ( tabStop > finFenetre && tabStart <= finFenetre && tabStart >= from ) {
	
			float startX = ((tabStart - from) * width) / basePerLine;
			float stopX = 1170;

			// Strand -
			if ( cds.getSens() == -1 ) {
						
				float length = stopX - startX;
						
				if ( length >= 35 ) {
						
					OMSVGPolygonElement polygon = new OMSVGPolygonElement();
					OMSVGPointList points = polygon.getPoints();

					points.appendItem(canvas.createSVGPoint(stopX, startY+20+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(stopX, startY+10+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+10+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+5+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30, startY+15+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+25+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+20+ajout_y+decal));
							
					element.appendChild(polygon);
				}
						
				else if ( length == 35 ) {
							
					OMSVGPolygonElement polygon = new OMSVGPolygonElement();
					OMSVGPointList points = polygon.getPoints();
							
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+5+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30, startY+15+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+25+ajout_y+decal));
							
					element.appendChild(polygon);
				}
						
				else {
							
					OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y+decal, stopX-startX+1-30, 10, 1, 1);
					element.appendChild(rect);
				}
						
				pos = startX + 30;
			}
	
			// Strand +
			else {

				OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y, stopX-startX+1-30, 10, 1, 1);
				element.appendChild(rect);
						
				pos = startX + 30;
			}
					
			// End points
			if ( tabStop > to ) {
					
				if ( cds.getSens() == -1 ) {
							
					OMSVGTextElement points = new OMSVGTextElement(stopX+5, startY+15+ajout_y+decal, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
					element.appendChild(points);
				}
						
				else {
							
					OMSVGTextElement points = new OMSVGTextElement(stopX+5, startY+15+ajout_y, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
					element.appendChild(points);
				}
			}
					
			else {

				startX = 0;
				stopX = ((tabStop - (from+basePerLine)) * width)  / basePerLine;
				startY += 80;
						
				// Strand -
				if ( cds.getSens() == -1 ) {
							
					OMSVGRectElement rect = new OMSVGRectElement(30, startY+10+ajout_y+decal, stopX+1, 10, 1, 1);
					element.appendChild(rect);
										
					pos = 30;
				}
						
				// Strand +
				else {
							
					float length = stopX - startX;
							
					if ( length >= 5 ) {
								
						OMSVGPolygonElement polygon = new OMSVGPolygonElement();
						OMSVGPointList points = polygon.getPoints();
								
						points.appendItem(canvas.createSVGPoint(startX+30, startY+20+ajout_y));
						points.appendItem(canvas.createSVGPoint(startX+30, startY+10+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+10+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+5+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30, startY+15+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+25+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+20+ajout_y));
								
						element.appendChild(polygon);
							
						pos = startX+30;
					}
							
					else if ( length == 5 ) {
								
						OMSVGPolygonElement polygon = new OMSVGPolygonElement();
						OMSVGPointList points = polygon.getPoints();
								
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+5+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30, startY+15+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+25+ajout_y));
								
						element.appendChild(polygon);
								
						pos = stopX+30-5;
					}
							
					else {
								
						OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y, stopX-startX+1, 10, 1, 1);
						element.appendChild(rect);
								
						pos = startX+30;
					}
				}
			}
		}
				
		// 2nd case: start out & stop in
		else if ( tabStart < from && tabStop <= finFenetre && tabStop >= from) {
					
			float startX = 0;
			float stopX = ((tabStop - from) * width)  / basePerLine;
					
			// Strand +
			if ( cds.getSens() == 1  ) {
						
				float length = stopX - startX;
						
					if ( length >= 5 ) {
							
						OMSVGPolygonElement polygon = new OMSVGPolygonElement();
						OMSVGPointList points = polygon.getPoints();
							
						points.appendItem(canvas.createSVGPoint(startX+30, startY+20+ajout_y));
						points.appendItem(canvas.createSVGPoint(startX+30, startY+10+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+10+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+5+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30, startY+15+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+25+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+20+ajout_y));
							
						element.appendChild(polygon);
						
						pos = startX+30;
					}
						
					else if ( length == 5 ) {
							
						OMSVGPolygonElement polygon = new OMSVGPolygonElement();
						OMSVGPointList points = polygon.getPoints();
							
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+5+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30, startY+15+ajout_y));
						points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+25+ajout_y));
							
						element.appendChild(polygon);
							
						pos = stopX+30-5;
					}
						
					else {
							
						OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y, stopX-startX+1, 10, 1, 1);
						element.appendChild(rect);
							
						pos = startX+30;
					}
						
				// End points
				OMSVGTextElement points = new OMSVGTextElement(startX+15, startY+15+ajout_y, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				element.appendChild(points);
			}
					
			// Strand -
			else {
						
				OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y+decal, stopX-startX+1, 10, 1, 1);
				element.appendChild(rect);
						
				pos = startX+30;
						
				// Start points
				OMSVGTextElement points = new OMSVGTextElement(startX+15, startY+15+ajout_y+decal, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				element.appendChild(points);
			}
		}
				
		// 3rd case: start & stop out
		else if ( tabStart < from && tabStop > finFenetre ) {
										
			float startX = 0;
			float stopX = 1170;
					
			if ( cds.getSens() == 1  ) {
				
				OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y, stopX-startX+1-30, 10, 1, 1);
				element.appendChild(rect);
						
				// Start points
				OMSVGTextElement points1 = new OMSVGTextElement(startX+15, startY+15+ajout_y, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				element.appendChild(points1);
						
				// End points
				OMSVGTextElement points2 = new OMSVGTextElement(stopX+5, startY+15+ajout_y, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				element.appendChild(points2);
			}
					
			else {
						
				OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y+decal, stopX-startX+1, 10, 1, 1);
				element.appendChild(rect);
				
				// Start points
				OMSVGTextElement points1 = new OMSVGTextElement(startX+15, startY+15+ajout_y+decal, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				element.appendChild(points1);
						
				// End points
				OMSVGTextElement points2 = new OMSVGTextElement(stopX+5, startY+15+ajout_y+decal, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				element.appendChild(points2);
			}
		}
				
		// 4th case: start et stop in
		else if (tabStart >= from && tabStop >= from && tabStart <= finFenetre && tabStop <= finFenetre) {
					
			float startX = ((tabStart - from) * width) / basePerLine;
			float stopX = ((tabStop - from) * width) / basePerLine;
					
			// Strand +
			if ( cds.getSens() == 1 ) {
						
				float length = stopX - startX;
						
				if ( length >= 5 ) {
							
					OMSVGPolygonElement polygon = new OMSVGPolygonElement();
					OMSVGPointList points = polygon.getPoints();
								
					points.appendItem(canvas.createSVGPoint(startX+30, startY+20+ajout_y));
					points.appendItem(canvas.createSVGPoint(startX+30, startY+10+ajout_y));
					points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+10+ajout_y));
					points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+5+ajout_y));
					points.appendItem(canvas.createSVGPoint(stopX+30, startY+15+ajout_y));
					points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+25+ajout_y));
					points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+20+ajout_y));
								
					element.appendChild(polygon);
							
					pos = startX+30;
				}
						
				else if ( length == 5 ) {
							
					OMSVGPolygonElement polygon = new OMSVGPolygonElement();
					OMSVGPointList points = polygon.getPoints();
								
					points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+5+ajout_y));
					points.appendItem(canvas.createSVGPoint(stopX+30, startY+15+ajout_y));
					points.appendItem(canvas.createSVGPoint(stopX+30-5, startY+25+ajout_y));
								
					element.appendChild(polygon);
								
					pos = stopX+30-5;
				}
						
				else {
								
					float w = stopX-startX+1;
								
					// Very small CDS
					if (w < 1 ) { w = 1; }
								
					OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y, w, 10, 1, 1);
					element.appendChild(rect);
								
					pos = startX+30;
				}
			}
					
			// Strand -
			else if ( cds.getSens() == -1 ) {
						
				float length = stopX - startX;
						
				if ( length >= 5 ) {
						
					OMSVGPolygonElement polygon = new OMSVGPolygonElement();
					OMSVGPointList points = polygon.getPoints();
							
					points.appendItem(canvas.createSVGPoint(stopX+30, startY+20+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(stopX+30, startY+10+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+10+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+5+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30, startY+15+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+25+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+20+ajout_y+decal));
							
					element.appendChild(polygon);
				}
						
				else if ( length == 5 ) {
							
					OMSVGPolygonElement polygon = new OMSVGPolygonElement();
					OMSVGPointList points = polygon.getPoints();
							
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+5+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30, startY+15+ajout_y+decal));
					points.appendItem(canvas.createSVGPoint(startX+30+5, startY+25+ajout_y+decal));
							
					element.appendChild(polygon);
				}
						
				else {
							
					float w = stopX-startX+1-30;
							
					// Very small CDS
					if (w < 1 ) { w = 1; }
							
					OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY+10+ajout_y+decal, w, 10, 1, 1);
					element.appendChild(rect);
				}
						
				pos = startX+30;
			}
		}
			
		// ID & color
		element.setId("cds_"+cds.getId_feat());
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "#1EB7D2");
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "#1EB7D2");
		
		float posy = 102;
		if ( cds.getSens() == -1 ) { posy = 162; }
		
		OMSVGTextElement text = new OMSVGTextElement(pos, posy, OMSVGLength.SVG_LENGTHTYPE_PX, cds.getId_feat());
		text.setId("name_" + cds.getId_feat());
		text.getStyle().setSVGProperty(SVGConstants.CSS_FONT_SIZE_PROPERTY, "9");
		text.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "none");
		text.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "none");
			
		if ( Genoscapist.appControler.getGeneSelected() != null ) {
				
			if ( cds.getId_feat().contains(Genoscapist.appControler.getGeneSelected().getLocusTag()) ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, "#8462B1");
				element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "#8462B1");
			}
		}
			
		canvas.appendChild(text);
	}
	
	public OMSVGGElement getArrow() {
		return element;
	}
}