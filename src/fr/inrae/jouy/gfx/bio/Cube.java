/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.gfx.bio;

import org.vectomatic.dom.svg.OMSVGGElement;
import org.vectomatic.dom.svg.OMSVGLength;
import org.vectomatic.dom.svg.OMSVGRectElement;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.OMSVGTextElement;
import org.vectomatic.dom.svg.utils.SVGConstants;

import fr.inrae.jouy.gfx.BioContext;
import fr.inrae.jouy.shared.DeletedRegion;
import fr.inrae.jouy.shared.Region;
import fr.inrae.jouy.shared.RegionUpRho;
import fr.inrae.jouy.shared.Regulator;
import fr.inrae.jouy.shared.Segment;
import fr.inrae.jouy.shared.Transterm;

public class Cube {
	
	OMSVGGElement element;

	/** Graphic object for Transterm **/
	public Cube(final Transterm transterm, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {

		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
				
		int tabStart = (int) transterm.getStart();
		int tabStop = (int) transterm.getEnd();
			
		from = from + ( (long) basePerLine * posLine );

		float startX = 0;
		float stopX = 0;
		
		// Start in
		if ( tabStart <= finFenetre && tabStart >= from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;
			
			// Left points
			OMSVGTextElement points;
			
			if ( transterm.getStrand() == 1 ) {
//				points = new OMSVGTextElement(startX+15, startY-(float) transterm.getThickness()*10 + (((float) transterm.getThickness()*10) / 2), OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				points = new OMSVGTextElement(startX+15, startY-3, OMSVGLength.SVG_LENGTHTYPE_PX, "...");	
			}
			
			else {
//				points = new OMSVGTextElement(startX+15, startY-(float) transterm.getThickness()*10 - (((float) transterm.getThickness()*10) / 2), OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				points = new OMSVGTextElement(startX+15, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			
			element.appendChild(points);
		}
		
		// Stop in
		if ( tabStop <= finFenetre && tabStop >= from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1170-30-1;
			
			// Right points
			OMSVGTextElement points;
			
			if ( transterm.getStrand() == 1 ) {
//				points = new OMSVGTextElement(stopX+35, startY-(float) transterm.getThickness()*10 + (((float) transterm.getThickness()*10) / 2), OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				points = new OMSVGTextElement(stopX+35, startY-3, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			
			else {
//				points = new OMSVGTextElement(stopX+35, startY-(float) transterm.getThickness()*10 - (((float) transterm.getThickness()*10) / 2), OMSVGLength.SVG_LENGTHTYPE_PX, "...");
				points = new OMSVGTextElement(stopX+35, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			
			element.appendChild(points);
		}
		
		OMSVGRectElement rect;
				
		if ( transterm.getStrand() == 1 ) {
			rect = new OMSVGRectElement(startX+30, startY-(float) transterm.getThickness()*10, stopX-startX+1, (float) transterm.getThickness()*10, 0, 0);
		}
		
		else {
			rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, (float) transterm.getThickness()*10, 0, 0);
		}
		
		element.appendChild(rect);
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_NONE_VALUE);
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, transterm.getColor());
		element.setId("transterm_"+transterm.getName());	
		
		float posy = 45;
		if ( transterm.getStrand() == -1 ) {
			posy = 80;
		}
		OMSVGTextElement text = new OMSVGTextElement(startX+30, posy, OMSVGLength.SVG_LENGTHTYPE_PX, transterm.getName());
  		text.setId("name_" + transterm.getName());
  		text.getStyle().setSVGProperty(SVGConstants.CSS_FONT_SIZE_PROPERTY, "9");
  		text.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "none");
  		text.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "none");
  		canvas.appendChild(text);
	}
	
	/** Graphic object for Regulator **/
	public Cube(final Regulator regulator, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {

		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
		
		int tabStart = (int) regulator.getStart();
		int tabStop = (int) regulator.getEnd();
				
		from = from + ( (long) basePerLine * posLine );

		float startX = 0;
		float stopX = 0;
		
		if ( regulator.getStrand() == -1 ) {
			startY += 15;
		}
		else {
			startY += 5;
		}
		
		// Start in
		if ( tabStart <= finFenetre && tabStart >= from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;
			
			// Left points
			OMSVGTextElement points = new OMSVGTextElement(startX+15, startY-15, OMSVGLength.SVG_LENGTHTYPE_PX, "...");					
			element.appendChild(points);
		}
		
		// Stop in
		if ( tabStop <= finFenetre && tabStop >= from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1170;
			
			// Right points
			OMSVGTextElement points = new OMSVGTextElement(stopX+5, startY-15, OMSVGLength.SVG_LENGTHTYPE_PX, "...");					
			element.appendChild(points);
		}
		
		OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, (float) regulator.getThickness()*50, 0, 0);					
		element.appendChild(rect);
		
		// ID & color
		element.setId("regulator_"+regulator.getName());
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
		
		if ( regulator.getColor().compareTo("cyan") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_CYAN_VALUE);
		}
		
		else if ( regulator.getColor().compareTo("magenta") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_MAGENTA_VALUE);
		}
		
		float posy = 170;
		if ( regulator.getStrand() == -1 ) {
			posy = 205;
		}
		OMSVGTextElement text = new OMSVGTextElement(startX+30, posy, OMSVGLength.SVG_LENGTHTYPE_PX, regulator.getName());
  		text.setId("name_" + regulator.getName());
  		text.getStyle().setSVGProperty(SVGConstants.CSS_FONT_SIZE_PROPERTY, "9");
  		text.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "none");
  		text.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "none");
  		canvas.appendChild(text);
	}
	
	/** Graphic object for Segment **/
	public Cube(Segment segment, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {

		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
		
		int tabStart = (int) segment.getStart();
		int tabStop = (int) segment.getEnd();
		
		// Test
		from = from + ( (long) basePerLine * posLine );
		
		float startX = 0;
		float stopX = 0;
		float pos = 0;
		
		// Start in
		if ( tabStart <= finFenetre && tabStart >= from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;
			
			// Left points
			OMSVGTextElement points;
			if ( segment.getStrand() == 1 ) {
				points = new OMSVGTextElement(startX+15, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			else {
				points = new OMSVGTextElement(startX+15, startY+25, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		
		// Stop in
		if ( tabStop <= finFenetre && tabStop >= from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1170-30;
			
			// Right points
			OMSVGTextElement points;
			if ( segment.getStrand() == 1 ) {
				points = new OMSVGTextElement(stopX+35, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			else {
				points = new OMSVGTextElement(stopX+35, startY+25, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		
		OMSVGRectElement rect;
		if ( segment.getStrand() == 1 ) {
			rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, 10, 0, 0);
		}
		
		else {
			rect = new OMSVGRectElement(startX+30, startY+20, stopX-startX+1, 10, 0, 0);
		}
		element.appendChild(rect);
		
		// ID & color
		element.setId("segment_"+segment.getName());
		pos = startX+30;
		if ( segment.getColor().compareTo("darkred") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_DARKRED_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_DARKRED_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("lightblue") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_LIGHTBLUE_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_LIGHTBLUE_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("orange") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_ORANGE_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_ORANGE_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("oldyellow") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_YELLOW_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_YELLOW_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("blue") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BLUE_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BLUE_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("red") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("black") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BLACK_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BLACK_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("brown") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BROWN_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BROWN_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("darkblue") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_DARKBLUE_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_DARKBLUE_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		else if ( segment.getColor().compareTo("green") == 0 ) {
			
			element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_GREEN_VALUE);
			
			if ( segment.getFilled().compareTo("yes") == 0 ) {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_GREEN_VALUE);
			}
			
			else {
				element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
			}
		}
		
		float posy = 215;
		if ( segment.getStrand() == -1 ) {
			posy = 265;
		}
		OMSVGTextElement text = new OMSVGTextElement(pos, posy, OMSVGLength.SVG_LENGTHTYPE_PX, segment.getName());
  		text.setId("name_" + segment.getName());
  		text.getStyle().setSVGProperty(SVGConstants.CSS_FONT_SIZE_PROPERTY, "9"); // 8
  		text.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "none");
  		text.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "none");
  		canvas.appendChild(text);
	}
	
	/** Graphic object for RegionUpRho **/
	public Cube(RegionUpRho segment, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {

		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
		
		int tabStart = (int) segment.getStart();
		int tabStop = (int) segment.getEnd();
				
		from = from + ( (long) basePerLine * posLine );

		float startX = 0;
		float stopX = 0;
		
		// Start in
		if ( tabStart < finFenetre && tabStart > from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;
			
			// Left points
			OMSVGTextElement points;
			if ( segment.getStrand() == 1 ) {
				points = new OMSVGTextElement(startX+15, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			else {
				points = new OMSVGTextElement(startX+15, startY+25, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		// Stop in
		if ( tabStop <  finFenetre && tabStop > from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1170-30;
			
			// Right points
			OMSVGTextElement points;
			if ( segment.getStrand() == 1 ) {
				points = new OMSVGTextElement(stopX+35, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			else {
				points = new OMSVGTextElement(stopX+35, startY+25, OMSVGLength.SVG_LENGTHTYPE_PX, "...");
			}
			element.appendChild(points);
		}
		
		OMSVGRectElement rect;
		
		if ( segment.getStrand() == 1 ) {
			rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, 10, 0, 0);
		}
		
		else {
			rect = new OMSVGRectElement(startX+30, startY+20, stopX-startX+1, 10, 0, 0);
		}
		
		// ID & color
		element.setId("rectangle_"+segment.getName());
		element.appendChild(rect);
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, segment.getColor());
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, segment.getColor());
	}
	
	/** Graphic object for Region **/
	public Cube(Region region, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc, int i, int line) {

		float finFenetre = (from + (basePerLine * posLine)) + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
		
		int tabStart = (int) region.getStart();
		int tabStop = (int) region.getEnd();
				
		from = from + ( (long) basePerLine * posLine );

		float startX = 0;
		float stopX = 0;
		
		// Start in
		if ( tabStart < finFenetre && tabStart > from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;
		}
		// Stop in
		if ( tabStop <  finFenetre && tabStop > from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1170-30;
		}
		
		OMSVGRectElement rect;
		
		// Strand +
		if ( region.getStrand() == 1 ) {
			rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, 70, 0, 0);
		}
		// Strand -
		else {
			rect = new OMSVGRectElement(startX+30, startY+100, stopX-startX+1, 70, 0, 0);
		}
		
		if ( line != 0 ) {
			element.setId("rectangle_"+i+"_"+line);
		}
		else {
			element.setId("rectangle_"+i);
		}
		
		// ID & Color
		element.appendChild(rect);
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_WHITE_VALUE);
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_OPACITY_PROPERTY, "1");
	}
	
	/** Graphic object for DeletedRegion **/
	public Cube(DeletedRegion deletedRegion, long from, long to, float width, float startY, float basePerLine, int posLine, OMSVGSVGElement canvas, BioContext bc) {		
		
		float finFenetre = from + basePerLine - 1;
		
		element = bc.getDocument().createSVGGElement();
		
		int tabStart = (int) deletedRegion.getStart();
		int tabStop = (int) deletedRegion.getEnd();
				
		float startX = 0;
		float stopX = 0;
		
		// Start in
		if ( tabStart < finFenetre && tabStart >= from ) {
			startX = ((tabStart - from) * width) / basePerLine;
		}
		// Start out
		else {
			startX = 0;			
			
			// Left points
			OMSVGTextElement points = new OMSVGTextElement(startX+15, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");				
			element.appendChild(points);
		}
		// Stop in
		if ( tabStop <  finFenetre && tabStop > from ) {
			stopX = ((tabStop - from) * width)  / basePerLine;
		}
		// Stop out
		else {
			stopX = 1170-30;
			
			// Right points
			OMSVGTextElement points = new OMSVGTextElement(stopX+35, startY+5, OMSVGLength.SVG_LENGTHTYPE_PX, "...");				
			element.appendChild(points);	
		}
		
		OMSVGRectElement rect = new OMSVGRectElement(startX+30, startY, stopX-startX+1, 10, 0, 0);					
		element.appendChild(rect);
		
		// ID & Color 
		element.setId("deletedregion_"+deletedRegion.getName());
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, deletedRegion.getColor());
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, deletedRegion.getColor());
	}
	
	/** Graphic object for BlankRegion **/
	public Cube(String type, float startY, BioContext bc) {
		
		element = bc.getDocument().createSVGGElement();
		
		float startX = 0;
		float width = 30;
		
		if ( type.contains("first") == true ) {
			startX = 0;
			if ( type.contains("rho") == true )	{ element.setId("rho_rectangle_first_"+startY); }
			else								{ element.setId("rectangle_first"); }
		}
		else {
			startX = 1170;
			width = 40;
			if ( type.contains("rho") == true )	{ element.setId("rho_rectangle_last_"+startY); }
			else								{ element.setId("rectangle_last"); }
		}
		
		OMSVGRectElement rect = new OMSVGRectElement(startX, startY, width, 180, 0, 0);
		element.appendChild(rect);
		
		// Color
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_WHITE_VALUE);
		element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_WHITE_VALUE);
		element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_OPACITY_PROPERTY, "1");
	}

	public OMSVGGElement getCube() {
		
		return element;
	}
}