/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.gfx.curve;

import java.util.ArrayList;

import org.vectomatic.dom.svg.OMSVGPathSegList;

import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.gfx.BioContext;

public class Abs extends Axe {
	
	private int sens = 1;
	
	/**
     * Default values
     *
     * @param x
     *            x position
     * @param y
     *            y position
     * @param width
     *            axis size
     * @param l
     *            legend text
     */
    public Abs(BioContext bc, float x, float y, int w, String l) {
            
    	super(bc, x, y, w, l);
    }
    
    /**
     * Legend
     *
     * @param x
     *            x position
     * @param y
     *            y position
     * @param width
     *            axis size
     */
    public Abs(BioContext bc, float x, float y, float widthWithoutMargin) {
            
    	super(bc, x, y, widthWithoutMargin);
    }
    
    /**
     * Axis graduation
     *
     * @param val
     *            value
     * @param leg
     *            legend
     */
    protected void drawGraduation(float val, String leg) {
    	
    	float x = X + Math.round(getPos(val));
    	OMSVGPathSegList segs1 = gfxUnits.getPathSegList();
        segs1.appendItem(gfxUnits.createSVGPathSegMovetoAbs(x, Y));
        segs1.appendItem(gfxUnits.createSVGPathSegLinetoAbs(x, Y + sens * gradHeight));

        float posTxt = Y;
        
        if (sens == -1)	{ posTxt = Y + sens * gradHeight - 13; }
        
        txtUnits.appendChild(bcx.getDocument().createSVGTextElement(x-(leg.length() * 3), posTxt+20, (short) 5, leg));
    }
    
    protected void drawGraduation(int position) {
    	
    	// X = 30 / Y = 60
    	int width = 1140;
    	float x = ((position - Genoscapist.appControler.getStart_view()) * width) / Genoscapist.sizeSequence;
        
        float grad = 1140 / Genoscapist.sizeSequence;
        if ( grad > 0 ) {
        	rectUnits.appendChild(bcx.getDocument().createSVGRectElement(x+X, Y, grad, gradHeight, 0, 0));
        }
        else {
        	OMSVGPathSegList segs1 = gfxUnits.getPathSegList();
            segs1.appendItem(gfxUnits.createSVGPathSegMovetoAbs(x+X, Y));
            segs1.appendItem(gfxUnits.createSVGPathSegLinetoAbs(x+X, Y + sens * gradHeight));
        }
        
        String leg = Integer.toString(position);
        txtUnits.appendChild(bcx.getDocument().createSVGTextElement(x-(leg.length() * 3) + grad/2 + X, Y+20, (short) 5, leg));
    }

    /**
     * Axis and secondary objects
     *
     * @param table
     *            table of axis values
     */
    public void create(float[] table) {
    	   
        setVal(table);
        drawLine(X + gfxMax, Y);
        drawGraduations();
        drawLegend(X + gfxMax - 30, Y + 5);
    }
    
    /**
     * Axis and secondary objects (original version)
     *
     * @param begin
     *            First value of axis
     * @param f
     *            Last value of axis
     * @param step
     *            Step between two values
     * @param last
     *            Display the last or not
     */
    public void create(long begin, float f, long step, boolean last) {
    	    	    	
    	float table[] = new float[(int) ((f - begin) / step) + 2];
    	
    	int j = 0;
    	
    	for (long i = begin; i <= f; i += step, j++) {
    		
    		table[j] = i - 1;
    	}

            if (last)	{ table[table.length - 1] = f; }
                        
            create(table);
    }
    
    public void create(long begin, float end) {
	
        drawLine(X + gfxMax, Y);
        ArrayList<Integer> table = drawGraduation(begin, end);
        
        for ( int i = 0 ; i < table.size() ; i++ ) {
        	drawGraduation(table.get(i));
        }
    }
    
    public void create(long begin, float f, float step, boolean last) {
      	
      	step = ((float) Genoscapist.sizeSequence)/5;
      	
      	float table[] = new float[6];
      	
      	int j = 0;
      	
      	for (long i = begin; i <= f; i += step, j++) {
      		
      		table[j] = i;
      	}

              if (last)	{ table[table.length - 1] = f; }
              
              create(table);
      }
    
    /**
     * Axis and secondary objects
     *
     * @param begin
     *            First value of axis
     * @param f
     *            Last value of axis
     * @param step
     *            Step between two values
     * @param last
     *            Display the last or not
     */
    public void create(float f, boolean last) {
    	
    	Y = f; 	
    	drawLine(X + gfxMax, Y);      
    }
}