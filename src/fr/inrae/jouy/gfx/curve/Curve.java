/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.gfx.curve;

import org.vectomatic.dom.svg.OMSVGGElement;
import org.vectomatic.dom.svg.OMSVGPointList;
import org.vectomatic.dom.svg.OMSVGPolylineElement;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.OMSVGTransform;
import org.vectomatic.dom.svg.OMSVGTransformList;
import org.vectomatic.dom.svg.utils.SVGConstants;

import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.gfx.BioContext;

public class Curve {
	
	OMSVGGElement element;

	public Curve(int sens, int[] pos, double[] val, long from, float width, float startY, long basePerLine, OMSVGSVGElement canvas, BioContext bc, String color, String expName) {
	    
	    element = bc.getDocument().createSVGGElement();
	    
	    // ID
	    element.setId(expName+"_"+sens);
	
	    OMSVGPolylineElement polyline = new OMSVGPolylineElement();
	    OMSVGPointList points = polyline.getPoints();
	    
        float startX;
        float y;
        
        if ( pos != null ) {
        	
        	for (int i = 0 ; i < pos.length - 1 ; i++) {
        	
        		startX = ((pos[i] - from) * width) / basePerLine;
                int varNorm = -40;
              	
                if ( Genoscapist.appControler.normalization.contains("CustomCDS") == true ) {
                    varNorm = 0;
               	}
                
                if ( sens == -1 ) {
                	y = (float) (60 + startY + 100 - val[i] * 5 + varNorm);
                }
                else {
                	y = (float) (60 + startY - val[i] * 5 + varNorm);
                }
                                                
               	points.appendItem(canvas.createSVGPoint(startX+30, y));
            }
        }
        
        element.appendChild(polyline);

        element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, SVGConstants.CSS_NONE_VALUE);
        element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, color);
	}
	
	public Curve(String tmp, int sens, int[] pos, double[] val, long from, float width, float startY, long basePerLine, OMSVGSVGElement canvas, BioContext bc, String color, String expName) {
		
		float z = 0;
		
		z = startY + 60;
	    
	    element = bc.getDocument().createSVGGElement();
	    
	    // ID
	    element.setId(expName+"_"+sens);
	
	    OMSVGPolylineElement polyline = new OMSVGPolylineElement();
	    OMSVGPointList points = polyline.getPoints();
	    
	    from = from + basePerLine;
	    
	    float conv = width / basePerLine;
	    
        int startX;
        int y;

        OMSVGTransformList xforms2 = polyline.getTransform().getBaseVal();
        OMSVGTransform r1 = canvas.createSVGTransform();
        r1.setTranslate(30 - from * conv, z);
        xforms2.appendItem(r1);
        
        if ( pos != null ) {
        	
        	for (int i = 0 ; i < pos.length - 1 ; i++) {
        		
        		startX = (int) (((pos[i + 1] - from) * width) / basePerLine);
                        
              	int varNorm = -40;
              	
                if ( Genoscapist.appControler.normalization.contains("CustomCDS") == true ) {
                        	
                	varNorm = 0;
               	}
                        
             	if ( sens ==  -1 ) {
             		y = (int) (100 - (val[i + 1]) * 5 + varNorm);
               	}
              	else {
              		y = (int) (val[i + 1] * -5 + varNorm);
            	}
                                                
               	points.appendItem(canvas.createSVGPoint(startX, y));
            }
        }
        
        element.appendChild(polyline);

        element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, SVGConstants.CSS_NONE_VALUE);
        element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, color);
	}
	
	public Curve(String tmp, String save, int sens, int[] pos, double[] val, long from, float width, float startY, long basePerLine, OMSVGSVGElement canvas, BioContext bc, String color, String expName) {
		
		float z = 0;
		
		z = startY + 60;
	    
	    element = bc.getDocument().createSVGGElement();
	    
	    // ID
	    element.setId(expName+"_"+sens);
	
	    OMSVGPolylineElement polyline = new OMSVGPolylineElement();
	    OMSVGPointList points = polyline.getPoints();
	    
	    float conv = width / basePerLine;
	    
        int startX;
        int y;

        OMSVGTransformList xforms2 = polyline.getTransform().getBaseVal();
        OMSVGTransform r1 = canvas.createSVGTransform();
        r1.setTranslate(30 - from * conv, z);
        xforms2.appendItem(r1);
        
        if ( pos != null ) {
        	
        	for (int i = 0 ; i < pos.length - 1 ; i++) {
        	
            	startX = (int) ((pos[i + 1]) * conv);
                        
              	int varNorm = -40;
              	
                if ( Genoscapist.appControler.normalization.contains("CustomCDS") == true ) {
                        	
                	varNorm = 0;
               	}
                        
             	if ( sens ==  -1 ) {
             		y = (int) (100 - (val[i + 1]) * 5 + varNorm);
               	}
              	else {
              		y = (int) (val[i + 1] * -5 + varNorm);
            	}
                                                
               	points.appendItem(canvas.createSVGPoint(startX, y));
            }
        }
        
        element.appendChild(polyline);

        element.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, SVGConstants.CSS_NONE_VALUE);
        element.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, color);
	}
	
	public OMSVGGElement getCurve() {
		return element;
	}
	
	public OMSVGSVGElement getCurve(OMSVGSVGElement canvas, String id) {
		
		OMSVGSVGElement test = (OMSVGSVGElement) canvas.getElementById(id);
		
		return test;
	}
	
	public void deleteCurve() {
		
		element.getElement().removeAllChildren();
	}
}