/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.gfx.curve;

import java.util.ArrayList;

import org.vectomatic.dom.svg.OMSVGElement;
import org.vectomatic.dom.svg.OMSVGPathElement;
import org.vectomatic.dom.svg.OMSVGPathSegList;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.utils.SVGConstants;


import fr.inrae.jouy.gfx.BioContext;

public abstract class Axe {

	/** Color **/
	protected String color = "rgb(0,0,0)";
	/** Graduation size **/
	protected int gradHeight = 4;
	/** X, Y positions of axis **/
	public float X, Y;
	/** Min. size of axis **/
	protected float gfxMax;
	/** Title of values **/
	protected String legend;
	/** Table of values **/
	protected float[] val;
	/** Table of legends associated to axis values **/
	protected String[] legVal;
	/** Min. value for axis **/
	protected float min;
	/** Max. value for axis **/
	protected float max;

	protected BioContext bcx;

	/** Graphics units for axis */
	protected OMSVGPathElement gfxUnits = null;
	protected OMSVGElement txtUnits = null;
	protected OMSVGElement rectUnits = null;

	/**
	 * Initialization of default values
	 * 
	 * @param x
	 *            x position of axis
	 * @param y
	 *            y position of axis
	 * @param widthWithoutMargin
	 *            axis size
	 **/
	public Axe(BioContext bc, float x, float y, float widthWithoutMargin) {

		bcx = bc;
		X = x;
		Y = y;
		gfxMax = widthWithoutMargin;
		gfxUnits = bcx.getDocument().createSVGPathElement();
		txtUnits = bcx.getDocument().createSVGGElement();
		rectUnits = bcx.getDocument().createSVGGElement();
	}

	/**
	 * Initialization of default values
	 * 
	 * @param x
	 *            x position of axis
	 * @param y
	 *            y position of axis
	 * @param width
	 *            axis size
	 * @param legend
	 *            axis legend
	 **/
	public Axe(BioContext bc, float x, float y, int width, String legend) {

		this(bc, x, y, width);
		setLegend(legend);
	}

	/**
	 * Initialization of the legend
	 * 
	 * @param l
	 *            legend text
	 **/
	public void setLegend(String l) {
		legend = l;
	}

	/**
	 * Legend
	 * 
	 * @return legend title
	 **/
	public String getLegend() {
		return legend;
	}

	/**
	 * Return the position of axis point
	 * 
	 * @param the
	 *            point to be placed
	 * @return the position of 'val' point
	 **/
	public float getPos(float val) {

		return (val - min) / bcx.getBpPerPixel();
	}

	/**
	 * Initialization of values to display & recuperation of min and max of table Et
	 * recuperation du min et du max dans le tableau.
	 * 
	 * @param table
	 *            table of values to display on axis
	 **/
	public void setVal(float[] table) {

		val = table;
		min = val[0];
		max = val[val.length - 1];
	}

	/**
	 * Initialization of values to display with legends
	 * 
	 * @param table
	 *            table of values to display on axis
	 * @param legend
	 *            table of legends
	 **/
	public void setVal(float[] table, String[] legend) {

		setVal(table);
		legVal = legend;
	}

	/**
	 * Display the right of this axis
	 * 
	 * @param x
	 *            position en x
	 * @param y
	 *            position en y
	 **/
	protected void drawLine(float x, float y) {

		OMSVGPathSegList segs1 = gfxUnits.getPathSegList();

		segs1.appendItem(gfxUnits.createSVGPathSegMovetoAbs(X, Y));
		segs1.appendItem(gfxUnits.createSVGPathSegLinetoAbs(x, y));
	}

	/** Display axis graduations **/
	protected void drawGraduations() {

		drawGraduation(24, 297);

		String leg;

		for (int i = 0; i < val.length; i++) {

			if (legVal == null) {
				leg = String.valueOf((int) val[i]);
			} else {
				leg = legVal[i];
			}

			drawGraduation(val[i], leg);
		}
	}

	protected ArrayList<Integer> drawGraduation(long x0, float x1) {

		double xstep_10 = Math.pow(10, Math.floor(Math.log10(x1 - x0)));
		double nticks_10_mean = (x1 - x0 + 1) / xstep_10;
		int nticks_target = 5;
		double[] adj_fact = new double[] { 2, 1, 0.5, 0.2 };
		double adj_ichoose_ref = 10000000;
		double adj_ichoose = 0;
		for (int i = 0; i < adj_fact.length; i++) {

			double tmp = Math.abs((nticks_10_mean / adj_fact[i]) - nticks_target);

			if (tmp < adj_ichoose_ref) {

				adj_ichoose_ref = tmp;
				adj_ichoose = i;
			}
		}

		double xstep_adj = xstep_10 * adj_fact[(int) adj_ichoose];
		double xtick0_adj = Math.ceil(x0 / xstep_adj) * xstep_adj;
		double nintervals_adj = Math.floor((x1 - xtick0_adj) / xstep_adj);

		ArrayList<Integer> txticks = new ArrayList<Integer>();
		
		// 1st position
		txticks.add((int) x0);
		
		for (int i = 0; i <= nintervals_adj; i++) {
			txticks.add((int) (xtick0_adj + i * xstep_adj));
		}

		// Last position
		if ( txticks.get((int) nintervals_adj + 1) != (int) x1 )	{ txticks.add((int) x1); }
		
		return txticks;
	}

	/**
	 * Method for the display of graduations
	 * 
	 * @param v
	 *            value of this point
	 * @param l
	 *            legend of this point
	 **/
	protected abstract void drawGraduation(float v, String l);

	/**
	 * Display Legend axis
	 * 
	 * @param x
	 *            position en x
	 * @param y
	 *            position en y
	 **/
	public void drawLegend(float x, float y) {

		if (legend != null) {
			txtUnits.appendChild(bcx.getDocument().createSVGTextElement(x, y, (short) 5, legend));
		}
	}

	/**
	 * Draw all elements of axis
	 * 
	 * @param gfx
	 *            Graphics
	 **/
	public void paint(OMSVGSVGElement canvas) {

		gfxUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY, color);
		gfxUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
		rectUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY, color);
		rectUnits.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, "white");
		canvas.appendChild(gfxUnits);
		canvas.appendChild(txtUnits);
		canvas.appendChild(rectUnits);
	}

	public void paint(OMSVGSVGElement canvas, String color) {

		gfxUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY, color);
		gfxUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
		rectUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY, color);
		rectUnits.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, "white");
		canvas.appendChild(gfxUnits);
		canvas.appendChild(txtUnits);
		canvas.appendChild(rectUnits);
	}

	public void paint(OMSVGSVGElement canvas, String color, String dasharray, String id) {

		gfxUnits.setId(id);
		gfxUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY, color);
		gfxUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
		gfxUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_DASHARRAY_PROPERTY, dasharray);
		rectUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY, color);
		rectUnits.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, "white");
		canvas.appendChild(gfxUnits);
		canvas.appendChild(txtUnits);
		canvas.appendChild(rectUnits);
	}
}