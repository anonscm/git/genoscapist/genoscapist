/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.header;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import fr.inrae.jouy.client.Config;
import fr.inrae.jouy.client.content.ContentView;

public class HeaderView extends Composite {

	private static HeaderUiBinder uiBinder = GWT.create(HeaderUiBinder.class);

	interface HeaderUiBinder extends UiBinder<Widget, HeaderView> {
	}

	public interface ImageResources extends ClientBundle {

        public static final ImageResources INSTANCE = GWT.create(ImageResources.class);
        
        @Source("home_seb.png")
        ImageResource imgSEBStart();

        @Source("supplementary_seb.png")
        ImageResource imgSEBSupp();
        
        @Source("home_aeb.png")
        ImageResource imgAEBStart();

        @Source("supplementary_aeb.png")
        ImageResource imgAEBSupp();
        
	}
	
	@UiField Image logoStart;
	@UiField Image logoSupp;
	
	@UiField HTML header;
	
	@UiHandler("logoSupp")
    void onLogoSuppClick(ClickEvent e) {
    	
		 ContentView.mainContentView.showWidget(2);
		 History.newItem("&supplementaryData");
	}
	
	@UiHandler("logoStart")
    void onLogoAEBClick(ClickEvent e) {
    	
		 History.newItem("");
		 ContentView.mainContentView.showWidget(0);
    }
	
	public HeaderView() {
		
		initWidget(uiBinder.createAndBindUi(this));
		header.setHTML(Config.name);
		
		if ( Config.abbr == "seb" || Config.abbr == "seb_min" ) {
			logoStart.setResource(ImageResources.INSTANCE.imgSEBStart());
		}
		else {
			logoStart.setResource(ImageResources.INSTANCE.imgAEBStart());
		}

		logoStart.addMouseOverHandler(new MouseOverHandler() {
            public void onMouseOver(MouseOverEvent event) {
                    logoStart.setStyleName("gwt-Image-clikableImg");
            }
        });
		
        logoStart.setTitle("Start Page");
        logoStart.setWidth("80px"); logoStart.setHeight("80px"); // 72px
        
        if ( Config.abbr == "seb" || Config.abbr == "seb_min" ) {
        	logoSupp.setResource(ImageResources.INSTANCE.imgSEBSupp());
		}
		else {
			logoSupp.setResource(ImageResources.INSTANCE.imgAEBSupp());
		}
        
		logoSupp.addMouseOverHandler(new MouseOverHandler() {
            public void onMouseOver(MouseOverEvent event) {
                    logoSupp.setStyleName("gwt-Image-clikableImg");
            }
        });
		
		logoSupp.setTitle("Supplementary Data");
		logoSupp.setWidth("80px"); logoSupp.setHeight("80px"); // 72px
	}
}