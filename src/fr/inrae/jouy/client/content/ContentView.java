/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.vectomatic.dom.svg.OMElement;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.OMSVGTextElement;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.IdentityColumn;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.ListDataProvider;

import fr.inrae.jouy.client.Config;
import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.client.RPCall.CallForBDInfo;
import fr.inrae.jouy.client.RPCall.CallForBDInfoAsync;
import fr.inrae.jouy.client.map.PhysicalMap;
import fr.inrae.jouy.client.map.action.MapEvent;
import fr.inrae.jouy.shared.Cluster;
import fr.inrae.jouy.shared.Experience;
import fr.inrae.jouy.shared.GeneItem;
import fr.inrae.jouy.shared.Sequence;
import fr.inrae.jouy.shared.Species;

public class ContentView extends Composite {

	private static ContentViewUiBinder uiBinder = GWT.create(ContentViewUiBinder.class);

	interface ContentViewUiBinder extends UiBinder<Widget, ContentView> {
	}
	
	public interface CellTableResource extends CellTable.Resources {
        public interface CellTableStyle extends CellTable.Style {};

        @Override
        @Source( { CellTable.Style.DEFAULT_CSS, "CellTable.css" })
        CellTableStyle cellTableStyle();
	};
	
	// Images 
	public interface ImageResources extends ClientBundle {

		public static final ImageResources INSTANCE = GWT.create(ImageResources.class);
	        
		@Source("big_straf_left_aeb.png")
		ImageResource imgBigLeftAEB();		
		@Source("small_straf_left_aeb.png")
		ImageResource imgSmallLeftAEB();
		@Source("big_straf_right_aeb.png")
		ImageResource imgBigRightAEB();		
		@Source("small_straf_right_aeb.png")
		ImageResource imgSmallRightAEB();
		@Source("link_aeb.png")
		ImageResource imgShareAEB();
		@Source("zoom_small_in_aeb.png")
		ImageResource imgSmallInAEB();
		@Source("zoom_in_aeb.png")
		ImageResource imgInAEB();
		@Source("zoom_neutral_aeb.png")
		ImageResource imgNeutralAEB();
		@Source("zoom_small_out_aeb.png")
		ImageResource imgSmallOutAEB();
		@Source("zoom_out_aeb.png")
		ImageResource imgOutAEB();
		
		@Source("big_straf_left_seb.png")
		ImageResource imgBigLeftSEB();		
		@Source("small_straf_left_seb.png")
		ImageResource imgSmallLeftSEB();
		@Source("big_straf_right_seb.png")
		ImageResource imgBigRightSEB();		
		@Source("small_straf_right_seb.png")
		ImageResource imgSmallRightSEB();
	    @Source("link_seb.png")
	    ImageResource imgShareSEB();
		@Source("zoom_small_in_seb.png")
		ImageResource imgSmallInSEB();
		@Source("zoom_in_seb.png")
		ImageResource imgInSEB();
		@Source("zoom_neutral_seb.png")
		ImageResource imgNeutralSEB();
		@Source("zoom_small_out_seb.png")
		ImageResource imgSmallOutSEB();
		@Source("zoom_out_seb.png")
		ImageResource imgOutSEB();
	}
	
	@UiField public static DeckPanel mainContentView;

	// Table
	@UiField static CellTable<GeneItem> table;
	
	// HTML
	@UiField static HTML geneselected;
	@UiField static HTML detailsHighCondition;
	@UiField static HTML detailsLowCondition;
	@UiField static HTML detailsHighSegment;
	@UiField static HTML detailsLowSegment;
	@UiField static HTML genbank;
	@UiField static HTML information;
	@UiField static HTML profile;
	@UiField static HTML annotation;
	@UiField static HTML view;
	@UiField public static HTML testImage;
	@UiField public static HTML html_suppData;
	
	// Buttons
	@UiField static Button buttonSample;
	@UiField static Button buttonNormalization;
	@UiField static Button buttonRho;
	@UiField static Button buttonAddFeatureName;
	@UiField static Button buttonLegend;
	
	// Images
	@UiField public static Image png;
	@UiField public static Image imgBefore;
	@UiField public static Image imgBefore5000;
	@UiField public static Image imgAfter;
	@UiField public static Image imgAfter5000;	
	@UiField public static Image imgSmallMinus;
	@UiField public static Image imgBigMinus;
	@UiField public static Image imgNeutral;
	@UiField public static Image imgSmallPlus;
	@UiField public static Image imgBigPlus;
	@UiField public static Image imgSharing;
	
	// Sections
	@UiField public static DockPanel viewDock;
	@UiField public static DockPanel viewDockCourbe;
	@UiField public static DockPanel viewDockCourbeRho;
	@UiField public static DisclosurePanel disclosurePanelSample;
	
	// SuggestBox
	@UiField public static VerticalPanel identifier;
	@UiField public static VerticalPanel cluster;
	@UiField public static VerticalPanel position;
	@UiField public static Button buttonPosition;

	/** Button Sample **/
    @UiHandler("buttonSample")
	void onRefreshClickSample(ClickEvent event) {
            
    	if ( disclosurePanelSample.isOpen() != true ) {
	        disclosurePanelSample.setOpen(true);
     	}
            
       	else {
            disclosurePanelSample.setOpen(false);
       	}
    }
    
    /** Button Position **/
    @UiHandler("buttonPosition")
    void onRefreshClickbuttonPosition(ClickEvent event) {
    	
        selectPosition(textboxPosition.getValue());
	}
    
    /** Button Normalization **/
    @UiHandler("buttonNormalization")
  	void onRefreshClickNormalization(ClickEvent event) {
    
    	int position_vbar = -1;
        
    	// Vertical Bar (mark)
    	if ( physicalMap.canvas.getElementById("repere_g") != null ) {
    		
    		String c = ((OMElement) physicalMap.canvas.getElementById("pos_g")).getElement().getInnerHTML();
    		
      		position_vbar = Integer.parseInt(c);
    		Genoscapist.appControler.setVbarPosition(position_vbar);
	  	}
	    	
	  	if ( physicalMap.canvas2.getElementById("repere_c") != null ) {
	    	physicalMap.canvas2.getElementById("repere_c").getElement().removeFromParent();
	 	}
	    	
	  	if ( Genoscapist.appControler.isRhoActive() == true && physicalMap.canvas3.getElementById("repere_r") != null ) {
			physicalMap.canvas3.getElementById("repere_r").getElement().removeFromParent();
		}
            
      	if ( Genoscapist.appControler.getNormalization().equalsIgnoreCase("CustomCDS") == true ) {
            	
        	Genoscapist.appControler.setNormalization("Median");
         	buttonNormalization.setHTML("Switch to CDS-Quantile normalisation");
     	}
            
    	else {
            	
    		Genoscapist.appControler.setNormalization("CustomCDS");
         	buttonNormalization.setHTML("Switch to Median normalisation");
       	}
            
        AsyncCallback<List<Experience>> callback = new AsyncCallback<List<Experience>>() {
                
        	public void onFailure(Throwable caught) {
	            Window.alert("RPC callForBDInfoService.getExpRefreshNorm failed : " + caught.getMessage());
           	}
                
         	public void onSuccess(List<Experience> result) {
                	
         		Genoscapist.appControler.setListAllSelectedExperience(result);
         		
         		viewDockCourbe.getElement().removeAllChildren();
            	physicalMap.canvas2.getElement().removeAllChildren();
            	
            	if ( Genoscapist.appControler.rhoActive == true ) {
                	viewDockCourbeRho.getElement().removeAllChildren();
                  	physicalMap.canvas3.getElement().removeAllChildren();
              	}
                    
            	Scheduler.get().scheduleDeferred(new ScheduledCommand() {

            		@Override
        			public void execute() {
        	            	
        	        	physicalMap.paintCourbe();
        	            		
        	           	Scheduler.get().scheduleDeferred(new ScheduledCommand() {

    						@Override
    						public void execute() {
    									
    							if ( Genoscapist.appControler.rhoActive == true ) {
    										
    								physicalMap.paintCourbeRho();
    										
    								// SVG
    								Scheduler.get().scheduleDeferred(new ScheduledCommand() {

    		    						@Override
    		    						public void execute() {
    		    							
    		    							createSVGImage();
    		    							
    		    							// Vertical Bar
    		    					      	if ( Genoscapist.appControler.getVbarPosition() > -1)	{ physicalMap.createRepere(Genoscapist.appControler.getVbarPosition(), true); }
    		    						}
    		    		        	});
    							}
    							
    							else {
    								
    								// Vertical Bar
	    					      	if ( Genoscapist.appControler.getVbarPosition() > -1)	{ physicalMap.createRepere(Genoscapist.appControler.getVbarPosition(), true); }
    							}
    						}
        	           	});
            		}
        		});
         	}
     	};
            
      	try {
        	callForBDInfoService.getExpRefreshNorm(Genoscapist.appControler.listAllSelectedExperience, Genoscapist.appControler.getNormalization(), callback);
      	} catch (Exception e1) {
    	   	Window.alert("ERROR callForBDInfoService.getExpRefreshNorm : " + e1.getMessage());
    	}
    }
    
    /** Button Rho **/
    @UiHandler("buttonRho")
	void onRefreshClickRho(ClickEvent event) {

    	if ( Genoscapist.appControler.rhoActive == false ) {
            
    		int position_vbar = -1;
            	
        	Genoscapist.appControler.setRhoActive(true);
        	buttonRho.setHTML("Remove Rho samples track");
            	
        	// Vertical Bar position
          	if ( physicalMap.canvas.getElementById("repere_g") != null ) {
          		
          		String c = ((OMElement) physicalMap.canvas.getElementById("pos_g")).getElement().getInnerHTML();
          		position_vbar = Integer.parseInt(c);
        		Genoscapist.appControler.setVbarPosition(position_vbar);
          	}
            	
          	physicalMap.paintCourbeRho();
            	
         	if ( position_vbar > -1)	{ physicalMap.createRepere(position_vbar, true); }
       	}
     	else {
            	
        	Genoscapist.appControler.setRhoActive(false);
        	buttonRho.setHTML("Add Rho samples track");
            	
        	viewDockCourbeRho.getElement().removeAllChildren();
            	
	        physicalMap.canvas3.getElement().removeAllChildren();
          	createSVGImage();
            }
    	}
    
    /** Button Add Features Names **/
    @UiHandler("buttonAddFeatureName")
  	void onRefreshClickAddFeatureName(ClickEvent event) {
    	
    	if ( Genoscapist.appControler.addFeatureNameActive == false) {
    			
    		Genoscapist.appControler.setAddFeatureNameActive(true);
    		buttonAddFeatureName.setHTML("Remove Feature Names");
    			
    		physicalMap.addFeaturesNames();
    	}
    		
    	else {
    			
    		Genoscapist.appControler.setAddFeatureNameActive(false);
           	buttonAddFeatureName.setHTML("Add Feature Names");
            	
          	physicalMap.removeFeaturesNames();
          	createSVGImage();
    	}
    }
    
    /** Button Sharing **/
    @UiHandler("imgSharing")
  	void onRefreshClickSharingImg(ClickEvent event) {
       	showShareDialog();
	}
    
    /** Button Zoom * 2 **/
    @UiHandler("imgBigMinus")
    void onRefreshClickBigMinusImg(ClickEvent event) {
        	    	
    	Genoscapist.sizeSequence = Genoscapist.sizeSequence * 2;
    	checkSeqSize();
    	resize();
	}
    
    /** Button Zoom * 1.5 **/
    @UiHandler("imgSmallMinus")
	void onRefreshClickSmallMinusImg(ClickEvent event) {
        	    	
    	Genoscapist.sizeSequence = (int) (Genoscapist.sizeSequence * 1.5);
    	checkSeqSize();
    	resize();
    }
    
    /** Button Zoom (10000) **/
    @UiHandler("imgNeutral")
	void onRefreshClickNeutralImg(ClickEvent event) {
	
    	if ( Genoscapist.sizeSequence != 10000 ) {
    		
			Genoscapist.sizeSequence = 10000;
			checkSeqSize();
    		resize();
		}
	}
    
    /** Button Zoom * 0.75 **/
    @UiHandler("imgSmallPlus")
	void onRefreshClickSmallPlusImg(ClickEvent event) {

    	Genoscapist.sizeSequence = (int) (Genoscapist.sizeSequence * 0.75);
    	checkSeqSize();
		resize();
    }
    
    /** Button Zoom * 0.5 **/
    @UiHandler("imgBigPlus")
    void onRefreshClickBigPlusImg(ClickEvent event) {

    	Genoscapist.sizeSequence = (int) (Genoscapist.sizeSequence * 0.5);
		checkSeqSize();
		resize();
    }
    
    /** Button Legend **/
    @UiHandler("buttonLegend")
    void onRefreshClickLegend(ClickEvent event) {
    	
    	if ( Genoscapist.appControler.rhoActive == false ) {           
        	Window.open(Config.url_img+Config.abbr+"/images/GenomeSectionLegend.svg","_blank","");
    	}
        else {
        	Window.open(Config.url_img+Config.abbr+"/images/GenomeSectionLegendRho.svg", "_blank", "");
        }
    };

	/** Button Before **/
    @UiHandler("imgBefore")
	void onRefreshClickBeforeImg(ClickEvent event) {

    	// URL
    	History.newItem("", false);
        move("before");
	}
    
    /** Button After **/
    @UiHandler("imgAfter")
   	void onRefreshClickAfterImg(ClickEvent event) {

    	// URL
    	History.newItem("", false);
    	move("after");
	}
    
    /** Button Before 5000 **/
    @UiHandler("imgBefore5000")
	void onRefreshClickBefore5000Img(ClickEvent event) {

    	// URL
    	History.newItem("", false);
		move("beforeSmall");
	}
    
    /** Button After 5000 **/
    @UiHandler("imgAfter5000")
    void onRefreshClickAfter5000Img(ClickEvent event) {

    	// URL
    	History.newItem("", false);
    	move("afterSmall");
 	}
    
	// SuggestBox Identifier
	public static SuggestBox suggestboxIdentifier = null;
    public static MultiWordSuggestOracle oracleIdentifier = new MultiWordSuggestOracle();
	
	// SuggestBox Cluster
	public static SuggestBox suggestboxCluster = null;
    public static MultiWordSuggestOracle oracleCluster = new MultiWordSuggestOracle();
	
    // TextBox Position
    public static TextBox textboxPosition = null;
    
	// Genes table
	public static ListDataProvider<GeneItem> geneitemlist = new ListDataProvider<GeneItem>(Genoscapist.appControler.getListAllGeneItem());
	
	public static List<Sequence> seqList = null;
	private final static CallForBDInfoAsync callForBDInfoService = (CallForBDInfoAsync) GWT.create(CallForBDInfo.class);
	public static PhysicalMap physicalMap;
	
	// Waiting Popup
	public static AppLoadingView msg = new AppLoadingView();
	
	public ContentView() {
		
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	/** SVG Download **/
	public static void createSVGImage() {
				
		// Expression profiles view
   		OMSVGSVGElement canvastmp = new OMSVGSVGElement();
   		canvastmp.getElement().setInnerHTML(physicalMap.canvas2.getElement().getInnerHTML());
   		
   		// Min. & Max. axis : +400
   		ArrayList<String> axes = new ArrayList<String>();
   		axes.add("axeMinP"); axes.add("axeMinM"); axes.add("axeMaxP"); axes.add("axeMaxM");
   		for ( int i = 0 ; i < axes.size() ; i++ ) {
   			
   			String d = ((OMElement) canvastmp.getElementById(axes.get(i))).getAttribute("d");
   	   		d = d.replaceAll("M 30 ", ""); d = d.replaceAll(" L .+", "");
   	   		int newP = Integer.parseInt(d);
   	   		canvastmp.getElementById(axes.get(i)).setAttribute("d", "M 30 "+(newP+400)+" L 1170 "+(newP+400));
   		}
   		
   		// CustomCDS axis
   		if ( Genoscapist.appControler.normalization.contains("CustomCDS") == true ) {
	   		canvastmp.getElementById("axe1").setAttribute("d", "M 30 570 L 1170 570");
	   		canvastmp.getElementById("axe2").setAttribute("d", "M 30 565 L 1170 565");
	   		canvastmp.getElementById("axe3").setAttribute("d", "M 30 470 L 1170 470");
	   		canvastmp.getElementById("axe4").setAttribute("d", "M 30 465 L 1170 465");
   		}
   		// MedianNorm axis
   		else {
   			canvastmp.getElementById("axe1").setAttribute("d", "M 30 580 L 1170 580");
	   		canvastmp.getElementById("axe2").setAttribute("d", "M 30 568 L 1170 568");
	   		canvastmp.getElementById("axe3").setAttribute("d", "M 30 563 L 1170 563");
	   		canvastmp.getElementById("axe4").setAttribute("d", "M 30 468 L 1170 468");
	   		canvastmp.getElementById("axe5").setAttribute("d", "M 30 463 L 1170 463");
	   		canvastmp.getElementById("axe6").setAttribute("d", "M 30 480 L 1170 480");
   		}
   		float vtrans = 30 - Genoscapist.appControler.getStart_view() * (PhysicalMap.widthWithoutMargin / Genoscapist.sizeSequence);
   		
   		// Expression profiles
   		for ( int i = 0 ; i < Genoscapist.appControler.listAllSelectedExperience.size() ; i++ ) {
   			
   			String exp_name = Genoscapist.appControler.listAllSelectedExperience.get(i).getExp();
   			int strand = Genoscapist.appControler.listAllSelectedExperience.get(i).getStrand();
   			   			
   			OMElement ecurve = (OMElement) canvastmp.getElementById(exp_name+"_"+strand).getFirstChild();
   			String points[] = ecurve.getAttribute("points").split(" ");
   			String npoints = points[0];
   			for ( int p = 1 ; p < points.length ; p++ ) {
   				if (p % 2 == 0) { npoints = npoints + " " + points[p]; }
   				else 			{ float newP = Float.parseFloat(points[p]) + 400;
   								  npoints = npoints + " " + Float.toString(newP); }
   			}
   			ecurve.setAttribute("points", npoints);
   		}
   		
   		// Regions without data
   		int m = 1;
		String name = "rectangle_"+m;
		
		while ( (OMElement) canvastmp.getElementById(name) != null ) {
			
			m++;			
			OMElement testPlus1 = (OMElement) canvastmp.getElementById(name).getFirstChild();
	   		int pos1 = Integer.parseInt(testPlus1.getAttribute("y")) + 400;
			testPlus1.setAttribute("y", Integer.toString(pos1));			
			name = "rectangle_"+m;
		}
		
		// First block
		if ( (OMElement) canvastmp.getElementById("rectangle_first") != null ) {
			OMElement testFirst = (OMElement) canvastmp.getElementById("rectangle_first").getFirstChild();
	   		int posFirst = Integer.parseInt(testFirst.getAttribute("y")) + 400;
			testFirst.setAttribute("y", Integer.toString(posFirst));
		}
		
		// Last block
		if ( (OMElement) canvastmp.getElementById("rectangle_last") != null ) {
			OMElement testLast = (OMElement) canvastmp.getElementById("rectangle_last").getFirstChild();
	   		int posLast = Integer.parseInt(testLast.getAttribute("y")) + 400;
	   		testLast.setAttribute("y", Integer.toString(posLast));
		}
   		
   		// Vertical bar
		if ( (OMElement) canvastmp.getElementById("repere_c") != null ) {
	   		String r = ((OMElement) canvastmp.getElementById("repere_c")).getAttribute("d");
	   		String r1 = r.split("M ")[1];
	   		r1 = r1.replaceAll(" 0 L .+", ""); r1 = r1.replaceAll("M ", "");
	   		int newR1 = Integer.parseInt(r1);
	   		String r2 = r.split("M ")[2];
	   		r2 = r2.replaceAll(" 0 L .+", ""); r2 = r2.replaceAll("M ", "");
	   		int newR2 = Integer.parseInt(r2);
	   		canvastmp.getElementById("repere_c").setAttribute("d", "M " + newR1 + " 400 L " + newR1 + " 660 M " + newR2 + " 400 L " + newR2 + " 660");
		}
		
		OMSVGSVGElement canvastmprho = null;
		// Rho
		if ( Genoscapist.appControler.rhoActive == true ) {
			
			// Rho expression profiles
	   		canvastmprho = new OMSVGSVGElement();
	   		canvastmprho.getElement().setInnerHTML(physicalMap.canvas3.getElement().getInnerHTML());
			
	   		final ArrayList<Integer> abs = new ArrayList<Integer>();
	   		abs.add(60); 
	   		if ( Config.abbr == "aeb"  ) {
	   			abs.add(280); abs.add(500); abs.add(720);
	   		}
   	    	
	   		for ( int i = 0 ; i < abs.size() ; i++ ) {
	   			
	   			// Min. & Max. axis : +200
	   	   		for ( int i1 = 0 ; i1 < axes.size() ; i1++ ) {
	   	   			
	   	   			String d = ((OMElement) canvastmprho.getElementById(axes.get(i1)+"_"+abs.get(i))).getAttribute("d");
	   	   	   		d = d.replaceAll("M 30 ", ""); d = d.replaceAll(" L .+", "");
	   	   	   		int newP = Integer.parseInt(d);
	   	   	   		canvastmprho.getElementById(axes.get(i1)+"_"+abs.get(i)).setAttribute("d", "M 30 "+(newP+660)+" L 1170 "+(newP+660));
	   	   		}
   	    		
   	    		// Axis - CustomCDS
		   		if ( Genoscapist.appControler.normalization.contains("CustomCDS") == true ) {
		   			
		   			int value = 0;
	   	    		value = abs.get(i) + 570 + 200; canvastmprho.getElementById("rhoaxe1_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
	   	    		value = abs.get(i) + 565 + 200; canvastmprho.getElementById("rhoaxe2_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
	   	    		value = abs.get(i) + 470 + 200; canvastmprho.getElementById("rhoaxe3_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
	   	    		value = abs.get(i) + 465 + 200; canvastmprho.getElementById("rhoaxe4_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);	   		
		   		}
		   		// Axis - MedianNorm
		   		else {
		   			
		   			int value = 0;
		   			value = abs.get(i) + 580 + 200; canvastmprho.getElementById("rhoaxe1_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
		   			value = abs.get(i) + 568 + 200; canvastmprho.getElementById("rhoaxe2_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
		   			value = abs.get(i) + 563 + 200; canvastmprho.getElementById("rhoaxe3_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
		   			value = abs.get(i) + 468 + 200; canvastmprho.getElementById("rhoaxe4_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
		   			value = abs.get(i) + 463 + 200; canvastmprho.getElementById("rhoaxe5_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
		   			value = abs.get(i) + 480 + 200; canvastmprho.getElementById("rhoaxe6_"+abs.get(i)).setAttribute("d", "M 30 " + value + " L 1170 "+ value);
		   		}
   	    	}
	   		
	   		// Rho expression profiles
	   		for ( int i = 0 ; i < Genoscapist.appControler.getListExpRho().size() ; i++ ) {
	   			
	   			String exp_name = Genoscapist.appControler.getListExpRho().get(i).getExp();
	   			int strand = Genoscapist.appControler.getListExpRho().get(i).getStrand();
	   			
	   			OMElement ecurve = (OMElement) canvastmprho.getElementById(exp_name+"_"+strand).getFirstChild();
	   			String points[] = ecurve.getAttribute("points").split(" ");
	   			String npoints = points[0];
	   			for ( int p = 1 ; p < points.length ; p++ ) {
	   				if (p % 2 == 0) { npoints = npoints + " " + points[p]; }
	   				else 			{ float newP = Float.parseFloat(points[p]) + 660;
	   								  npoints = npoints + " " + Float.toString(newP); }
	   			}
	   			ecurve.setAttribute("points", npoints);	   			
	   		}
	   		
	   		// Regions without data
	   		for ( int i = 0 ; i < abs.size() ; i++ ) {
	   			
		   		int m1 = 1;
				String name1 = "rectangle_"+m1+"_"+abs.get(i);
				
				while ( (OMElement) canvastmprho.getElementById(name1) != null ) {
					
					m1++;			
					OMElement test = (OMElement) canvastmprho.getElementById(name1).getFirstChild();
			   		int pos1 = Integer.parseInt(test.getAttribute("y")) + 660;
					test.setAttribute("y", Integer.toString(pos1));			
					name1 = "rectangle_"+m1+"_"+abs.get(i);
				}
	   		}
	   		
	   		// Blocks
	   		for ( int i = 0 ; i < abs.size() ; i++ ) {
	   			
	   			// First block
				OMElement testFirstRho = (OMElement) canvastmprho.getElementById("rho_rectangle_first_"+(abs.get(i)-30)).getFirstChild();
		   		int posFirstRho = Integer.parseInt(testFirstRho.getAttribute("y")) + 660;
		   		testFirstRho.setAttribute("y", Integer.toString(posFirstRho));
				
				// Last block
				OMElement testLastRho = (OMElement) canvastmprho.getElementById("rho_rectangle_last_"+(abs.get(i)-30)).getFirstChild();
		   		int posLastRho = Integer.parseInt(testLastRho.getAttribute("y")) + 660;
		   		testLastRho.setAttribute("y", Integer.toString(posLastRho));
	   		}
	   		
	   		// Regions UpExpressed
	   		if ( Config.abbr == "aeb" && Genoscapist.appControler.getListRegionRho() != null ) {
	   				   			
		   		for ( int i = 0 ; i < Genoscapist.appControler.getListRegionRho().size() ; i++ ) {
		   			
		   			String region_name = "rectangle_"+Genoscapist.appControler.getListRegionRho().get(i).getName();
		   			
		   			if ( ((OMElement) canvastmprho.getElementById(region_name)).getFirstChild().toString().contains("rect") == true || ((OMElement) canvastmprho.getElementById(region_name)).getFirstChild().toString().contains("SVGRect") == true ) {
		   				
		   				OMElement test = (OMElement) canvastmprho.getElementById(region_name).getFirstChild();
			   			int pos1 = Integer.parseInt(test.getAttribute("y")) + 660;
			   			test.setAttribute("y", Integer.toString(pos1));
			   			
			   			if ( ((OMElement) canvastmprho.getElementById(region_name)).getLastChild().toString().contains("text") == true ) {
				   			
				   			OMSVGTextElement test1 = (OMSVGTextElement) canvastmprho.getElementById(region_name).getLastChild();		
				   			String px = test1.getAttribute("y"); // 460px
				   			px = px.replaceAll("px", ""); int n = Integer.parseInt(px) + 660;		   			
				   			test1.setAttribute("y", String.valueOf(n)+"px");
			   			}
		   			}
		   			
		   			else {
		   				
		   				OMElement test = (OMElement) canvastmprho.getElementById(region_name).getLastChild();
			   			int pos1 = Integer.parseInt(test.getAttribute("y")) + 660;
			   			test.setAttribute("y", Integer.toString(pos1));
			   			
			   			OMSVGTextElement test1 = (OMSVGTextElement) canvastmprho.getElementById(region_name).getFirstChild();		
			   			String px = test1.getAttribute("y"); // 460px
			   			px = px.replaceAll("px", ""); int n = Integer.parseInt(px) + 660;		   			
			   			test1.setAttribute("y", String.valueOf(n)+"px");
		   			}
		   		}
	   		}
	   		
	   		// Vertical Bar (mark)
	   		if ( (OMElement) canvastmprho.getElementById("repere_r") != null ) {
		   		String rr = ((OMElement) canvastmprho.getElementById("repere_r")).getAttribute("d");
		   		String rr1 = rr.split("M ")[1];
		   		rr1 = rr1.replaceAll(" 0 L .+", ""); rr1 = rr1.replaceAll("M ", "");
		   		int newRR1 = Integer.parseInt(rr1);
		   		String rr2 = rr.split("M ")[2];
		   		rr2 = rr2.replaceAll(" 0 L .+", ""); rr2 = rr2.replaceAll("M ", "");
		   		int newRR2 = Integer.parseInt(rr2);
		   		canvastmprho.getElementById("repere_r").setAttribute("d", "M " + newRR1 + " 660 L " + newRR1 + " 1560 M " + newRR2 + " 660 L " + newRR2 + " 1560");
	   		}
		}
				
		// Deleted Region for Minimal Genome of Subtilis
		if ( Config.abbr == "seb_min" ) {
			
			int decal = 400;
			int z = decal + 265; canvastmp.getElementById("dr_axe1").setAttribute("d", "M 30 " + z + " L 1170 "+ z);
			z = decal + 315; canvastmp.getElementById("dr_axe2").setAttribute("d", "M 30 " + z + " L 1170 "+ z);
			z = decal + 365; canvastmp.getElementById("dr_axe3").setAttribute("d", "M 30 " + z + " L 1170 "+ z);
			z = decal + 415; canvastmp.getElementById("dr_axe4").setAttribute("d", "M 30 " + z + " L 1170 "+ z);
			z = decal + 465; canvastmp.getElementById("dr_axe5").setAttribute("d", "M 30 " + z + " L 1170 "+ z);
			z = decal + 515; canvastmp.getElementById("dr_axe6").setAttribute("d", "M 30 " + z + " L 1170 "+ z);
			
			for ( int i = 0 ; i < Genoscapist.appControler.getListAllDeletedRegion().size() ; i++ ) {
				
				String dname = "deletedregion_"+Genoscapist.appControler.getListAllDeletedRegion().get(i).getName();
				
				if ( ((OMElement) canvastmp.getElementById(dname)).getFirstChild().toString().contains("rect") == true || ((OMElement) canvastmp.getElementById(dname)).getFirstChild().toString().contains("SVGRect") == true ) {
	   				
	   				OMElement test = (OMElement) canvastmp.getElementById(dname).getFirstChild();
		   			int pos1 = Integer.parseInt(test.getAttribute("y")) + decal;
		   			test.setAttribute("y", Integer.toString(pos1));
		   			
		   			if ( ((OMElement) canvastmp.getElementById(dname)).getLastChild().toString().contains("text") == true ) {
			   			
			   			OMSVGTextElement test1 = (OMSVGTextElement) canvastmp.getElementById(dname).getLastChild();		
			   			String px = test1.getAttribute("y"); // 460px
			   			px = px.replaceAll("px", ""); int n = Integer.parseInt(px) + decal;		   			
			   			test1.setAttribute("y", String.valueOf(n)+"px");
		   			}
	   			}
	   			
	   			else {
	   				
	   				OMElement test = (OMElement) canvastmp.getElementById(dname).getLastChild();
		   			int pos1 = Integer.parseInt(test.getAttribute("y")) + decal;
		   			test.setAttribute("y", Integer.toString(pos1));
		   			
		   			OMSVGTextElement test1 = (OMSVGTextElement) canvastmp.getElementById(dname).getFirstChild();		
		   			String px = test1.getAttribute("y"); // 460px
		   			px = px.replaceAll("px", ""); int n = Integer.parseInt(px) + decal;		   			
		   			test1.setAttribute("y", String.valueOf(n)+"px");
	   			}
			}
		}
				
		// SVG complete
   		String test = "";
   		if ( Genoscapist.appControler.rhoActive == true ) {
   			
   			if ( Config.abbr == "aeb" ) {
   				test = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1210 1650\" id=\"canvasPaint\" height=\"1650px\" width=\"1210px\">"+physicalMap.canvas.getElement().getInnerHTML() + canvastmp.getElement().getInnerHTML() + canvastmprho.getElement().getInnerHTML() +"</svg>";   		
   			}
   			else {
   				test = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1210 950\" id=\"canvasPaint\" height=\"950px\" width=\"1210px\">"+physicalMap.canvas.getElement().getInnerHTML() + canvastmp.getElement().getInnerHTML() + canvastmprho.getElement().getInnerHTML() +"</svg>";   		
   			}
   		}
   		
   		else if ( Config.abbr == "seb_min" ) {
				test = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1210 1000\" id=\"canvasPaint\" height=\"1000px\" width=\"1210px\">"+physicalMap.canvas.getElement().getInnerHTML() + canvastmp.getElement().getInnerHTML() +"</svg>";
		}
   		
   		else {
   				test = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1210 650\" id=\"canvasPaint\" height=\"650px\" width=\"1210px\">"+physicalMap.canvas.getElement().getInnerHTML() + canvastmp.getElement().getInnerHTML() +"</svg>";
   		}
   		   		
   		// SVG Image
   		String url2 = "data:image/svg+xml;base64," + base64encode(test);
   		final Image svgBase64EncodedImage = new Image(url2);
   	    svgBase64EncodedImage.setWidth("900");
   	    svgBase64EncodedImage.setHeight("350");
   	    
   	    Canvas canvas = Canvas.createIfSupported();
   	    
   	    if (canvas == null) {
   	    	Window.alert("Sorry, your browser doesn't support the HTML5 Canvas element");
   	    	return;
   	   	}
   	       	    
   	    canvas.setWidth("900px");
   	    canvas.setCoordinateSpaceWidth(900);
   	    canvas.setHeight("350px");
   	  	canvas.setCoordinateSpaceHeight(350);
   	  	
   	  	Context2d context = canvas.getContext2d();
   	  	
   	  	ImageElement imageElement = ImageElement.as(svgBase64EncodedImage.getElement());
   		imageElement.setWidth(900);
   		imageElement.setHeight(350);
   	 	context.drawImage(imageElement, 0, 0, imageElement.getWidth(), imageElement.getHeight(), 0, 0, imageElement.getWidth(), imageElement.getHeight());
   	            	 	
        // SVG Image
   	 	testImage.setHTML("<center><a href=\""+url2+"\" download=\"export_"+Config.abbr+".svg\"><img src=\""+Config.url_img+Config.abbr+"/images/"+"svg_"+Config.abbr+".png\" title=\"Download SVG\" width=\"26px\" height=\"26px\"></center>");
    }
	
	 // SVG
    private static native String base64encode(String str) /*-{
    	return $wnd.btoa(str);
	}-*/;
	
	/** Check Sequence size **/
	public static void checkSeqSize() {
		
		if ( Genoscapist.sizeSequence < 10 ) {
			Genoscapist.sizeSequence = 10;
		}
		
		if ( Genoscapist.sizeSequence > Genoscapist.appControler.getSeqLen() ) {
			Genoscapist.sizeSequence = Genoscapist.appControler.getSeqLen();
		}
	}
	
	/** SuggestBox Gene Selection **/
	public static void selectGene(final String fullGeneNameAsString) {

		if ( Config.abbr == "seb_min" ) {
			buttonNormalization.setVisible(false); buttonRho.setVisible(false);
		}
		
		AsyncCallback<String> callback = new AsyncCallback<String>() {

            public void onFailure(Throwable caught) {
            	
                    Window.alert("RPC callForGenesInfoService.getType failed : " + caught.getMessage());
            }

            public void onSuccess(String typeFeat) {
            	
            	AsyncCallback<GeneItem> callback = new AsyncCallback<GeneItem>() {

                    public void onFailure(Throwable caught) {
                    	
                            Window.alert("RPC callForGenesInfoService.getGeneItem failed : " + caught.getMessage());
                    }

                    public void onSuccess(GeneItem geneReturn) {
                    	
                            select(geneReturn);
                    }
        	    };
        	
        	    try {
        	            callForBDInfoService.getGeneItem(typeFeat, fullGeneNameAsString, callback);
        	
        	    } catch (Exception e1) {
        	            Window.alert("ERROR callForBDInfoService.getGeneItem : " + e1.getMessage());
        	    }
            }
	    };
	
	    try {
	            callForBDInfoService.getTypeFeature(fullGeneNameAsString, callback);
	
	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getTypeFeature : " + e1.getMessage());
	    }
	}
	
	/** SuggestBox Cluster Selection **/
	public static void selectCluster(String fullClusterNameAsString) {

		msg.startProcessing();
		
        Cluster clus = null;
        
        // Clusters list
        for ( int i = 0; i < Genoscapist.appControler.getListAllCluster().size(); i++ ) {
        	
                Cluster cluster = Genoscapist.appControler.getListAllCluster().get(i);
                
                if (fullClusterNameAsString.compareTo(cluster.getName()) == 0) {
                        clus = cluster;
                        break;
                }
        }
        
        // If selected cluster
        if (clus != null) {
        	
        	// URL
    		History.newItem("&cluster=" + clus.getName(), false);
        	
        	// Genes list with selected cluster
    		AsyncCallback<ArrayList<GeneItem>> callback = new AsyncCallback<ArrayList<GeneItem>>() {
                
                public void onFailure(Throwable caught) {
                	
                	Window.alert("RPC callForBDInfoService.getGenesWithCluster failed : " + caught.getMessage());
                }
                
                public void onSuccess(ArrayList<GeneItem> result) {

                    geneitemlist.setList(result);
                    
                    msg.stopProcessing();
                    
                    ContentView.create();
                }
            };
            
            try {
                callForBDInfoService.getGenesWithCluster(clus, callback);

    	    } catch (Exception e1) {
    	            Window.alert("ERROR callForBDInfoService.getGenesWithCluster : "
    	                            + e1.getMessage());
    	    }
        		
        } else {
        	
            Window.alert("No cluster found for your selection");
        }
	}
	
	/** TextBox Position Selection **/
	public static void selectPosition(String positionAsString) {
		
		// kb to b (kb = 1000b)
		positionAsString = positionAsString.concat("000");
				
		// Genes list with position
		AsyncCallback<ArrayList<GeneItem>> callback = new AsyncCallback<ArrayList<GeneItem>>() {
    		
            public void onFailure(Throwable caught) {
            	
            	Window.alert("RPC callForBDInfoService.getGenesWithPosition failed : " + caught.getMessage());
            }
            
            public void onSuccess(ArrayList<GeneItem> result) {
            	
                geneitemlist.setList(result);
                mainContentView.showWidget(0);
            }
        };
        
        try {
            callForBDInfoService.getGenesWithPosition(positionAsString, callback);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getGenesWithPosition : "
	                            + e1.getMessage());
	    }
	}
	
	/** ContentView Create **/
	public static void create() {
		
		if ( Config.abbr == "aeb" ) { html_suppData.setHTML(SuppData.AEB); }
		else						{ html_suppData.setHTML(SuppData.SEB); }
		
		mainContentView.showWidget(0);
		
		// Gene selection SuggestBox
        suggestboxIdentifier = new SuggestBox(oracleIdentifier);
        suggestboxIdentifier.setSize("10em", "15px");
        suggestboxIdentifier.getElement().getStyle().setProperty("borderRadius", "3px");
        suggestboxIdentifier.getElement().getStyle().setProperty("padding", ".2em 0");
        suggestboxIdentifier.setText(Genoscapist.gene);
        suggestboxIdentifier.addSelectionHandler(new SelectionHandler<Suggestion>() {
                @Override
                public void onSelection(SelectionEvent<Suggestion> event) {
                        selectGene(event.getSelectedItem().getReplacementString());
                }
        });
        
        // Cluster selection SuggestBox
        suggestboxCluster = new SuggestBox(oracleCluster);
        suggestboxCluster.setSize("10em", "15px");
        suggestboxCluster.getElement().getStyle().setProperty("borderRadius", "3px");
        suggestboxCluster.getElement().getStyle().setProperty("padding", ".2em 0");
        suggestboxCluster.setText(Genoscapist.cluster);
        suggestboxCluster.addSelectionHandler(new SelectionHandler<Suggestion>() {
            @Override
            public void onSelection(SelectionEvent<Suggestion> event) {
                    selectCluster(event.getSelectedItem().getReplacementString());
                    
                    msg.startProcessing();
            }
        });
        
        // Position Choice
        textboxPosition = new TextBox();
        textboxPosition.setWidth("6em");
        textboxPosition.setHeight("15px");
        textboxPosition.getElement().getStyle().setProperty("borderRadius", "3px");
        textboxPosition.getElement().getStyle().setProperty("padding", ".2em 0");
        textboxPosition.addKeyPressHandler(new KeyPressHandler() {
			
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (!Character.isDigit(event.getCharCode())) {
			          ((TextBox) event.getSource()).cancelKey();
			          Window.alert("Please enter only number please.");
			        }
			}
		});
		
		// Genes table
		table.setWidth("100%", true);
		table.setPageSize(Genoscapist.appControler.getListAllGeneItem().size());
        table.addStyleName(".cellTableHoveredRow");
       
        geneitemlist.addDataDisplay(table);
        List<GeneItem> list = geneitemlist.getList();
        for (GeneItem gene : Genoscapist.appControler.getListAllGeneItem()) {
                list.add(gene);
        }
        
        msg.stopProcessing();

        // SortHandler Genes list
        ListHandler<GeneItem> sortHandler = new ListHandler<GeneItem>(list);
        table.addColumnSortHandler(sortHandler);
        
        // Name column
        Column<GeneItem, String> nameColumn = new Column<GeneItem, String>(new ClickableTextCell()) {

        	@Override
        	public String getValue(GeneItem object) {

                String name = object.getName();

                if (name.equalsIgnoreCase("")) {
                        return "none";
                } else {
                        return object.getName();
                }
        	}
        };
        
        nameColumn.setFieldUpdater(new FieldUpdater<GeneItem, String>() {

            @Override
            public void update(int index, GeneItem object, String value) {            	
            	select(object);
            }
        });
        nameColumn.setCellStyleNames("linkCellTable");
        table.setColumnWidth(nameColumn, 70, Unit.PX);
        table.addColumn(nameColumn, "Name");
        
        // Locus Tag column
        Column<GeneItem, String> locustagColumn = new Column<GeneItem, String>(new ClickableTextCell()) {

        	@Override
        	public String getValue(GeneItem object) {

                String locustag = object.getLocusTag();

                if (locustag.equalsIgnoreCase("")) {
                        return "none";
                } else {
                        return object.getLocusTag();
                }
        	}
        };
        locustagColumn.setFieldUpdater(new FieldUpdater<GeneItem, String>() {

            @Override
            public void update(int index, GeneItem object, String value) {            	
            	select(object);
            }
        });
        locustagColumn.setCellStyleNames("linkCellTable");
        table.setColumnWidth(locustagColumn, 70, Unit.PX);
        table.addColumn(locustagColumn, "Locus tag");
        
        // Location column : [start, end] (strand strand)
        Column<GeneItem, String> locationColumn = new Column<GeneItem, String>(new ClickableTextCell()) {

        	@Override
        	public String getValue(GeneItem object) {

        		int start = object.getStart();
        		int end = object.getEnd();
        		int strand = object.getStrand();
        		String location = "["+start+","+end+"]"+" (strand "+strand+")";
                if (location.equalsIgnoreCase("")) {
                        return "none";
                } else {
                		return location;
                }
        	}
        };
        table.setColumnWidth(locationColumn, 70, Unit.PX);
        table.addColumn(locationColumn, "Location");
        
        // Clusters column
        IdentityColumn<GeneItem> clustersColumn = new IdentityColumn<GeneItem>(new AbstractCell<GeneItem>() {

			@Override
			public void render(Context context, GeneItem geneItem, SafeHtmlBuilder sb) {
				
				String clusterA = geneItem.getClusterA(); if (clusterA == null) {clusterA = "";}
				String clusterB = geneItem.getClusterB(); if (clusterB == null) {clusterB = "";}
				String clusterC = geneItem.getClusterC(); if (clusterC == null) {clusterC = "";}
				
				String safeHtml = null;
				
				safeHtml = "<a href = \"" + Config.url + "#&cluster="+clusterA+"\">"+clusterA+"</a> ";
				safeHtml += "<a href = \"" + Config.url + "#&cluster="+clusterB+"\">"+clusterB+"</a> ";
				safeHtml += "<a href = \"" + Config.url + "#&cluster="+clusterC+"\">"+clusterC+"</a>";
				
				sb.append(SafeHtmlUtils.fromTrustedString(safeHtml));
			}
		});
        table.setColumnWidth(clustersColumn, 70, Unit.PX);
        table.addColumn(clustersColumn, "Clusters");
                
        identifier.add(suggestboxIdentifier);
        cluster.add(suggestboxCluster);
        position.add(textboxPosition);
        
        LoadingDialog lDiagGene = new LoadingDialog("All_genes");
        LoadingDialog lDiagCluster = new LoadingDialog("All_clusters");
	}
	
	/** Sharing popup **/
	private static DialogBox showShareDialog() {

		final DialogBox dialog = new DialogBox(true, true);
	    dialog.setText("Share this view");
		Label content = new Label("Paste this link");
		VerticalPanel vPanel = new VerticalPanel(); vPanel.setSpacing(2);
		vPanel.add(content); vPanel.add(new Label("\n"));
		HorizontalPanel hPanel = new HorizontalPanel();
		TextBox txtB = new TextBox(); txtB.setName("ButtonCopy"); txtB.setSize("20em", "1.4em");

		// URL
		String url = Config.url+"#";

		// Normalization
		if (Genoscapist.appControler.getNormalization() == "CustomCDS" ) {
			url += "&norm=customcds";
		}
		else {
			url += "&norm=median";
		}
		// ID or Position
		if ( Genoscapist.appControler.getGeneSelected() != null ) {
			url += "&id="+Genoscapist.appControler.getGeneSelected().getLocusTag();
		}
		else {
			url += "&position="+Genoscapist.appControler.getStart_view();
		}
		// Rho
		if ( Genoscapist.appControler.isRhoActive() ) 	{ url += "&rho=1"; }
		else												{ url += "&rho=0"; }
		// Scale
		if ( Genoscapist.sizeSequence == 10000 )			{ url += "&scale=normal"; }
		else if ( Genoscapist.sizeSequence == 20000 )		{ url += "&scale=large"; }
		else if ( Genoscapist.sizeSequence == 40000 )		{ url += "&scale=huge"; }
		else if ( Genoscapist.sizeSequence == 5000 )		{ url += "&scale=small"; }
		else												{ url += "&scale=tiny"; }
		// Features names
		if ( Genoscapist.appControler.isAddFeatureNameActive() ) 	{ url += "&fname=1"; }
		else														{ url += "&fname=0"; }
		// Experiences
		url += "&exp=";
		for ( int i = 0 ; i < Genoscapist.appControler.getListAllSelectedExperience().size() ; i++ ) {
			
			if ( Genoscapist.appControler.getListAllSelectedExperience().get(i).getStrand() == 1 ) {
				url += Genoscapist.appControler.getListAllSelectedExperience().get(i).getExp()+",";
				if ( i == Genoscapist.appControler.getListAllSelectedExperience().size()-1 ) {
					url += Genoscapist.appControler.getListAllSelectedExperience().get(i).getColorSelect();
				}
				else {
					url += Genoscapist.appControler.getListAllSelectedExperience().get(i).getColorSelect()+";";
				}
			}	
		}
		
		txtB.setValue(url);
		hPanel.add(txtB);
		vPanel.add(hPanel);
		Label content2 = new Label("Press CTRL-C to copy");
		vPanel.add(content2); vPanel.add(new Label("\n"));
		dialog.setWidget(vPanel);
		dialog.center();
		dialog.show();
		return dialog;
	}
	
	/** Gene Selection **/
	protected static void select(GeneItem geneS) {
				
		Genoscapist.appControler.setGeneSelected(geneS);
		
		// Cleaning
		viewDock.getElement().removeAllChildren();
		viewDockCourbe.getElement().removeAllChildren();
		viewDockCourbeRho.getElement().removeAllChildren();
		
		mainContentView.showWidget(1);
		
		// URL
		History.newItem("&id=" + geneS.getLocusTag() +"&size="+Genoscapist.sizeSequence, false);
		
		geneselected.setHTML("<h2><div class='h2-withoutborder'>Gene/Segment: "+Genoscapist.appControler.getGeneSelected().getName()+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Locus Tag: "+Genoscapist.appControler.getGeneSelected().getLocusTag()+"</div></h2>");
		
		// Details about Section
    	AsyncCallback<String> callbackHighCondition = new AsyncCallback<String>() {
            
            public void onFailure(Throwable caught) {
            	Window.alert("RPC callForBDInfoService.getCondition failed : " + caught.getMessage());
            }
            
            public void onSuccess(String result) {
        		
            	if ( Config.abbr == "seb_min" ) {
            		detailsHighCondition.setVisible(false);
            		profile.setVisible(false);
            		png.setVisible(false);
            	}
            	else {
            		detailsHighCondition.setHTML(result);
            	}
				
				// Expression profile of segment Section
				if ( detailsHighCondition.getHTML() != "" ) {
					String file2 = Genoscapist.appControler.getGeneSelected().getName() + "_" + Genoscapist.appControler.getGeneSelected().getStart() + "_" + Genoscapist.appControler.getGeneSelected().getEnd() + "_" + Genoscapist.appControler.getGeneSelected().getStrand() + ".png";
					png.setUrl(Config.url_img+Config.abbr+"/images/details/" + file2);
		        	profile.setHTML("<h2><div class='h2-withborder'>Expression Profile of " + Genoscapist.appControler.getGeneSelected().getLocusTag()+"</div></h2>");
		        	png.setSize("830px", "519px");
		    		png.setVisible(true);
		        }
		        else {
		        	profile.setVisible(false);;
		        	png.setVisible(false);
		        }
            }
        };
        
        try {
            callForBDInfoService.getCondition(geneS, 1, callbackHighCondition);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getCondition : " + e1.getMessage());
	    }
		
		// Lowest Expression Conditions
        AsyncCallback<String> callbackLowCondition = new AsyncCallback<String>() {
            
            public void onFailure(Throwable caught) {
            	
            	Window.alert("RPC callForBDInfoService.getCondition failed : " + caught.getMessage());
            }
            
            public void onSuccess(String result) {
        		
            	if ( Config.abbr == "seb_min" ) {
            		detailsLowCondition.setVisible(false);
            	}
            	else {
            		detailsLowCondition.setHTML(result);
            	}
            }
        };
        
        try {
            callForBDInfoService.getCondition(geneS, -1, callbackLowCondition);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getCondition : " + e1.getMessage());
	    }
		
		// Most Positively Correlated Segments
        AsyncCallback<String> callbackPositiveSegment = new AsyncCallback<String>() {
            
            public void onFailure(Throwable caught) {
            	Window.alert("RPC callForBDInfoService.getSegment failed : " + caught.getMessage());
            }
            
            public void onSuccess(String result) {
        		
            	detailsHighSegment.setHTML(result);
            }
        };
        
        try {
            callForBDInfoService.getSegment(geneS.getLocusTag(), 1, callbackPositiveSegment);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getSegment : " + e1.getMessage());
	    }
		
		// Most Negatively Correlated Segments
        AsyncCallback<String> callbackNegativeSegment = new AsyncCallback<String>() {
            
            public void onFailure(Throwable caught) {
            	Window.alert("RPC callForBDInfoService.getSegment failed : " + caught.getMessage());
            }
            
            public void onSuccess(String result) {
        		
            	detailsLowSegment.setHTML(result);
            }
        };
        
        try {
            callForBDInfoService.getSegment(geneS.getLocusTag(), -1, callbackNegativeSegment);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getSegment : " + e1.getMessage());
	    }
        
        annotation.setHTML("<h2><div class='h2-withborder'>Annotation of " + Genoscapist.appControler.getGeneSelected().getLocusTag()+"</div></h2>");
		       
		// GenBank annotation of gene Section
		AsyncCallback<HashMap<String, String>> callback = new AsyncCallback<HashMap<String, String>>() {
            
            public void onFailure(Throwable caught) {
            	
            	Window.alert("RPC callForBDInfoService.getQualifiersGenbank failed : " + caught.getMessage());
            }
            
            public void onSuccess(HashMap<String, String> result) {
            	
            	String gb = "<h3>GenBank annotation</h3>";
            	
            	if ( result.size() != 0 ) {
            		
                	List<String> keys = new ArrayList<String>(result.keySet());
                	
                	for ( String key : keys ) {
                		gb += "<b>" + key + "</b>: " + result.get(key) + "<br/>";
                	}
                	                	
                	genbank.setHTML(gb);
            	}
            	
            	else {
            		
            		gb += "<b><i>No information</i></b><br/>";
            		genbank.setHTML(gb);
            	}
            	
            }
        };
        
        try {
            callForBDInfoService.getQualifiersGenbank(Genoscapist.appControler.getGeneSelected(), callback);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getQualifiersGenbank : " + e1.getMessage());
	    }
                
        // Additional information for gene Section
		AsyncCallback<String> callback2 = new AsyncCallback<String>() {
            
            public void onFailure(Throwable caught) {
            	
            	Window.alert("RPC callForBDInfoService.getQualifiersInformation failed : " + caught.getMessage());
            }
            
            public void onSuccess(String result) {
            	
            	if ( Config.abbr == "seb_min" || result == "" ) { result = "<h3>Additional informations</h3>"; }
            	if ( Config.abbr.contains("seb") ) {            		
            		result += "<a href = \"http://subtiwiki.uni-goettingen.de/v3/gene/search/exact/"+Genoscapist.appControler.getGeneSelected().getLocusTag()+"\" target=\"_blank\"><img src=\""+Config.url_img+Config.abbr+"/images/"+"subtiwiki.png\" width=\"12%\"></a>";
            	}
            	else if ( Config.abbr.contains("aeb") ) {
            		
            		if ( Genoscapist.appControler.getGeneSelected().getLocusTag().contains("SAOUHSC_") == true ) {
            			result += "<a href = \"https://aureowiki.med.uni-greifswald.de/"+Genoscapist.appControler.getGeneSelected().getLocusTag()+"\" target=\"_blank\"><img src=\""+Config.url_img+Config.abbr+"/images/"+"aureowiki.png\" width=\"12%\"></a>";
            		}
            		
            		else {
            			result += "<a href = \"https://aureowiki.med.uni-greifswald.de/"+Genoscapist.appControler.getGeneSelected().getName()+"\" target=\"_blank\"><img src=\""+Config.url_img+Config.abbr+"/images/"+"aureowiki.png\" width=\"12%\"></a>";
            		}
            	}
            		
            	information.setHTML(result);
            }
        };
        
        try {
            callForBDInfoService.getQualifiersInformation(Genoscapist.appControler.getGeneSelected(), callback2);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getQualifiersInformation : " + e1.getMessage());
	    }
		
        // Expression profile of segment Section		
		buttonSample.setHTML("Sample selection");
		if ( Genoscapist.appControler.getNormalization().contains("Median") == true ) {
			buttonNormalization.setHTML("Switch to CDS-Quantile normalisation");
		}
		else {
			buttonNormalization.setHTML("Switch to Median normalisation");
		}
		if ( Genoscapist.appControler.rhoActive == true ) {
			buttonRho.setHTML("Remove Rho samples track");
		}
		else {
			buttonRho.setHTML("Add Rho samples track");
		}
		
		if ( Genoscapist.appControler.addFeatureNameActive == true ) {
			buttonAddFeatureName.setHTML("Remove Feature Names");
		}
		else {
			buttonAddFeatureName.setHTML("Add Feature Names");
		}
		
		buttonLegend.setHTML("legend");
		
		if ( Config.abbr == "seb" || Config.abbr == "seb_min" ) {
			
			// Zoom
			imgBigMinus.setResource(ImageResources.INSTANCE.imgOutSEB());
			imgSmallMinus.setResource(ImageResources.INSTANCE.imgSmallOutSEB());
			imgNeutral.setResource(ImageResources.INSTANCE.imgNeutralSEB());
			imgSmallPlus.setResource(ImageResources.INSTANCE.imgSmallInSEB());
			imgBigPlus.setResource(ImageResources.INSTANCE.imgInSEB());
			
			// Share
			imgSharing.setResource(ImageResources.INSTANCE.imgShareSEB());
			
			// Export
			testImage.setHTML("<center><img src=\""+Config.url_img+Config.abbr+"/images/"+"svg_seb.png\" title=\"Download SVG\" width=\"26px\" height=\"26px\"></center>");
			
			// Navigation
			imgBefore.setResource(ImageResources.INSTANCE.imgBigLeftSEB());
			imgBefore5000.setResource(ImageResources.INSTANCE.imgSmallLeftSEB());
			imgAfter5000.setResource(ImageResources.INSTANCE.imgSmallRightSEB());
			imgAfter.setResource(ImageResources.INSTANCE.imgBigRightSEB());
		}
		else {
			
			// Zoom
			imgBigMinus.setResource(ImageResources.INSTANCE.imgOutAEB());
			imgSmallMinus.setResource(ImageResources.INSTANCE.imgSmallOutAEB());
			imgNeutral.setResource(ImageResources.INSTANCE.imgNeutralAEB());
			imgSmallPlus.setResource(ImageResources.INSTANCE.imgSmallInAEB());
			imgBigPlus.setResource(ImageResources.INSTANCE.imgInAEB());
			
			// Share
			imgSharing.setResource(ImageResources.INSTANCE.imgShareAEB());
			
			// Export
			testImage.setHTML("<center><img src=\""+Config.url_img+Config.abbr+"/images/"+"svg_aeb.png\" title=\"Download SVG\" width=\"26px\" height=\"26px\"></center>");
			
			// Navigation
			imgBefore.setResource(ImageResources.INSTANCE.imgBigLeftAEB());
			imgBefore5000.setResource(ImageResources.INSTANCE.imgSmallLeftAEB());
			imgAfter5000.setResource(ImageResources.INSTANCE.imgSmallRightAEB());
			imgAfter.setResource(ImageResources.INSTANCE.imgBigRightAEB());
		}
		
		imgBigMinus.setSize("26px", "26px");
		imgSmallMinus.setSize("26px", "26px");
		imgNeutral.setSize("26px", "26px");
		imgSmallPlus.setSize("26px", "26px");
		imgBigPlus.setSize("26px", "26px");
		imgSharing.setSize("26px", "26px");
		imgSharing.setAltText("Sharing by URL");
		imgSharing.setTitle("Sharing by URL");
		imgBefore.setSize("26px", "26px");
		imgBefore5000.setSize("26px", "26px");
		imgAfter5000.setSize("26px", "26px");
		imgAfter.setSize("26px", "26px");
		
		// Genomic view of gene/segment Section
		view.setHTML("<div align = 'left'><h2><div class='h2-withborder'>Genomic View of " + Genoscapist.appControler.getGeneSelected().getLocusTag()+"</div></h2></div>");
		
		createDisclosurePanelSample(null, null, Genoscapist.appControler.getListAllSelectedExperience());
		
        msg.stopProcessing();
        
		speciesChanged(Genoscapist.currentSpecies, null);
	}
	
	/** Sample selection **/
	public static void createDisclosurePanelSample(final String name, final String color, final List<Experience> listExpSelected) {
				
		final HashMap<String, Experience> hashSelectedExp = new HashMap<String, Experience>();
		         
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(5);
						
		disclosurePanelSample.setHeader(new HTML(""));
		disclosurePanelSample.setAnimationEnabled(true);
		
		int line; int column;
		if ( Config.abbr == "aeb" ) {
			line = 20;
			column = 16;
		}
		else if ( Config.abbr == "seb_min" ) {
			line = 2;
			column = 16;
		}
		else {
			line = 34;
			column = 16;
		}
		        		
		final Grid content = new Grid(line, column);
		        		
		content.setCellSpacing(6);
		        		
		int i = 0;
		int j = 0;
				
		for ( final Experience exp : Genoscapist.appControler.getListAllExperience() ) {
		        			
			if (  Config.abbr == "aeb" && ( i == 16  || i == 32 || i == 48  || i == 64   || i == 80   || i == 96  || i == 112  || i == 128  || i == 144  || 
		          i == 160 || i == 176 || i == 192  || i == 208  || i == 224  || i == 240 || i == 256 || i == 272 || i == 288 || 
		          i == 304 || i == 320  || i == 336 ) ) {
		           				
		    	j++;
		    	i = 0;
		    }
		        			
		    else if ( Config.abbr == "seb_min" && i == 16 ) {
		        				
		    	j++;
		    	i = 0;
		    }
		           			
		    else if ( Config.abbr == "seb" && ( i == 16  || i == 32 || i == 48  || i == 64   || i == 80   || i == 96  || i == 112  || i == 128  || i == 144  || 
		              i == 160 || i == 176 || i == 192  || i == 208  || i == 224  || i == 240 || i == 256 || i == 272 || i == 288 || 
		              i == 304 || i == 320  || i == 336 || i == 352 || i == 368 || i == 384 || i == 400 || i == 416 || i == 432 ||
		              i == 448 || i == 464 || i == 480 || i == 496 || i == 512 || i == 528 || i == 544 ) ) {
		           						           				
		    	j++;
		        i = 0;
		    }
		        				
		    CheckBox cb = new CheckBox();
		       				
		    for ( int k = 0 ; k < listExpSelected.size() ; k++ ) {
		       					
		    	if ( exp.getExp().compareTo(listExpSelected.get(k).getExp()) == 0 ) {
		    		
		       		cb.setValue(true);
		       		exp.setColorSelect(listExpSelected.get(k).getColorSelect());
			       	hashSelectedExp.put(exp.getExp(), exp);
			     	break;
		      	}
		 	}
		       				
			cb.setName(exp.getExp());
		 	content.setWidget(j, i, cb);
		      
		 	// Sample Selection
			cb.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
		       						
		       		boolean checked = ((CheckBox) event.getSource()).getValue();
		       						
		       		if ( checked ) {
		       			hashSelectedExp.put(exp.getExp(), exp);
		       		}
		       				        
		       		else {
		       			hashSelectedExp.remove(((CheckBox) event.getSource()).getName());
		       		}
		       	}
			});
		        
			// Sample Name
		    final HTML html;
		    if ( name != null && exp.getExp().compareTo(name) == 0 ) {
		    	html = new HTML("<font color = '" + color + "'><b>" + exp.getExp() + "</b></font>");
		    }
		    
		    else {
		    	html = new HTML("<font color = '" + exp.getColorSelect() + "'><b>" + exp.getExp() + "</b></font>");
		   	}
		       				
		    html.setTitle("Click on the experience's name to change the color");
		    content.setWidget(j, i+1, html);
		        					       				
		    // Change color
		    html.addClickHandler(new ClickHandler() {
									
		    	@Override
		    	public void onClick(ClickEvent event) {
										
		    		final DialogBox dialogBox = new DialogBox();
										
					dialogBox.setText("About " + exp.getExp());
					dialogBox.setWidth("400px");
										
					VerticalPanel dialogContent = new VerticalPanel();
					dialogContent.setSpacing(4);
										
					HTML title = new HTML("<h3>Description</h3>");
					dialogContent.add(title);
					dialogContent.setCellHorizontalAlignment(title, HasHorizontalAlignment.ALIGN_LEFT);
					HTML desc = new HTML(exp.getDescription());
					dialogContent.add(desc);
					dialogContent.setCellHorizontalAlignment(desc, HasHorizontalAlignment.ALIGN_JUSTIFY);
										
					HTML text = new HTML("<h3>Choose color for selected sample(s)</h3>");
					dialogContent.add(text);
					dialogContent.setCellHorizontalAlignment(text, HasHorizontalAlignment.ALIGN_LEFT);
										
					final ListBox listBox = new ListBox();
					listBox.addItem("default", exp.getColor());
					// Colors list
					listBox.addItem("red", "#FF0000");
					listBox.addItem("orange", "#FF8800");
					listBox.addItem("yellow", "#FFFF00");
					listBox.addItem("green", "#00FF00");
					listBox.addItem("cyan", "#00FFFF");
					listBox.addItem("blue", "#4444FF");
					listBox.addItem("pink", "#FF88FF");
					listBox.addItem("brown", "#773311");
					listBox.addItem("black", "#000000");
					listBox.setMultipleSelect(false);
					dialogContent.add(listBox);
							
					// Validation
					Button closeButton = new Button("Ok", new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {

							dialogBox.hide();
							exp.setColorSelect(listBox.getSelectedValue());
							                        
							int t = 0;
							for ( int j = 0 ; j < Genoscapist.appControler.listAllSelectedExperience.size() ; j++ ) {
							                        	
								if ( Genoscapist.appControler.listAllSelectedExperience.get(j).getExp().contains(exp.getExp()) == true ) {
							    	t = t + 1;
							        Genoscapist.appControler.listAllSelectedExperience.get(j).setColorSelect(listBox.getSelectedValue());
							        if ( t == 2 ) { break; }
							    }
							}
							                        
							html.setHTML("<font color = '" + listBox.getSelectedValue() + "'><b>" + exp.getExp() + "</b></font>");
						}
					});
							        
					dialogContent.add(closeButton);
					dialogContent.setCellHorizontalAlignment(closeButton, HasHorizontalAlignment.ALIGN_CENTER);
								        
					dialogBox.setWidget(dialogContent);
										
					dialogBox.setGlassEnabled(true);
					dialogBox.setAnimationEnabled(true);
					dialogBox.center();
					dialogBox.show();
		    	}
		    });
		        				        				
		  	i = i + 2;
		}
		        		
		// Default button
		Button defaut = new Button("default", new ClickHandler() {
							
			@Override
			public void onClick(ClickEvent event) {
		        				
				Genoscapist.appControler.listAllSelectedExperience.clear();
								
				final AsyncCallback<List<Experience>> callback = new AsyncCallback<List<Experience>>() {

					@Override
					public void onFailure(Throwable caught) {
						            	
						Window.alert("No experience found !!!");
					}

					@Override
					public void onSuccess(List<Experience> result) {
						            	
						Genoscapist.appControler.setListAllDefaultExperience(result);
						          		
						hashSelectedExp.clear();

						ContentView.disclosurePanelSample.clear();
				      	ContentView.createDisclosurePanelSample(null, null, result);
				   }
				};
						    	
				try {
					callForBDInfoService.getAllDefaultExperience(Genoscapist.appControler.getNormalization(), callback);
				} catch (Exception e1) {
					Window.alert("ERROR callForDBInfoService.getAllExperience : " + e1.getMessage());
				}
			}
		});
		        		
		// All button
		Button all = new Button("all", new ClickHandler() {
							
			@Override
			public void onClick(ClickEvent event) {

				hashSelectedExp.clear();
								
				for ( Experience exp : Genoscapist.appControler.getListAllExperience() ) {
									
					hashSelectedExp.put(exp.getExp(), exp);
				}
																
				for ( int i = 0 ; i < line ; i++ ) {
									
					for ( int j = 0 ; j < column-1 ; j++ ) {
									
						if ( j != 0 ) { j++; }
							CheckBox ccb = (CheckBox) content.getWidget(i, j);
							ccb.setValue(true);
					}
				}
			}
		});
		        		
		// Clean button
		Button clear = new Button("none", new ClickHandler() {
							
			@Override
			public void onClick(ClickEvent event) {
								
				Genoscapist.appControler.listAllSelectedExperience.clear();
								
				hashSelectedExp.clear();
								
				for ( int i = 0 ; i < line ; i++ ) {
									
					for ( int j = 0 ; j < column-1 ; j++ ) {
									
						if ( j != 0 ) { j++; }
							CheckBox ccb = (CheckBox) content.getWidget(i, j);
							ccb.setValue(false);
					}
				}
			}
		});
		        		
		// Apply button
		Button refresh = new Button("apply", new ClickHandler() {
							
			@Override
			public void onClick(ClickEvent event) {
									
				// Vertical bar position
				int position_vbar = -1;
				if ( physicalMap.canvas.getElementById("repere_g") != null ) {
					
					String c = ((OMElement) physicalMap.canvas.getElementById("pos_g")).getElement().getInnerHTML();
		      		position_vbar = Integer.parseInt(c);
		    		Genoscapist.appControler.setVbarPosition(position_vbar);
				}
								
				Genoscapist.appControler.listAllSelectedExperience.clear();
														
				Iterator iterator = hashSelectedExp.keySet().iterator();
								
				disclosurePanelSample.setOpen(false);
								
				while ( iterator.hasNext() ) {
									
					final String key = (String) iterator.next();
					Experience expS = hashSelectedExp.get(key);
					Genoscapist.appControler.listAllSelectedExperience.add(expS);
				}
								
				disclosurePanelSample.setOpen(false);
								
				AsyncCallback<List<Experience>> callback4 = new AsyncCallback<List<Experience>>() {
						            
					public void onFailure(Throwable caught) {
						Window.alert("RPC callForBDInfoService.getExperience failed : " + caught.getMessage());
					}

					@Override
					public void onSuccess(List<Experience> result) {
										
						Genoscapist.appControler.listAllSelectedExperience = result;
					
						// Cleaning Profile View
						viewDockCourbe.getElement().removeAllChildren();
										
						Scheduler.get().scheduleDeferred(new ScheduledCommand() {

							@Override
							public void execute() {
								physicalMap.paintCourbe();
							}
						});
					}
				};
						        
				try {
					callForBDInfoService.getReverseExperience(Genoscapist.appControler.getNormalization(), Genoscapist.appControler.listAllSelectedExperience, callback4);
				} catch (Exception e1) {
					Window.alert("ERROR callForBDInfoService.getReverseExperience : " + e1.getMessage());
				}
			}
		});
		        		
		// Buttons	        		
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setSpacing(5);		        		
		hPanel.add(defaut); hPanel.add(clear); hPanel.add(all); hPanel.add(refresh);
		        		
		vPanel.add(content);
		vPanel.add(hPanel);
		vPanel.setCellHorizontalAlignment(hPanel, HasHorizontalAlignment.ALIGN_RIGHT);
		        		
		disclosurePanelSample.setContent(vPanel);
	}
	
	
	/** Resize **/
	public static void resize() {
				
		msg.startProcessing();
		
		imgBigPlus.setVisible(false);
		imgBigMinus.setVisible(false);
		imgSmallMinus.setVisible(false);
		imgSmallPlus.setVisible(false);
		imgNeutral.setVisible(false);
		
		// Cleaning
		viewDock.getElement().removeAllChildren();
		viewDockCourbe.getElement().removeAllChildren();
		viewDockCourbeRho.getElement().removeAllChildren();
		
		speciesChanged(Genoscapist.currentSpecies, "nochange");
		
		msg.stopProcessing();
	}
	
	/** Navigation **/
	public static void move(String sens) {
		
		msg.startProcessing();
		
		imgBefore.setVisible(false);
		imgBefore5000.setVisible(false);
		imgAfter.setVisible(false);
		imgAfter5000.setVisible(false);
		
		// No selected gene
		Genoscapist.appControler.setGeneSelected(null);
		
		// Cleaning
		viewDock.getElement().removeAllChildren();
		viewDockCourbe.getElement().removeAllChildren();
		viewDockCourbeRho.getElement().removeAllChildren();
		
		mainContentView.showWidget(1);
		
		geneselected.setHTML("<h2><div class='h2-withoutborder'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></h2>");
		
		// Informations
		detailsHighCondition.setHTML("");
		detailsLowCondition.setHTML("");
		detailsHighSegment.setHTML("");
		detailsLowSegment.setHTML("");
		annotation.setHTML("");
		genbank.setHTML("");
		information.setHTML("");
		profile.setHTML("");
		png.setUrl("");
		png.setVisible(false);
		
		// Buttons
		buttonSample.setHTML("Sample selection");
		if ( Config.abbr == "seb_min" ) { buttonNormalization.setVisible(false); buttonRho.setVisible(false); }
		if ( Genoscapist.appControler.getNormalization().contains("Median") == true ) { buttonNormalization.setHTML("Switch to CDS-Quantile normalisation"); }
		else { buttonNormalization.setHTML("Switch to Median normalisation"); }
		if ( Genoscapist.appControler.rhoActive == false ) { buttonRho.setHTML("Add Rho samples track"); }
		if ( Genoscapist.appControler.addFeatureNameActive == false ) { buttonAddFeatureName.setHTML("Add Feature Names"); }
        else { buttonAddFeatureName.setHTML("Remove Feature Names"); }
		buttonLegend.setHTML("legend");
		
		if ( Config.abbr == "seb" || Config.abbr == "seb_min" ) {
			
			// Zoom
			imgBigMinus.setResource(ImageResources.INSTANCE.imgOutSEB());
			imgSmallMinus.setResource(ImageResources.INSTANCE.imgSmallOutSEB());
			imgNeutral.setResource(ImageResources.INSTANCE.imgNeutralSEB());
			imgSmallPlus.setResource(ImageResources.INSTANCE.imgSmallInSEB());
			imgBigPlus.setResource(ImageResources.INSTANCE.imgInSEB());
			
			// Share
			imgSharing.setResource(ImageResources.INSTANCE.imgShareSEB());
			
			// Export
			testImage.setHTML("<center><img src=\""+Config.url_img+Config.abbr+"/images/"+"svg_seb.png\" title=\"Download SVG\" width=\"26px\" height=\"26px\"></center>");
			
			// Navigation
			imgBefore.setResource(ImageResources.INSTANCE.imgBigLeftSEB());
			imgBefore5000.setResource(ImageResources.INSTANCE.imgSmallLeftSEB());
			imgAfter5000.setResource(ImageResources.INSTANCE.imgSmallRightSEB());
			imgAfter.setResource(ImageResources.INSTANCE.imgBigRightSEB());
		}
		else {
			
			// Zoom
			imgBigMinus.setResource(ImageResources.INSTANCE.imgOutAEB());
			imgSmallMinus.setResource(ImageResources.INSTANCE.imgSmallOutAEB());
			imgNeutral.setResource(ImageResources.INSTANCE.imgNeutralAEB());
			imgSmallPlus.setResource(ImageResources.INSTANCE.imgSmallInAEB());
			imgBigPlus.setResource(ImageResources.INSTANCE.imgInAEB());
			
			// Share
			imgSharing.setResource(ImageResources.INSTANCE.imgShareAEB());
			
			// Export
			testImage.setHTML("<center><img src=\""+Config.url_img+Config.abbr+"/images/"+"svg_aeb.png\" title=\"Download SVG\" width=\"26px\" height=\"26px\"></center>");
			
			// Navigation
			imgBefore.setResource(ImageResources.INSTANCE.imgBigLeftAEB());
			imgBefore5000.setResource(ImageResources.INSTANCE.imgSmallLeftAEB());
			imgAfter5000.setResource(ImageResources.INSTANCE.imgSmallRightAEB());
			imgAfter.setResource(ImageResources.INSTANCE.imgBigRightAEB());
		}
		
		imgBigMinus.setSize("26px", "26px");
		imgSmallMinus.setSize("26px", "26px");
		imgNeutral.setSize("26px", "26px");
		imgSmallPlus.setSize("26px", "26px");
		imgBigPlus.setSize("26px", "26px");
		imgSharing.setSize("26px", "26px");
		imgSharing.setAltText("Sharing by URL");
		imgSharing.setTitle("Sharing by URL");
		imgBefore.setSize("26px", "26px");
		imgBefore5000.setSize("26px", "26px");
		imgAfter5000.setSize("26px", "26px");
		imgAfter.setSize("26px", "26px");
		
		// Genomic View
		view.setHTML("<div align = 'left'><h2><div class = 'h2-withborder'>Genomic View</div></h2></div>");
		
		createDisclosurePanelSample(null, null, Genoscapist.appControler.getListAllSelectedExperience());
		
        msg.stopProcessing();
        
		speciesChanged(Genoscapist.currentSpecies, sens);
	}
	
	/** Change of specie (sequences) **/
	public static void speciesChanged(Species speciesToShow, final String sens) {
				
		Genoscapist.currentSpecies = speciesToShow;
               
        AsyncCallback<List<Sequence>> callback = new AsyncCallback<List<Sequence>>() {
            
            public void onFailure(Throwable caught) {
            	
            	Window.alert("RPC callForBDInfoService.getAccessions failed : " + caught.getMessage());
            }
            
            public void onSuccess(List<Sequence> result) {

                    seqList = ((List<Sequence>) result);

                    MapEvent me = new MapEvent();
                    // Only if sequence(s)
                    if (seqList.size() > 0 ) {

                            me.setSource(seqList.get(0));
                            me.setPosition(1);
                            changedPosition(me, sens);
                    }
            }
        };
        
        try {
            callForBDInfoService.getAccessions(Genoscapist.currentSpecies, callback);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getAccessions : " + e1.getMessage());
	    }
	}
	
	/** Change of position **/
	public static void changedPosition(MapEvent e, String sens) {
						
		physicalMap = new PhysicalMap(Genoscapist.ressource);
						
        if ( e.getPosition() < 1 )      { physicalMap.setSequence(e.getSource(),  1,  1 + Genoscapist.sizeSequence); }
        else {

        	if ( Genoscapist.appControler.getGeneSelected() == null ) {
        		        		
        		int start = Genoscapist.appControler.getStart_view();
        		if ( sens.contains("beforeSmall") == true ) 	{ start = start - (Genoscapist.sizeSequence/2); }
        		else if ( sens.contains("afterSmall") == true ) { start = start + (Genoscapist.sizeSequence/2); }
        		else if ( sens.contains("before") == true ) 	{ start = start - Genoscapist.sizeSequence; }
        		else if ( sens.contains("after") == true ) 		{ start = start + Genoscapist.sizeSequence; }

        		if ( start <= 0 ) { start = 1; }
        		else if ( start >= e.getSource().getLength() ) { start = e.getSource().getLength() - Genoscapist.sizeSequence + 1; }
        		
        		Genoscapist.appControler.setStart_view(start);
        		        		
        		// URL
        		History.newItem("&start="+(Genoscapist.appControler.getStart_view())+"&size="+Genoscapist.sizeSequence, false);
        		     		
        		physicalMap.setSequence(e.getSource(), start, start + Genoscapist.sizeSequence);
        	}
        	else {
        		
        		// If selected gene, new view start
	   			int size = Genoscapist.appControler.getGeneSelected().getEnd() - Genoscapist.appControler.getGeneSelected().getStart() + 1;
	   			int diff = (Genoscapist.sizeSequence - size) / 2;
	  			int init = Genoscapist.appControler.getGeneSelected().getStart() - diff;

	        	// If init is out of a window
	        	if ( init <= 0 ) 	{ init = 1; }
	        	Genoscapist.appControler.setStart_view(init);
	        		        	 	
	        	// URL
        		History.newItem("&id="+Genoscapist.appControler.getGeneSelected().getLocusTag()+"&size="+Genoscapist.sizeSequence, false);
	        	
	        	// Physic map
	        	physicalMap.setSequence(e.getSource(),  init,  init + Genoscapist.sizeSequence);
        	}
        }
        
        physicalMap.showFeatures("CDS");
	}
}