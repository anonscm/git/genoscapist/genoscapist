/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.content;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.client.RPCall.CallForBDInfo;
import fr.inrae.jouy.client.RPCall.CallForBDInfoAsync;
import fr.inrae.jouy.shared.Cluster;
import fr.inrae.jouy.shared.GeneItem;

public class LoadingDialog {
	
	private final CallForBDInfoAsync callForBDInfoService = (CallForBDInfoAsync) GWT.create(CallForBDInfo.class);
	
	public LoadingDialog(String type) {

         if(type.compareTo("All_genes")==0){

                 loadAllGenes();
         }
         
         else if(type.compareTo("All_clusters")==0){
        	 
        	 loadAllClusters();
         }
 }

	private void loadAllGenes() {
		
		ContentView.oracleIdentifier.clear();
		
		ArrayList<GeneItem> aloiReturned = Genoscapist.appControler.getListAllGeneItem();
		
		ArrayList<String> aloiReturnedAsString = new ArrayList<String>();
		
		for ( int i = 0 ; i < aloiReturned.size() ; i++ ) {
		
			GeneItem gi = aloiReturned.get(i);
		    aloiReturnedAsString.add(gi.getName());
		    aloiReturnedAsString.add(gi.getLocusTag());
		}
		
		ContentView.oracleIdentifier.addAll(aloiReturnedAsString);
	}
	
	private void loadAllClusters() {
		
		AsyncCallback<ArrayList<Cluster>> callback = new AsyncCallback<ArrayList<Cluster>>() {

            public void onFailure(Throwable caught) {
                    Window.alert("RPC callForDBInfoService.getAllCluster failed : " + caught.getMessage());
            }

            public void onSuccess(ArrayList<Cluster> aloiReturned) {

                    Genoscapist.appControler.setListAllCluster(aloiReturned);

                    ContentView.oracleCluster.clear();

                    ArrayList<String> aloiReturnedAsString = new ArrayList<String>();

                    for ( int i = 0 ; i < aloiReturned.size() ; i++ ) {

                            Cluster ci = aloiReturned.get(i);
                            aloiReturnedAsString.add(ci.getName());
                    }

                    ContentView.oracleCluster.addAll(aloiReturnedAsString);
            }
		};
		
		try {
			callForBDInfoService.getClusters(callback);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getClusters : "
	                            + e1.getMessage());
	    }
	}
}