/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.RPCall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.inrae.jouy.shared.Axis;
import fr.inrae.jouy.shared.Cluster;
import fr.inrae.jouy.shared.DeletedRegion;
import fr.inrae.jouy.shared.Experience;
import fr.inrae.jouy.shared.Expression;
import fr.inrae.jouy.shared.Feature;
import fr.inrae.jouy.shared.GeneItem;
import fr.inrae.jouy.shared.Promoter;
import fr.inrae.jouy.shared.Region;
import fr.inrae.jouy.shared.RegionUpRho;
import fr.inrae.jouy.shared.Regulator;
import fr.inrae.jouy.shared.Segment;
import fr.inrae.jouy.shared.Sequence;
import fr.inrae.jouy.shared.Species;
import fr.inrae.jouy.shared.Terminator;
import fr.inrae.jouy.shared.Transterm;

public interface CallForBDInfoAsync {
	
	void getSeqLength(AsyncCallback<Integer> callback) throws Exception;
	
	void getGeneItem(String type, String geneName, AsyncCallback<GeneItem> callback) throws Exception;
	
	void getFeatures(final Sequence s, String type, long from, long to, AsyncCallback<List<Feature>> callback) throws Exception;

	void getTransterm(Sequence s, long from, long to, AsyncCallback<List<Transterm>> callback ) throws Exception;
	
	void getPromoter(Sequence s, long from, long to, AsyncCallback<List<Promoter>> callback) throws Exception;
	
	void getTerminator(Sequence s, long from, long to, AsyncCallback<List<Terminator>> callback) throws Exception;
	
	void getRegulator(Sequence s, long from, long to, AsyncCallback<List<Regulator>> callback ) throws Exception;
	
	void getSegment(Sequence s, long from, long to, AsyncCallback<List<Segment>> callback ) throws Exception;
	
	void getRegionUpRho(Sequence s, long from, long to, AsyncCallback<List<RegionUpRho>> callback ) throws Exception;
	
	void getRegion(Sequence s, long from, long to, AsyncCallback<List<Region>> callback ) throws Exception;
	
	void getAxis(Sequence s, long from, long to, AsyncCallback<List<Axis>> callback) throws Exception;
	
	void getQualifiersGenbank(GeneItem gene, AsyncCallback<HashMap<String, String>> callback) throws Exception;
	
	void getQualifiersInformation(GeneItem gene, AsyncCallback<String> callback) throws Exception;
	
	void getAllGeneItem(AsyncCallback<ArrayList<GeneItem>> callback) throws Exception;
	
	void getClusters(AsyncCallback<ArrayList<Cluster>> callback) throws Exception;
	
	void getExpression(Experience e, int start, int stop, int seqSize, AsyncCallback<Expression> callback);
	
	void getAccessions(Species sp, AsyncCallback<List<Sequence>> callback) throws Exception;
	
	void getSpecie( String name, AsyncCallback<Species> callback ) throws Exception;
	
	void getExperience(String name, int strand, String normalisation, AsyncCallback<Experience> callback) throws Exception;
	
	void getAllExperience(String normalization, AsyncCallback<List<Experience>> callback) throws Exception;
	
	void getAllExperienceDefault(AsyncCallback<List<Experience>> callback) throws Exception;
	
	void getAllDefaultExperience(String normalization, AsyncCallback<List<Experience>> callback) throws Exception;
	
	void getAllExperienceRho(String normalization, AsyncCallback<List<Experience>> callback) throws Exception;
	
	void getGenesWithCluster(Cluster cluster, AsyncCallback<ArrayList<GeneItem>> callback) throws Exception;
	
	void getGenesWithPosition(String positionAsString, AsyncCallback<ArrayList<GeneItem>> callback);
	
	void getReverseExperience(String normalization, List<Experience> listExp, AsyncCallback<List<Experience>> callback) throws Exception;
	
	void getExpRefreshNorm(List<Experience> listExp, String norm, AsyncCallback<List<Experience>> callback) throws Exception;
	
	void getCondition(GeneItem gene, int level, AsyncCallback<String> callback) throws Exception;
	
	void getSegment(String locustag, int level, AsyncCallback<String> callback) throws Exception;

	void getTypeFeature(String feature, AsyncCallback<String> callback);
	
	void getGeneItem(Segment segment, AsyncCallback<GeneItem> callback) throws Exception;
	
	void getDeletedRegion(Sequence s, long from, long to, AsyncCallback<List<DeletedRegion>> callback ) throws Exception;
}