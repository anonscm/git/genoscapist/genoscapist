/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.RPCall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.inrae.jouy.shared.Axis;
import fr.inrae.jouy.shared.Cluster;
import fr.inrae.jouy.shared.DeletedRegion;
import fr.inrae.jouy.shared.Experience;
import fr.inrae.jouy.shared.Expression;
import fr.inrae.jouy.shared.Feature;
import fr.inrae.jouy.shared.GeneItem;
import fr.inrae.jouy.shared.Promoter;
import fr.inrae.jouy.shared.Region;
import fr.inrae.jouy.shared.RegionUpRho;
import fr.inrae.jouy.shared.Regulator;
import fr.inrae.jouy.shared.Segment;
import fr.inrae.jouy.shared.Sequence;
import fr.inrae.jouy.shared.Species;
import fr.inrae.jouy.shared.Terminator;
import fr.inrae.jouy.shared.Transterm;

@RemoteServiceRelativePath("callForBDInfo")
public interface CallForBDInfo extends RemoteService {
	
	public int getSeqLength() throws Exception;
	
	public String getTypeFeature( final String feature ) throws Exception;

	public GeneItem getGeneItem(String type, String geneName) throws Exception;
	
	public List<Feature> getFeatures(final Sequence s, String type, long from, long to ) throws Exception;
	
	public List<Transterm> getTransterm(Sequence s, long from, long to ) throws Exception;
	
	public List<Promoter> getPromoter(Sequence s, long from, long to ) throws Exception;
	
	public List<Terminator> getTerminator(Sequence s, long from, long to ) throws Exception;
	
	public List<Regulator> getRegulator(Sequence s, long from, long to ) throws Exception;
	
	public List<Segment> getSegment(Sequence s, long from, long to ) throws Exception;
	
	public List<RegionUpRho> getRegionUpRho(Sequence s, long from, long to ) throws Exception;
	
	public List<Region> getRegion(Sequence s, long from, long to ) throws Exception;

	public List<Axis> getAxis(Sequence s, long from, long to ) throws Exception;
	
	public HashMap<String, String> getQualifiersGenbank(GeneItem gene) throws Exception;
	
	public String getQualifiersInformation(GeneItem gene) throws Exception;
	
	public ArrayList<GeneItem> getAllGeneItem() throws Exception;

	public ArrayList<Cluster> getClusters() throws Exception;
	
	public Expression getExpression(Experience e, int start, int stop, int seqSize) throws Exception;
	
	public List<Sequence> getAccessions(Species sp) throws Exception;
	
	public Species getSpecie( String name ) throws Exception;

	public Experience getExperience( String name, int strand, String normalisation) throws Exception;
	
	public List<Experience> getAllExperience(String normalization) throws Exception;
	
	public List<Experience> getAllExperienceDefault() throws Exception;
	
	public List<Experience> getAllDefaultExperience(String normalization) throws Exception;
	
	public List<Experience> getAllExperienceRho(String normalization) throws Exception;
	
	public ArrayList<GeneItem> getGenesWithCluster(Cluster cluster) throws Exception;
	
	public ArrayList<GeneItem> getGenesWithPosition(String position) throws Exception;
	
	public List<Experience> getReverseExperience(String normalization, List<Experience> listExp) throws Exception;
	
	public List<Experience> getExpRefreshNorm(List<Experience> listExp, String norm) throws Exception;
	
	public String getCondition(GeneItem gene, int level) throws Exception;
	
	public String getSegment(String locustag, int level) throws Exception;
	
	public GeneItem getGeneItem(Segment segment) throws Exception;
	
	public List<DeletedRegion> getDeletedRegion(Sequence s, long from, long to ) throws Exception;
}