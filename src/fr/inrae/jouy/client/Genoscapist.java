/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;

import fr.inrae.jouy.client.RPCall.CallForBDInfo;
import fr.inrae.jouy.client.RPCall.CallForBDInfoAsync;
import fr.inrae.jouy.client.content.ContentView;
import fr.inrae.jouy.client.content.SuppData;
import fr.inrae.jouy.client.footer.FooterView;
import fr.inrae.jouy.client.header.HeaderView;
import fr.inrae.jouy.client.map.PhysicalMap;
import fr.inrae.jouy.shared.Cluster;
import fr.inrae.jouy.shared.Experience;
import fr.inrae.jouy.shared.GeneItem;
import fr.inrae.jouy.shared.Sequence;
import fr.inrae.jouy.shared.Species;

public class Genoscapist implements EntryPoint {

	public static AppControler appControler = new AppControler();
	
	private final CallForBDInfoAsync callForBDInfoService = (CallForBDInfoAsync) GWT.create(CallForBDInfo.class);
	
	public static String gene;
	public static String cluster;
    public static PhysicalMap physicalMap;
	public static int sizeSequence = 10000;
	public static String ressource;
	public static Species currentSpecies = null;
	public static List<Sequence> seqList = null;
	public static List<Experience> expList = null;
	
	@Override
	public void onModuleLoad() {
		

		DockLayoutPanel p = new DockLayoutPanel(Unit.EM);
		
		HeaderView header = new HeaderView();
		FooterView footer = new FooterView();
		ContentView content = new ContentView();
		
		p.addNorth(header, 10);
		p.addSouth(footer, 5);
		p.add(content);
	    
	    RootLayoutPanel rp = RootLayoutPanel.get();
	    rp.add(p);
	    
	    // Normalization by default
	    Genoscapist.appControler.setNormalization("CustomCDS");
	    
		// Loading data
	    loadExpAll();
	    
	    // Get Specie
	    AsyncCallback<Species> callback = new AsyncCallback<Species>() {
            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("RPC callForBDInfoService.getSpecie failed : " + caught.getMessage());
            }

            @Override
            public void onSuccess(Species result) {

            	currentSpecies = result;
            }
	    };
	    
	    try {
            callForBDInfoService.getSpecie(Config.name_specie, callback);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getSpecie : " + e1.getMessage());
	    }
	    
	    // Get Specie Sequence Length
	    AsyncCallback<Integer> callback2 = new AsyncCallback<Integer>() {
            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("RPC callForBDInfoService.getSeqLength failed : " + caught.getMessage());
            }

            @Override
            public void onSuccess(Integer result) {

            	Genoscapist.appControler.setSeqLen(result);
            }
	    };
	    
	    try {
            callForBDInfoService.getSeqLength(callback2);

	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getSeqLength : " + e1.getMessage());
	    }
	    
	    // Loading GIF
	    ContentView.msg.startProcessing();
	    
	    // Loading data
	    loadExpDefault();
	    loadAllGeneItem();
	    
	    // History
	    History.addValueChangeHandler(new ValueChangeHandler<String>() {

	    	// RegExp history
	    	RegExp details = RegExp.compile("&id=(.+)&size=(.+)");
	    	RegExp cluster = RegExp.compile("&cluster=(.+)");
	    	RegExp suppData = RegExp.compile("&supplementaryData");
	    	RegExp positionScale = RegExp.compile("&position=([0-9]+)&scale=(.+)");	
	    	RegExp param = RegExp.compile("&norm=(.+)&id=(.+)&rho=(0|1)&scale=(.+)&fname=(0|1)&exp=(.+)");	    	
	    	RegExp choice = RegExp.compile("&norm=");
	    	RegExp startSize = RegExp.compile("&start=");
	    	
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				
				String historyToken = event.getValue();
				
				try {
					if ( !historyToken.isEmpty() ) {
						
						MatchResult matcherDetails = details.exec(historyToken);
						MatchResult matcherCluster = cluster.exec(historyToken);
						MatchResult matcherSuppData = suppData.exec(historyToken);
						MatchResult matcherPositionScale = positionScale.exec(historyToken);
						MatchResult matcherParam = param.exec(historyToken);
						MatchResult matcherChoice = choice.exec(historyToken);
						MatchResult matcherStartSize = startSize.exec(historyToken);
						
						// History complete
						if ( matcherChoice != null ) {
														
							String url = historyToken; url = url.replaceFirst("&", "");
							String[] test = url.split("&");
							String id = "";
							String[] exps = null;
							String mode = "";
							for ( int i = 0 ; i < test.length ; i++ ) {
								
								String[] opt = test[i].split("=");
															
								// Normalization
								if ( opt[0].contains("norm") == true && opt[1].equalsIgnoreCase("median"))			{ Genoscapist.appControler.setNormalization("Median"); }
								else if ( opt[0].contains("norm") == true && opt[1].equalsIgnoreCase("customcds"))	{ Genoscapist.appControler.setNormalization("CustomCDS"); }
								// Position
								else if ( opt[0].contains("position") == true)										{ mode = "position"; Genoscapist.appControler.setStart_view(Integer.parseInt(opt[1])); Genoscapist.appControler.setGeneSelected(null); }
								// ID
								else if ( opt[0].contains("id") == true )											{ mode = "gene"; id = opt[1]; }
								// Rho
								else if ( opt[0].contains("rho") == true && opt[1].contains("0") )					{ Genoscapist.appControler.setRhoActive(false); }
								else if ( opt[0].contains("rho") == true && opt[1].contains("1") )					{ Genoscapist.appControler.setRhoActive(true); }
								// Features Names
								else if ( opt[0].contains("fname") == true && opt[1].contains("0") )				{ Genoscapist.appControler.setAddFeatureNameActive(false); }
								else if ( opt[0].contains("fname") == true && opt[1].contains("1") )				{ Genoscapist.appControler.setAddFeatureNameActive(true); }
								// Scale
								else if ( opt[0].contains("scale") == true ) {
									if ( opt[1].contains("normal") == true ) 										{ Genoscapist.sizeSequence = 10000; }
									else if ( opt[1].contains("large") == true ) 									{ Genoscapist.sizeSequence = 20000; }
									else if ( opt[1].contains("huge") == true ) 									{ Genoscapist.sizeSequence = 40000; }
									else if ( opt[1].contains("small") == true ) 									{ Genoscapist.sizeSequence = 5000; }
									else if ( opt[1].contains("tiny") == true ) 									{ Genoscapist.sizeSequence = 2500; }
								}
								// Experiences
								else if ( opt[0].contains("exp") == true ) {
									
									Genoscapist.appControler.listAllSelectedExperience.clear();
									exps = opt[1].split(";");
									for ( int j = 0 ; j < exps.length ; j++ ) {
										
										String[] e = exps[j].split(",");
										
										// Get Experience (strand +)
										final AsyncCallback<Experience> callback = new AsyncCallback<Experience>() {

							                @Override
							                public void onFailure(Throwable caught) {
							                	Window.alert("No experience found !!!");
							                }

							                @Override
							                public void onSuccess(Experience result) {
							                	result.setColorSelect(e[1]);
							                	Genoscapist.appControler.listAllSelectedExperience.add(result);
							                }
										};

							    		try {
							        		callForBDInfoService.getExperience(e[0], 1, Genoscapist.appControler.getNormalization(), callback);
			
							        	} catch (Exception e1) {
							        		Window.alert("ERROR callForDBInfoService.getExperience : " + e1.getMessage());
							        	}
							    		
							    		// Get Experience (strand -)
							    		final AsyncCallback<Experience> callback2 = new AsyncCallback<Experience>() {

							                @Override
							                public void onFailure(Throwable caught) {
							                	Window.alert("No experience found !!!");
							                }

							                @Override
							                public void onSuccess(Experience result) {
							                	result.setColorSelect(e[1]);
							                	Genoscapist.appControler.listAllSelectedExperience.add(result);
							                }
										};

							    		try {
							        		callForBDInfoService.getExperience(e[0], -1, Genoscapist.appControler.getNormalization(), callback2);
			
							        	} catch (Exception e1) {
							        		Window.alert("ERROR callForDBInfoService.getExperience : " + e1.getMessage());
							        	}
									}
								}
							}
							
							if ( mode.contains("gene") == true )	{ ContentView.selectGene(id); }
							else									{ ContentView.move(""); }
							
						}
						
						// History like "#&norm=quantile&id=SAOUHSC_00001&rho=0&scale=normal&fname=0&exp=intTHP1-2h_1,0;RPMI/FCS-exp_1,0;CO2-2h_1,0"
						else if ( matcherParam != null ) {
														
							String url = historyToken;
							String id = url.replaceAll("(.+)&id=", ""); id = id.replaceAll("&rho=(.+)", "");
							String exp = url; exp = exp.replaceAll("(.+)&exp=", "");
							
							// Normalization
							if ( url.contains("norm=median") == true ) 			{ Genoscapist.appControler.setNormalization("Median"); }
							else if ( url.contains("norm=quantile") == true ) 	{ Genoscapist.appControler.setNormalization("CustomCDS"); }
							else 												{ Window.alert("This normalisation (" + url + ") does not exist!"); }
							// Rho
							if ( url.contains("rho=0") == true ) 				{ Genoscapist.appControler.setRhoActive(false); }
							else if ( url.contains("rho=1") == true ) 			{ Genoscapist.appControler.setRhoActive(true); }
							else 												{ Window.alert("This option for rho (" + url + ") is not possible!"); }
							// Feature names
							if ( url.contains("fname=0") == true ) 				{ Genoscapist.appControler.setAddFeatureNameActive(false); }
							else if ( url.contains("fname=1") == true ) 		{ Genoscapist.appControler.setAddFeatureNameActive(true); }
							else 												{ Window.alert("This option for fname (" + url + ") is not possible!"); }
							// Scale
							if ( url.contains("scale=normal") == true ) 		{ Genoscapist.sizeSequence = 10000; }
							else if ( url.contains("scale=large") == true ) 	{ Genoscapist.sizeSequence = 20000; }
							else if ( url.contains("scale=huge") == true ) 		{ Genoscapist.sizeSequence = 40000; }
							else if ( url.contains("scale=small") == true ) 	{ Genoscapist.sizeSequence = 5000; }
							else if ( url.contains("scale=tiny") == true ) 		{ Genoscapist.sizeSequence = 2500; }
								
							// Experiences
							Genoscapist.appControler.listAllSelectedExperience.clear();

							String[] exps = exp.split(";");
							for ( int i = 0 ; i < exps.length ; i++ ) {
								
								String[] e = exps[i].split(",");
								
								// Get Experience (strand +)
								final AsyncCallback<Experience> callback = new AsyncCallback<Experience>() {

					                @Override
					                public void onFailure(Throwable caught) {
					                	Window.alert("No experience found !!!");
					                }

					                @Override
					                public void onSuccess(Experience result) {
					                	if ( e[1].contains("0") == false ) { result.setColorSelect(e[1]); }
					                	Genoscapist.appControler.listAllSelectedExperience.add(result);
					                }
								};

					    		try {
					        		callForBDInfoService.getExperience(e[0], 1, Genoscapist.appControler.getNormalization(), callback);
	
					        	} catch (Exception e1) {
					        		Window.alert("ERROR callForDBInfoService.getExperience : " + e1.getMessage());
					        	}
					    		
					    		// Get Experience (strand -)
					    		final AsyncCallback<Experience> callback2 = new AsyncCallback<Experience>() {

					                @Override
					                public void onFailure(Throwable caught) {
					                	Window.alert("No experience found !!!");
					                }

					                @Override
					                public void onSuccess(Experience result) {
					                	if ( e[1].contains("0") == false ) { result.setColorSelect(e[1]); }
					                	Genoscapist.appControler.listAllSelectedExperience.add(result);
					                }
								};

					    		try {
					        		callForBDInfoService.getExperience(e[0], -1, Genoscapist.appControler.getNormalization(), callback2);
	
					        	} catch (Exception e1) {
					        		Window.alert("ERROR callForDBInfoService.getExperience : " + e1.getMessage());
					        	}
							}
							
							ContentView.selectGene(id);
						}
						
						// History like "&id=SAOUHSC_00001&size=10000"
						else if ( matcherDetails != null ) {
							
							String url = historyToken;
							String name = url.replaceAll("&size=(.+)", ""); name = name.replaceAll("&id=", "");
							String size = url.replaceAll("&id=(.+)&size=", "");
							Genoscapist.sizeSequence = Integer.parseInt(size);
							ContentView.checkSeqSize();
							ContentView.selectGene(name);
						}
						
						// History like "cluster=C1"
						else if ( matcherCluster != null ) {
							
							String url = historyToken.replaceAll("&cluster=", "");
							
							// Get Clusters
					    	final AsyncCallback<ArrayList<Cluster>> callback = new AsyncCallback<ArrayList<Cluster>>() {

					          @Override
					          public void onFailure(Throwable caught) {
					          	
					          	Window.alert("No cluster found !!!");
					          }

					          @Override
					          public void onSuccess(ArrayList<Cluster> result) {
					        		
					        		Genoscapist.appControler.setListAllCluster(result);
					        		ContentView.selectCluster(url);
					          }
					      };
					  	
					      try {
					  		callForBDInfoService.getClusters(callback);

					  		} catch (Exception e1) {
					  			Window.alert("ERROR callForDBInfoService.getClusters() : " + e1.getMessage());
					  		}
						}
						
						// History Supplementary Data
						else if ( matcherSuppData != null ) {
							
							ContentView.msg.stopProcessing();
							
							if ( Config.abbr == "aeb" ) { ContentView.html_suppData.setHTML(SuppData.AEB); }
							else 						{ ContentView.html_suppData.setHTML(SuppData.SEB); }
							
							ContentView.mainContentView.showWidget(2);
						}
						
						// History like "position=10000&scale=normal"
						else if ( matcherPositionScale != null ) {
							
							String url = historyToken.replaceAll("&position=", "");
							String pos = url; pos = pos.replaceAll("&scale=(.+)", "");
							String scale = url; scale = scale.replaceAll("([0-9]+)&scale=", "");

							if ( scale.contains("normal") == true ) 		{ Genoscapist.sizeSequence = 10000; }
							else if ( scale.contains("large") == true ) 	{ Genoscapist.sizeSequence = 20000; }
							else if ( scale.contains("huge") == true ) 		{ Genoscapist.sizeSequence = 40000; }
							else if ( scale.contains("small") == true ) 	{ Genoscapist.sizeSequence = 5000; }
							else if ( scale.contains("tiny") == true ) 		{ Genoscapist.sizeSequence = 2500; }
							
							Genoscapist.appControler.setStart_view(Integer.parseInt(pos));							
							Genoscapist.appControler.setGeneSelected(null);
							
							ContentView.move("");
						}
						
						// History like "start=1&size=10000"
						else if ( matcherStartSize != null ) {
														
							String url = historyToken.replaceAll("&start=", "");
							String start = url; start = start.replaceAll("&size=(.+)", "");
							String tmp = url; int size = Integer.parseInt(tmp.replaceAll("([0-9]+)&size=", ""));
							
							Genoscapist.appControler.setStart_view(Integer.parseInt(start));
							Genoscapist.appControler.setGeneSelected(null);
							Genoscapist.sizeSequence = size;
														
							ContentView.move("");
						}
					}
					
					// History is null
                    else {
                    	ContentView.create();
                    }
				}
				catch ( IndexOutOfBoundsException e ) {
                    Window.alert("History.addValueChangerHandler IndexOutOfBoundsException : "+ e.getStackTrace().toString());
				}
			}
		});	    
	}
	
	/** Loading of genes list **/
	private void loadAllGeneItem() {
		
		// Get All Genes
        AsyncCallback<ArrayList<GeneItem>> callback = new AsyncCallback<ArrayList<GeneItem>>() {

            public void onFailure(Throwable caught) {
            	
                    Window.alert("RPC callForGenesInfoService.getAllGeneItem failed : " + caught.getMessage());
            }

            public void onSuccess(ArrayList<GeneItem> aloiReturned) {
            	
                    if (aloiReturned == null || aloiReturned.isEmpty()){
                            Window.alert("Problem with loadAllGeneItem in Genoscapist.java !!!!");
                    }
                    
                    else {
                    		Genoscapist.appControler.setListAllGeneItem(aloiReturned);

                    	 	if ( History.getToken().isEmpty() ) {
                    	 	 	
                    	 		ContentView.create();
                    	 	}
                    }
            }
	    };
	
	    try {
	            callForBDInfoService.getAllGeneItem(callback);
	
	    } catch (Exception e1) {
	            Window.alert("ERROR callForBDInfoService.getAllGeneItem : " + e1.getMessage());
	    }
	}
	
	/** Loading of default experiences list **/
    public void loadExpDefault() {
    	     
    	// Get All Default Experiences
    	final AsyncCallback<List<Experience>> callback = new AsyncCallback<List<Experience>>() {

          @Override
          public void onFailure(Throwable caught) {
          	
          	Window.alert("No experience found !!!");
          }

          @Override
          public void onSuccess(List<Experience> result) {
        		
        		Genoscapist.appControler.setListAllSelectedExperience(result);
        		Genoscapist.appControler.setListAllDefaultExperience(result);

        	 	if ( !History.getToken().isEmpty() ) {
        	 	 			
        	 		History.fireCurrentHistoryState();
        	 	}
          }
      };
  	
      try {
  		callForBDInfoService.getAllDefaultExperience(Genoscapist.appControler.getNormalization(), callback);

  		} catch (Exception e1) {
  			Window.alert("ERROR callForDBInfoService.loadExpDefault : " + e1.getMessage());
  		}
    }
    
	/** Loading of experiences list **/
    public void loadExpAll() {
    	        
    	// Get All Experiences
    	final AsyncCallback<List<Experience>> callback = new AsyncCallback<List<Experience>>() {

          @Override
          public void onFailure(Throwable caught) {
          	
          	Window.alert("No experience found !!!");
          }

          @Override
          public void onSuccess(List<Experience> result) {
        		
        		Genoscapist.appControler.setListAllExperience(result);
          }
      };
  	
      try {
  		callForBDInfoService.getAllExperience(Genoscapist.appControler.getNormalization(), callback);

  		} catch (Exception e1) {
  			Window.alert("ERROR callForDBInfoService.loadExpAll : " + e1.getMessage());
  		}
    }
}