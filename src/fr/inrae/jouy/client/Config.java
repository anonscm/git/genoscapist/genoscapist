/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client;

public class Config {

	// Update date
	public static String date = "11 May 2020";
	
	// Images URL
	public static String url_img = "[profile_url]";
	
	// URL
	public static String url = "http://127.0.0.1:8888/Genoscapist.html";
	
	// SEB Deploy
	public static String abbr = "seb";
	public static String name = "<i>B. subtilis</i> Expression Data Browser";
	public static String name_specie = "AL009126";
//	public static String url = "https://genoscapist.migale.inrae.fr/seb/";
	
	// AEB Deploy
//	public static String abbr = "aeb";
//	public static String name = "<i>S. aureus</i> Expression Data Browser";
//	public static String name_specie = "CP000253";
//	public static String url = "https://genoscapist.migale.inrae.fr/aeb/";
}