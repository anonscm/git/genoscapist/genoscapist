/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.footer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import fr.inrae.jouy.client.Config;

public class FooterView extends Composite {

	private static FooterUiBinder uiBinder = GWT.create(FooterUiBinder.class);

	interface FooterUiBinder extends UiBinder<Widget, FooterView> {
	}

	public interface ImageResources extends ClientBundle {

        public static final ImageResources INSTANCE = GWT.create(ImageResources.class);

        @Source("logo-basysbio.gif")
        ImageResource img1();

        @Source("logo_transregio34.png")
        ImageResource img2();
        
        @Source("MaiageLogo.png")
        ImageResource img3();

        @Source("inrae.jpg")
        ImageResource img4();    
	}
	
	@UiField HTML footer;	
	@UiField Image logo1;
	@UiField Image logo2;
	@UiField Image logo3;
	@UiField Image logo4;
	
	@UiHandler("logo1")
	void onLogo1Click(ClickEvent e) {
		Window.open("http://www.basysbio.com/", "Basysbio", null);
	}
	
	@UiHandler("logo3")
    void onLogo3Click(ClickEvent e) {
    	
		Window.open("http://maiage.inrae.fr/", "MaIAGE", null);
	}
	
	@UiHandler("logo4")
    void onLogo4Click(ClickEvent e) {
    	
         Window.open("http://www.inra.fr/", "INRAE", null);
	}
	
	public FooterView() {
		
		initWidget(uiBinder.createAndBindUi(this));
		
		footer.setHTML("<div class='h2-withoutborder'><b>Copyright © 2020 - INRAE - MaIAGE</b><br/>Last update: " + Config.date + "</font><br/>");
		
		logo2.setStyleName("img.no");
		
		logo1.setResource(ImageResources.INSTANCE.img1());
        logo1.setTitle("BasysBio");
        logo1.setSize("90px", "50px");
        
        if ( Config.abbr == "aeb" ) {
        	logo2.setResource(ImageResources.INSTANCE.img2());
        	logo2.setTitle("Transregio34");
        	logo2.setSize("135px", "50px");
        }
        else {
        	logo2.setVisible(false);
        }
        
        logo3.setResource(ImageResources.INSTANCE.img3());
    	logo3.setTitle("MaIAGE");
    	logo3.setSize("190px", "32px");
    	
        logo4.setResource(ImageResources.INSTANCE.img4());
        logo4.setTitle("INRAE");
        logo4.setSize("120px", "30px");
	}
}