/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.map;

import java.util.ArrayList;
import java.util.List;

import org.vectomatic.dom.svg.OMSVGElement;
import org.vectomatic.dom.svg.OMSVGPathElement;
import org.vectomatic.dom.svg.OMSVGPathSegList;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.utils.SVGConstants;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.inrae.jouy.client.Config;
import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.client.RPCall.CallForBDInfo;
import fr.inrae.jouy.client.RPCall.CallForBDInfoAsync;
import fr.inrae.jouy.client.content.ContentView;
import fr.inrae.jouy.gfx.BioContext;
import fr.inrae.jouy.gfx.action.FeatureListener;
import fr.inrae.jouy.gfx.bio.Arrow;
import fr.inrae.jouy.gfx.bio.Cube;
import fr.inrae.jouy.gfx.bio.Flag;
import fr.inrae.jouy.gfx.curve.Abs;
import fr.inrae.jouy.gfx.curve.Curve;
import fr.inrae.jouy.shared.Axis;
import fr.inrae.jouy.shared.DeletedRegion;
import fr.inrae.jouy.shared.Experience;
import fr.inrae.jouy.shared.Expression;
import fr.inrae.jouy.shared.Feature;
import fr.inrae.jouy.shared.GeneItem;
import fr.inrae.jouy.shared.Promoter;
import fr.inrae.jouy.shared.Region;
import fr.inrae.jouy.shared.RegionUpRho;
import fr.inrae.jouy.shared.Regulator;
import fr.inrae.jouy.shared.Segment;
import fr.inrae.jouy.shared.Sequence;
import fr.inrae.jouy.shared.Terminator;
import fr.inrae.jouy.shared.Transterm;

public class PhysicalMap extends Map {

	private List<FeatureListener> featureListeners = new ArrayList<FeatureListener>();
	
    private final CallForBDInfoAsync callForBDInfoService = (CallForBDInfoAsync) GWT.create(CallForBDInfo.class);
	
	private float axeY = 0;
    public static float widthWithoutMargin = 0f;

    protected BioContext bc = new BioContext();
    
    /** Start position of sequence **/
    protected long from;
    /** End position of sequence **/
    protected long to;
    
    /** Base number to be displayed **/
    private int  nbLine = 1; //5;
    /** Base number per line (default 10000) **/
    protected long currentBasePerLine = Genoscapist.sizeSequence;
    private int  spaceBetweenAxes = 80;
    
    /** Display sequence **/
    private Sequence sequenceToShow = null;
    public long getFrom() {
            return from;
    }

    public void setFrom(long from) {
            this.from = from;
    }

    public Sequence getSequenceToShow() {
            return sequenceToShow;
    }

    public void setSequenceToShow(Sequence sequenceToShow) {
            this.sequenceToShow = sequenceToShow;
    }
    
    /** All features of the sequence **/
    private List<Feature> globalFeatToDraw = new ArrayList<Feature>();
   
    private String ressource;
    
    public PhysicalMap(String ressource) {
    	    	
        this.ressource = ressource;
        startY = 110;
        startX = 30;
        
        if ( Genoscapist.appControler.rhoActive == true ) {
        	setBounds(0, 0, 1200, 700);
        }
        else {
        	setBounds(0, 0, 1200, 700);
        }
        
        canvas.addMouseDownHandler(new MouseDownHandler() {

                @Override
                public void onMouseDown(MouseDownEvent event) {
                }
        });
    }
    
    /** Adding of sequence in the graphical environment **/
    public void setSequence( Sequence s, long f, long t ) {
    	
        sequenceToShow = s;
        from = f;
        to   = t;
        startY = 60;
        globalFeatToDraw.clear();
                  
        if ( from < 1 ) from = 1;
        if ( to   > s.getLength() ) to = s.getLength();
    }
    
    public void showFeatures( final String typeFeatToDraw ) {
    	
        final PhysicalMap map = this;
        
        ArrayList<Feature> tmp = new ArrayList<Feature>();
        Genoscapist.appControler.setListCDS(tmp);

        AsyncCallback<List<Feature>> callback = new AsyncCallback<List<Feature>>() {
        	
        	public void onFailure(Throwable caught) {
        		
        		Window.alert("RPC callForDBInfoService.getFeatures failed : " + caught.getMessage());
        	}
        	
        	public void onSuccess(List<Feature> aloiReturned) {
        		
        		globalFeatToDraw = aloiReturned;
        		        			
        		for ( Feature feat : aloiReturned ) {
        				
        			if ( feat.getType().contains("CDS") == true ) {
	        	        Genoscapist.appControler.listCDS.add(feat);
        	        }
        		}
        		        			        			
        		map.paint();
        			
        		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

					@Override
					public void execute() {
							
						map.paintCourbe();
							
						// Multiple clic
						ContentView.imgBigPlus.setVisible(true);
						ContentView.imgBigMinus.setVisible(true);
						ContentView.imgSmallMinus.setVisible(true);
						ContentView.imgSmallPlus.setVisible(true);
						ContentView.imgNeutral.setVisible(true);
						ContentView.imgBefore.setVisible(true);
						ContentView.imgBefore5000.setVisible(true);
						ContentView.imgAfter.setVisible(true);
						ContentView.imgAfter5000.setVisible(true);
							
						Scheduler.get().scheduleDeferred(new ScheduledCommand() {

							@Override
							public void execute() {
									
								if ( Genoscapist.appControler.rhoActive == true ) {
									map.paintCourbeRho();
								}
									
								if ( Genoscapist.appControler.addFeatureNameActive == true ) {
							        Timer t = new Timer() {
									      @Override
									      public void run() {
									    	  addFeaturesNames();
									      }
									    };

									    // Schedule the timer to run once in 0.5 seconds.										    
									    t.schedule(500);
						        	}
								}
		        			});
						}
        			});
        		}
		};
		
		try {
			callForBDInfoService.getFeatures(sequenceToShow, typeFeatToDraw, from, to, callback);

		} catch (Exception e1) {
            Window.alert("ERROR callForBDInfoService.getFeatures : "
                            + e1.getMessage());
		}
    }
    
    /** Remove Vertical bar (position) **/
    public void removeRepere() {
    	
    	// Genomic mark
    	if ( canvas.getElementById("repere_g") != null ) {
    		canvas.getElementById("repere_g").getElement().removeFromParent();
			canvas.getElementById("pos_g").getElement().removeFromParent();
		}
    	
    	// Profile expression mark
    	if ( canvas2.getElementById("repere_c") != null ) {
    		canvas2.getElementById("repere_c").getElement().removeFromParent();
    	}
    	
    	// Rho mark
    	if ( Genoscapist.appControler.isRhoActive() == true && canvas3.getElementById("repere_r") != null ) {
			canvas3.getElementById("repere_r").getElement().removeFromParent();
		}
    }
    
    /** Create Vertical bar (position) **/
    public void createRepere(int x, boolean test) {
    	
    	removeRepere();
    	
    	if ( x >= Genoscapist.appControler.getStart_view() && x <= (Genoscapist.appControler.getStart_view() + Genoscapist.sizeSequence) ) {
    		
    		int width = 1140;
    		float grad = 1140 / Genoscapist.sizeSequence;
    		float pos = (((x - Genoscapist.appControler.getStart_view()) * width) / Genoscapist.sizeSequence) + 30;
    		
    		// Genomic mark
        	String legend = Integer.toString(x);
//           	final OMSVGElement txtUnits = bc.getDocument().createSVGTextElement(pos-(legend.length() * 3) + grad/2, (float) 30, (short) 5, legend);
           	final OMSVGElement txtUnits = bc.getDocument().createSVGTextElement(pos-(legend.length() * 3) + grad/2, (float) 50, (short) 5, legend);
        	txtUnits.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
        	txtUnits.setId("pos_g");
    		canvas.appendChild(txtUnits);
    		
    		final OMSVGPathElement repere_g = bc.getDocument().createSVGPathElement();
    		repere_g.setId("repere_g");
        	
        	OMSVGPathSegList segs1g = repere_g.getPathSegList();
        	segs1g.appendItem(repere_g.createSVGPathSegMovetoAbs(pos, 56));
        	segs1g.appendItem(repere_g.createSVGPathSegLinetoAbs(pos, 400));
        	
        	OMSVGPathSegList segs2g = repere_g.getPathSegList();
        	segs2g.appendItem(repere_g.createSVGPathSegMovetoAbs(pos+grad, 56));
        	segs2g.appendItem(repere_g.createSVGPathSegLinetoAbs(pos+grad, 400));        	
        	
        	OMSVGPathSegList segs3g = repere_g.getPathSegList();
        	segs3g.appendItem(repere_g.createSVGPathSegMovetoAbs(pos, 56));
        	segs3g.appendItem(repere_g.createSVGPathSegLinetoAbs(pos+grad, 56));
        	
        	OMSVGPathSegList segs4g = repere_g.getPathSegList();
        	segs4g.appendItem(repere_g.createSVGPathSegMovetoAbs(pos, 56));
        	segs4g.appendItem(repere_g.createSVGPathSegLinetoAbs(pos+grad, 56));
        	
        	repere_g.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
    	    repere_g.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
    	    canvas.appendChild(repere_g);
    	    
    	    // Profile expression mark
    	    int yc = 550;
    	    if ( Config.abbr == "seb_mini" ) { yc = 350; }
    	    
    		final OMSVGPathElement repere_c = bc.getDocument().createSVGPathElement();
    		repere_c.setId("repere_c");
    		
        	OMSVGPathSegList segs1c = repere_c.getPathSegList();
        	segs1c.appendItem(repere_c.createSVGPathSegMovetoAbs(pos, 0));
        	segs1c.appendItem(repere_c.createSVGPathSegLinetoAbs(pos, yc));
        	OMSVGPathSegList segs2c = repere_c.getPathSegList();
        	segs2c.appendItem(repere_c.createSVGPathSegMovetoAbs(pos+grad, 0));
        	segs2c.appendItem(repere_c.createSVGPathSegLinetoAbs(pos+grad, yc));
    	    repere_c.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
    	    repere_c.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
    	    canvas2.appendChild(repere_c);
    	    
    	    // Rho mark
    	    if ( Genoscapist.appControler.isRhoActive() == true ) {
    	    	
    	    	int y = 300;
    	    	if ( Config.abbr == "aeb" ) { y = 900; }
    	    	
    	    	final OMSVGPathElement repere_r = bc.getDocument().createSVGPathElement();
    	    	repere_r.setId("repere_r");
            	OMSVGPathSegList segs1r = repere_r.getPathSegList();
            	segs1r.appendItem(repere_r.createSVGPathSegMovetoAbs(pos, 0));
            	segs1r.appendItem(repere_r.createSVGPathSegLinetoAbs(pos, y));
            	OMSVGPathSegList segs2r = repere_r.getPathSegList();
            	segs2r.appendItem(repere_r.createSVGPathSegMovetoAbs(pos+grad, 0));
            	segs2r.appendItem(repere_r.createSVGPathSegLinetoAbs(pos+grad, y));
            	repere_r.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
            	repere_r.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
        	    canvas3.appendChild(repere_r);
    	    }
    	}
    	
    	ContentView.createSVGImage();
    }
    
    /** Translate Position **/
    public void translatePosition(int x) {
    	
    	float tmp = ((x - 30) * currentBasePerLine) / widthWithoutMargin + from;
    	createRepere((int) tmp, true);
    }
    
    /** Create Vertical bar (position) **/
    public void createRepere(int x) {
    	
    	removeRepere();
    	
    	if ( x >= 30 && x <= 1170 ) {
    		
    		double legendTest = ((x - 30) * currentBasePerLine) / widthWithoutMargin + from;
    		String legend = Integer.toString((int) legendTest);
    		final OMSVGElement position = bc.getDocument().createSVGTextElement((float) (x-7),(float) 20, (short) 5, legend);
    		position.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
    		position.setId("pos_g");
    		canvas.appendChild(position);
    		
    		// Genomic mark
    		final OMSVGPathElement repere_g = bc.getDocument().createSVGPathElement();
    		repere_g.setId("repere_g");
        	OMSVGPathSegList segs1g = repere_g.getPathSegList();
        	segs1g.appendItem(repere_g.createSVGPathSegMovetoAbs(x, 30));
        	segs1g.appendItem(repere_g.createSVGPathSegLinetoAbs(x, 400));
    	    repere_g.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
    	    repere_g.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
    	    canvas.appendChild(repere_g);
    		
    		// Profile expression mark
    	    int yc = 550;
    	    if ( Config.abbr == "seb_mini" ) { yc = 350; }
    	    
    		final OMSVGPathElement repere_c = bc.getDocument().createSVGPathElement();
    		repere_c.setId("repere_c");
        	OMSVGPathSegList segs1c = repere_c.getPathSegList();
        	segs1c.appendItem(repere_c.createSVGPathSegMovetoAbs(x, 0));
        	segs1c.appendItem(repere_c.createSVGPathSegLinetoAbs(x, yc));
    	    repere_c.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
    	    repere_c.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
    	    canvas2.appendChild(repere_c);
    	    
    	    // Rho mark
    	    if ( Genoscapist.appControler.isRhoActive() == true ) {
    	    	
    	    	int y = 300;
    	    	if ( Config.abbr == "aeb" ) { y = 900; }
    	    	
    	    	final OMSVGPathElement repere_r = bc.getDocument().createSVGPathElement();
    	    	repere_r.setId("repere_r");
            	OMSVGPathSegList segs1r = repere_r.getPathSegList();
            	segs1r.appendItem(repere_r.createSVGPathSegMovetoAbs(x, 0));
            	segs1r.appendItem(repere_r.createSVGPathSegLinetoAbs(x, y));
            	repere_r.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY,SVGConstants.CSS_PURPLE_VALUE);
            	repere_r.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
        	    canvas3.appendChild(repere_r);
    	    }
		}
    	
    	ContentView.createSVGImage();
    }
    
    /** Rho Expression Profiles View **/
    void createMapCourbeRho(final OMSVGSVGElement canvas3, int localWidth, int localHeight) {
    	    	
    	boolean r = this.getComponentCurveRho(Genoscapist.appControler.getNormalization());
    	
        ContentView.viewDockCourbeRho.getElement().appendChild(canvas3.getElement());
    }
    
    /** Expression Profiles View **/
    void createMapCourbe(final OMSVGSVGElement canvas2, int localWidth, int localHeight) {
    	
    	boolean c = this.getComponentCurveTest(Genoscapist.appControler.getNormalization());
    	
    	if ( Config.abbr == "seb_min" ) {
    		boolean d = this.getComponentDeletedRegion(Genoscapist.appControler.getNormalization());
    	}
    	ContentView.viewDockCourbe.getElement().appendChild(canvas2.getElement());
    }
        
    /** Genomic View **/
    public void createMap(OMSVGSVGElement canvas, int localWidth, int localHeight) {
    	
    	// AXIS
    	// ----
	    Abs a = null;
	    axeY = startY;	// y position of axis
	
	    widthWithoutMargin = localWidth - 2 * (int) startX;  // Image size without marges
	
	    long startBase = from;                              // Start
	
	    bc.setBpPerPixel(currentBasePerLine / widthWithoutMargin);
	    bc.setDocument(doc);
	
	    int sBA = spaceBetweenAxes;
	
	    // Number of lines to display
	    if ( to < 1 )   { to = Genoscapist.sizeSequence + 1; }
	    nbLine = (int) (to-from) / Genoscapist.sizeSequence;
	    if ( to == sequenceToShow.getLength() ) { nbLine++; }
	    
		// For each line --> axis
	    for ( int i = 0 ; i < nbLine ; i++ ) {
	    	
	    	a = new Abs(bc, startX, axeY, widthWithoutMargin);
	    	
	    	a.create(startBase, startBase+currentBasePerLine-1);
	    		    		
	        a.paint(canvas);
	        axeY += sBA;
	        startBase += currentBasePerLine;
	    }
	    
        // Context
        bc.setBpPerLine((long) currentBasePerLine);
        	      
       	// ELEMENTS FOR THE GENOMIC VIEW
        // -----------------------------
	    for ( Feature featToDraw : globalFeatToDraw ) {
	        	
	            if ( featToDraw.getType().equalsIgnoreCase("CDS") ) {
	            	boolean bool = this.getComponentCDS(featToDraw);
	            }
	    }	        
	    boolean boolRegulator = this.getComponentRegulator();
	    boolean boolTranstermHP = this.getComponentTransterm();
	    boolean boolSegment = this.getComponentSegment();
	    // Axis for promoters
	    a = new Abs(bc, startX, axeY, widthWithoutMargin);
	    a.create(320, true);
	    String color = "rgb(0,0,0)";
        a.paint(canvas, color);
        boolean boolPromoter = this.getComponentPromoter();
	    boolean boolTerminator = this.getComponentTerminator();	        
        boolean boolAxis = this.getComponentAxis();
        
        ContentView.viewDock.getElement().appendChild(canvas.getElement());
    }
    
    /** In case of multiline **/
    public int whichLine( float v ) {

        return (int) ((v-from)/currentBasePerLine);
    }
    
    
    /** Get CDS **/
    public boolean getComponentCDS(final Feature cds) {
            
    	// If Multiline
        // long start = cds.getStart();
        // int line = whichLine(start);
        int line = 0;

        float featStartY = 80;

        final Arrow arrow = new Arrow(cds, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas, bc);
        
        canvas.appendChild(arrow.getArrow());
     
        arrow.getArrow().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
			}
		});
        
        arrow.getArrow().addMouseOutHandler(new MouseOutHandler() {

                @Override
                public void onMouseOut(MouseOutEvent event) {
                	
                    arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "#1EB7D2");
                    arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "#1EB7D2");
                	
                	if ( Genoscapist.appControler.getGeneSelected() != null && arrow.getArrow().getId().contains(Genoscapist.appControler.getGeneSelected().getLocusTag()) ) {
                		
                            arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "#8462B1");
                            arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "#8462B1");
                	}
                	
                	if ( Genoscapist.appControler.addFeatureNameActive == true ) {
                		
                		canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "#1EB7D2");
                    	canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "#1EB7D2");
                    	
                    	if ( Genoscapist.appControler.getGeneSelected() != null && cds.getId_feat().contains(Genoscapist.appControler.getGeneSelected().getLocusTag()) ) {
                    		canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "#8462B1");
                        	canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "#8462B1");
                    	}
                	}                	
                	else {
                		canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
                    	canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
                	}
                }
        });

        arrow.getArrow().addMouseDownHandler(new MouseDownHandler() {
        	
                @Override
                public void onMouseDown(MouseDownEvent event) {

                    arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "#8462B1");
                    arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "#8462B1");
                    
        			History.newItem("&id="+cds.getId_feat(), false);
        			ContentView.selectGene(cds.getId_feat());
                }
        });
       
        arrow.getArrow().addMouseOverHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
            	
            	arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, "#8462B1");
            	arrow.getArrow().getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, "#8462B1");
              
            	canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "#8462B1");
                canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "#8462B1");
            }
        });
        
        return true;
    }
    
    /** Get Curve **/
    public boolean getComponentCurveTest(final String normalization) {
        
        startY = 60;
        
        if ( Genoscapist.appControler.normalization.contains("CustomCDS") == true ) {
        	
        	double minM = 0; double maxM = 0; double minP = 0; double maxP = 0;
        	
        	if ( Config.abbr == "aeb" ) {
        		minM = minP = 4.9;
        		maxM = maxP = 16.3;
        	}
        	else if ( Config.abbr == "seb" ) {
        		minM = minP = 6.8;
        		maxM = maxP = 16.5;
        	}
        	else if ( Config.abbr == "seb_min" ) {
        		minM = minP = 5.9;
        		maxM = maxP = 16.6;
        	}
        	
        	// Strand -
        	Abs axeMinM = new Abs(bc, 30, (int) (120 + 100 - (minM * 5)), widthWithoutMargin);
        	axeMinM.create((int) (120 + 100 - (minM * 5)), true);
        	axeMinM.paint(canvas2, "rgb(81,110,176)", "1", "axeMinM");
            Abs axeMaxM = new Abs(bc, 30, (int) (120 + 100 - (maxM * 5)), widthWithoutMargin);
            axeMaxM.create((int) (120 + 100 - (maxM * 5)), true);
            axeMaxM.paint(canvas2, "rgb(81,110,176)", "1", "axeMaxM");
        	// Strand +
            Abs axeMinP = new Abs(bc, 30, (int) (120  - (minP * 5)), widthWithoutMargin);
            axeMinP.create((int) (120  - (minP * 5)), true);
            axeMinP.paint(canvas2, "rgb(81,110,176)", "1", "axeMinP");
            Abs axeMaxP = new Abs(bc, 30, (int) (120 - (maxP * 5)), widthWithoutMargin);
            axeMaxP.create((int) (120 - (maxP * 5)), true);
            axeMaxP.paint(canvas2, "rgb(81,110,176)", "1", "axeMaxP");
            
        	// Strand -
        	Abs axe1 = new Abs(bc, 30, 170, widthWithoutMargin);
            axe1.create(170, true);
            axe1.paint(canvas2, "rgb(126,88,53)", "5,5", "axe1");
            Abs axe2 = new Abs(bc, 30, 165, widthWithoutMargin);
            axe2.create(165, true);
            axe2.paint(canvas2, "rgb(126,88,53)", "5,5", "axe2");
            // Strand +
            Abs axe3 = new Abs(bc, 30, 70, widthWithoutMargin);
            axe3.create(70, true);
            axe3.paint(canvas2, "rgb(126,88,53)", "5,5", "axe3");
            Abs axe4 = new Abs(bc, 30, 65, widthWithoutMargin);
            axe4.create(65, true);
            axe4.paint(canvas2, "rgb(126,88,53)", "5,5", "axe4");
        }
        else {
        	
        	double minM = 0; double maxM = 0; double minP = 0; double maxP = 0;
        	
        	if ( Config.abbr == "aeb" ) {
        		minM = minP = -4.5;
        		maxM = maxP = 7.4;
        	}
        	else if ( Config.abbr == "seb" ) {
        		minM = minP = -3.8;
        		maxM = maxP = 9.5;
        	}
        	
        	// Strand -
        	Abs axeMinM = new Abs(bc, 30, (int) (120 + 100 - (minM * 5 + 40)), widthWithoutMargin);
        	axeMinM.create((int) (120 + 100 - (minM * 5 + 40)), true);
        	axeMinM.paint(canvas2, "rgb(81,110,176)", "1", "axeMinM");
            Abs axeMaxM = new Abs(bc, 30, (int) (120 + 100 - (maxM * 5 + 40)), widthWithoutMargin);
            axeMaxM.create((int) (120 + 100 - (maxM * 5 + 40)), true);
            axeMaxM.paint(canvas2, "rgb(81,110,176)", "1", "axeMaxM");
        	// Strand +
            Abs axeMinP = new Abs(bc, 30, (int) (120 - (minP * 5 + 40)), widthWithoutMargin);
            axeMinP.create((int) (120 - (minP * 5 + 40)), true);
            axeMinP.paint(canvas2, "rgb(81,110,176)", "1", "axeMinP");
            Abs axeMaxP = new Abs(bc, 30, (int) (120 - (maxP * 5 + 40)), widthWithoutMargin);
            axeMaxP.create((int) (120 - (maxP * 5 + 40)), true);
            axeMaxP.paint(canvas2, "rgb(81,110,176)", "1", "axeMaxP");
            
        	// Strand -
        	Abs axe1 = new Abs(bc, 30, 180, widthWithoutMargin);
            axe1.create(180, true);
            axe1.paint(canvas2, "rgb(126,88,53)", "5,5", "axe1");
            Abs axe2 = new Abs(bc, 30, 168, widthWithoutMargin);
            axe2.create(168, true);
            axe2.paint(canvas2, "rgb(126,88,53)", "5,5", "axe2");
            Abs axe3 = new Abs(bc, 30, 163, widthWithoutMargin);
            axe3.create(163, true);
            axe3.paint(canvas2, "rgb(126,88,53)", "5,5", "axe3");
            // Strand +
            Abs axe4 = new Abs(bc, 30, 68, widthWithoutMargin);
            axe4.create(68, true);
            axe4.paint(canvas2, "rgb(126,88,53)", "5,5", "axe4");
            Abs axe5 = new Abs(bc, 30, 63, widthWithoutMargin);
            axe5.create(63, true);
            axe5.paint(canvas2, "rgb(126,88,53)", "5,5", "axe5");
            Abs axe6 = new Abs(bc, 30, 80, widthWithoutMargin);
            axe6.create(80, true);
            axe6.paint(canvas2, "rgb(126,88,53)", "5,5", "axe6");
        }
                
        for ( int i = 0 ; i < Genoscapist.appControler.listAllSelectedExperience.size() ; i++ ) {
        	
        	final int test = i;
        	
        	final Experience experience = Genoscapist.appControler.listAllSelectedExperience.get(i);
        	
	        final AsyncCallback<Expression> callback = new AsyncCallback<Expression>() {

                @Override
                public void onFailure(Throwable caught) {
                	
                	Window.alert("No expression found !!!");
                }

                @Override
                public void onSuccess(Expression result) {
                	
                	if ( result == null ) {
                		
                		Window.alert("No expression found !!!");
                	}
                	
                	else {

                        final Expression exp = result;
                        
                        final Curve curve = new Curve(exp.getSens(), exp.getPosition(), exp.getSignal(), from, widthWithoutMargin, startY, currentBasePerLine, canvas2, bc, experience.getColorSelect(), experience.getExp());
                        
                        final MyPopup pCurve = new MyPopup(experience.getExp());
                        pCurve.getElement().getStyle().setColor(experience.getColorSelect());
                        
                        curve.getCurve().addMouseOutHandler(new MouseOutHandler() {
							
							@Override
							public void onMouseOut(MouseOutEvent event) {
								
								// Strand +
								OMSVGElement courbePlus = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_1");
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_NONE_VALUE);
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
								// Strand -
								OMSVGElement courbeMoins = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_-1");
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_NONE_VALUE);
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
								
								pCurve.hide();
							}
						});
                        
                        curve.getCurve().addMouseDownHandler(new MouseDownHandler() {
							
							@Override
							public void onMouseDown(MouseDownEvent event) {

								final DialogBox dialogBox = new DialogBox();
								
								dialogBox.setText("About " + experience.getExp());
								dialogBox.setWidth("400px");
								
								VerticalPanel dialogContent = new VerticalPanel();
								dialogContent.setSpacing(4);
								
								HTML title = new HTML("<h3>Description</h3>");
								dialogContent.add(title);
								dialogContent.setCellHorizontalAlignment(title, HasHorizontalAlignment.ALIGN_LEFT);
								HTML desc = new HTML(experience.getDescription());
								dialogContent.add(desc);
								dialogContent.setCellHorizontalAlignment(desc, HasHorizontalAlignment.ALIGN_JUSTIFY);
								
								HTML text = new HTML("<h3>Choose color for selected sample(s)</h3>");
						        dialogContent.add(text);
						        dialogContent.setCellHorizontalAlignment(text, HasHorizontalAlignment.ALIGN_LEFT);
								
						        final ListBox listBox = new ListBox();
						        listBox.addItem("default", experience.getColor());
						        // Colors
						        listBox.addItem("red", "#FF0000");
						        listBox.addItem("orange", "#FF8800");
						        listBox.addItem("yellow", "#FFFF00");
						        listBox.addItem("green", "#00FF00");
						        listBox.addItem("cyan", "#00FFFF");
						        listBox.addItem("blue", "#4444FF");
						        listBox.addItem("pink", "#FF88FF");
						        listBox.addItem("brown", "#773311");
						        listBox.addItem("black", "#000000");
						        // ---------------------------------
						        listBox.setMultipleSelect(false);
						        dialogContent.add(listBox);
						        
						        Button closeButton = new Button("Ok", new ClickHandler() {

					                @Override
					                public void onClick(ClickEvent event) {

					                        dialogBox.hide();
					                        experience.setColorSelect(listBox.getSelectedValue());
					                        
					                        int t = 0;
					                        for ( int j = 0 ; j < Genoscapist.appControler.listAllSelectedExperience.size() ; j++ ) {
					                        
					                        	if ( Genoscapist.appControler.listAllSelectedExperience.get(j).getExp().contains(experience.getExp()) == true ) {
					                        		t = t + 1;
					                        		Genoscapist.appControler.listAllSelectedExperience.get(j).setColorSelect(listBox.getSelectedValue());
					                        		if ( t == 2 ) { break; }
					                        	}
					                        }
					                        
					                        ContentView.disclosurePanelSample.clear();
					                        ContentView.createDisclosurePanelSample(experience.getExp(), listBox.getSelectedValue(), Genoscapist.appControler.getListAllSelectedExperience());
					                        
					                        pCurve.getElement().getStyle().setColor(experience.getColorSelect());
					                        
											// Strand +
											OMSVGElement courbePlus = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_1");
											courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_NONE_VALUE);
											courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
											// Strand -
											OMSVGElement courbeMoins = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_-1");
											courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_NONE_VALUE);
											courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
											
											courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
											courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "1");
											
											ContentView.createSVGImage();											
					                }
						        });
					        
						        dialogContent.add(closeButton);
						        dialogContent.setCellHorizontalAlignment(closeButton, HasHorizontalAlignment.ALIGN_CENTER);
						        
								dialogBox.setWidget(dialogContent);
								
								dialogBox.setGlassEnabled(true);
						        dialogBox.setAnimationEnabled(true);
						        dialogBox.center();
						        dialogBox.show();
						        
								// Strand +
								OMSVGElement courbePlus = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_1");
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_NONE_VALUE);
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
								// Strand -
								OMSVGElement courbeMoins = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_-1");
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_NONE_VALUE);
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
							}
						});                       
                        
                        curve.getCurve().addMouseOverHandler(new MouseOverHandler() {
							
							@Override
							public void onMouseOver(MouseOverEvent event) {
								
								// Strand +
								OMSVGElement courbePlus = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_1");
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, SVGConstants.CSS_NONE_VALUE);
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
								courbePlus.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "2");
								// Strand -
								OMSVGElement courbeMoins = (OMSVGElement) canvas2.getElementById(experience.getExp()+"_-1");
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_FILL_PROPERTY, SVGConstants.CSS_NONE_VALUE);
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_VALUE, experience.getColorSelect());
								courbeMoins.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_WIDTH_PROPERTY, "2");
							  
                              pCurve.setPopupPosition(event.getClientX(), event.getClientY()-40);
                              pCurve.getElement().getStyle().setColor(experience.getColorSelect());
                              pCurve.show();
								
							}
						});
                        
                        canvas2.appendChild(curve.getCurve());
                        
                        if ( test == Genoscapist.appControler.listAllSelectedExperience.size() -1 ) {
                        	getComponentRegion();
                        }
                	}
                }
        	};

    		try {
        		callForBDInfoService.getExpression(experience, (int) from, (int) (from + currentBasePerLine - 1), Genoscapist.appControler.getSeqLen(), callback);

        	} catch (Exception e1) {
        		Window.alert("ERROR callForDBInfoService.getExpression : " + e1.getMessage());
        	}
        }
     
        return true;
    }
    
    /** Get Deleted Regions **/
    public boolean getComponentDeletedRegion(final String normalization) {
        
        startY = 60;
        
        Abs axe1 = new Abs(bc, 30, 265, widthWithoutMargin);
        axe1.create(265, true);
        axe1.paint(canvas2, "#5c5c5c", "2", "dr_axe1");
        Abs axe2 = new Abs(bc, 30, 315, widthWithoutMargin);
        axe2.create(315, true);
        axe2.paint(canvas2, "#c9b500", "2", "dr_axe2");
        Abs axe3 = new Abs(bc, 30, 365, widthWithoutMargin);
        axe3.create(365, true);
        axe3.paint(canvas2, "#009100", "2", "dr_axe3");
        Abs axe4 = new Abs(bc, 30, 415, widthWithoutMargin);
        axe4.create(415, true);
        axe4.paint(canvas2, "#0024c9", "2", "dr_axe4");
        Abs axe5 = new Abs(bc, 30, 465, widthWithoutMargin);
        axe5.create(465, true);
        axe5.paint(canvas2, "#b50091", "2", "dr_axe5");
        Abs axe6 = new Abs(bc, 30, 515, widthWithoutMargin);
        axe6.create(515, true);
        axe6.paint(canvas2, "#bd0000", "2", "dr_axe6");
        
        final AsyncCallback<List<DeletedRegion>> callback = new AsyncCallback<List<DeletedRegion>>() {

                @Override
                public void onFailure(Throwable caught) {
                	
                	Window.alert("Failure getDeletedRegion ::: " + caught.getMessage());
                }

                @Override
                public void onSuccess(List<DeletedRegion> result) {
                	
                	if ( result == null ) {
                		
                		Window.alert("No Deleted Region found !!!");
                	}
                	
                	else {

                        final List<DeletedRegion> listDeletedRegion = result;
                        Genoscapist.appControler.setListAllDeletedRegion(result);
                        
                        for ( int i = 0 ; i < listDeletedRegion.size() ; i++ ) {
                        	
                        	DeletedRegion deletedRegion = listDeletedRegion.get(i);
                        	int start = deletedRegion.getStart();
                        	int line = whichLine(start);
                        	
                        	float featStartY = 260;
                        	if ( deletedRegion.getName().contains("MGP181") == true ) 		{ featStartY = 310; }
                        	else if ( deletedRegion.getName().contains("MGP192") == true )  { featStartY = 360; }
                        	else if ( deletedRegion.getName().contains("MGP229") == true )  { featStartY = 410; }
                        	else if ( deletedRegion.getName().contains("MGP234") == true )  { featStartY = 460; }
                        	else if ( deletedRegion.getName().contains("MGP254") == true )  { featStartY = 510; }
                        	
                        	String name = deletedRegion.getName(); name = name.replaceAll("_[0-9]+$", "");
                        	final MyPopup p = new MyPopup(name);
                    		p.getElement().getStyle().setColor(deletedRegion.getColor());
                            p.getElement().getStyle().setFontWeight(FontWeight.BOLD);
                        	
                        	final Cube cube = new Cube(deletedRegion, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas, bc);
                            
                            canvas2.appendChild(cube.getCube());
                            
                            cube.getCube().addMouseOutHandler(new MouseOutHandler() {

                                @Override
                                public void onMouseOut(MouseOutEvent event) {
                                	p.hide();
                                }

                            });
                            
                            cube.getCube().addMouseOverHandler(new MouseOverHandler() {

                                @Override
                                public void onMouseOver(MouseOverEvent event) {
                                	
                                	p.setPopupPosition(event.getClientX(), event.getClientY()-30);
	                                p.show();
                                }
                            });
                        }
                	}
                }
        	};

    		try {
        		callForBDInfoService.getDeletedRegion(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

        	} catch (Exception e1) {
        		Window.alert("ERROR callForDBInfoService.getExpression : " + e1.getMessage());
        	}
     
        return true;
    }
    
    
    /** Get Rho **/
    public boolean getComponentCurveRho(String normalization) {

    	final ArrayList<Integer> abs = new ArrayList<Integer>();
    	abs.add(60);
    	if ( Config.abbr == "aeb" ) {
    		abs.add(280); abs.add(500); abs.add(720);
    	}
    	    	
    	for ( int i = 0 ; i < abs.size() ; i++ ) {
    		
	        if ( Genoscapist.appControler.normalization.contains("CustomCDS") == true ) {
	        	        	
	        	double minM = 0; double maxM = 0; double minP = 0; double maxP = 0;

	        	if ( Config.abbr == "aeb" ) {
	        		minM = minP = 4.9;
	        		maxM = maxP = 16.3;
	        	}
	        	
	        	else if ( Config.abbr == "seb" ) {
	        		minM = minP = 6.8;
	        		maxM = maxP = 16.5;
	        	}
	        	
	        	// Strand -
	        	Abs axeMinM = new Abs(bc, 30, (int) ((60 + abs.get(i)) + 100 - (minM * 5)), widthWithoutMargin);
	        	axeMinM.create((int) ((60 + abs.get(i)) + 100 - (minM * 5)), true);
	        	axeMinM.paint(canvas3, "rgb(81,110,176)", "1", "axeMinM_"+abs.get(i));
	            Abs axeMaxM = new Abs(bc, 30, (int) ((60 + abs.get(i)) + 100 - (maxM * 5)), widthWithoutMargin);
	            axeMaxM.create((int) ((60 + abs.get(i)) + 100 - (maxM * 5)), true);
	            axeMaxM.paint(canvas3, "rgb(81,110,176)", "1", "axeMaxM_"+abs.get(i));
	        	// Strand +
	            Abs axeMinP = new Abs(bc, 30, (int) ((60 + abs.get(i)) - (minP * 5)), widthWithoutMargin);
	            axeMinP.create((int) ((60 + abs.get(i)) - (minP * 5)), true);
	            axeMinP.paint(canvas3, "rgb(81,110,176)", "1", "axeMinP_"+abs.get(i));
	            Abs axeMaxP = new Abs(bc, 30, (int) ((60 + abs.get(i)) - (maxP * 5)), widthWithoutMargin);
	            axeMaxP.create((int) ((60 + abs.get(i)) - (maxP * 5)), true);
	            axeMaxP.paint(canvas3, "rgb(81,110,176)", "1", "axeMaxP_"+abs.get(i));
	        	
	        	// Strand -
	        	Abs axe1 = new Abs(bc, 30, ((60 + abs.get(i)) + 100 - 50), widthWithoutMargin);
	            axe1.create(((60 + abs.get(i)) + 100 - 50), true);
	            axe1.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe1_"+abs.get(i));
	            Abs axe2 = new Abs(bc, 30, ((60 + abs.get(i)) + 100 - 55), widthWithoutMargin);
	            axe2.create(((60 + abs.get(i)) + 100 - 55), true);
	            axe2.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe2_"+abs.get(i));
	            // Strand +
	            Abs axe3 = new Abs(bc, 30, ((60 + abs.get(i)) - 50), widthWithoutMargin);
	            axe3.create(((60 + abs.get(i)) - 50), true);
	            axe3.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe3_"+abs.get(i));
	            Abs axe4 = new Abs(bc, 30, ((60 + abs.get(i)) - 55), widthWithoutMargin);
	            axe4.create(((60 + abs.get(i)) - 55), true);
	            axe4.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe4_"+abs.get(i));
	        }
	        else {
	        	
	        	double minM = 0; double maxM = 0; double minP = 0; double maxP = 0;
	        	
	        	if ( Config.abbr == "aeb" ) {
	        		minM = minP = -4.5;
	        		maxM = maxP = 7.4;
	        	}
	        	else if ( Config.abbr == "seb" ) {
	        		minM = minP = -3.8;
	        		maxM = maxP = 9.5;
	        	}
	        	
	        	// Strand -
	        	Abs axeMinM = new Abs(bc, 30, (int) ((60 + abs.get(i)) + 100 - (minM * 5 + 40)), widthWithoutMargin);
	        	axeMinM.create((int) ((60 + abs.get(i)) + 100 - (minM * 5 + 40)), true);
	        	axeMinM.paint(canvas3, "rgb(81,110,176)", "1", "axeMinM_"+abs.get(i));
	            Abs axeMaxM = new Abs(bc, 30, (int) ((60 + abs.get(i)) + 100 - (maxM * 5 + 40)), widthWithoutMargin);
	            axeMaxM.create((int) ((60 + abs.get(i)) + 100 - (maxM * 5 + 40)), true);
	            axeMaxM.paint(canvas3, "rgb(81,110,176)", "1", "axeMaxM_"+abs.get(i));
	        	// Strand +
	            Abs axeMinP = new Abs(bc, 30, (int) ((60 + abs.get(i)) - (minP * 5 + 40)), widthWithoutMargin);
	            axeMinP.create((int) ((60 + abs.get(i)) - (minP * 5 + 40)), true);
	            axeMinP.paint(canvas3, "rgb(81,110,176)", "1", "axeMinP_"+abs.get(i));
	            Abs axeMaxP = new Abs(bc, 30, (int) ((60 + abs.get(i)) - (maxP * 5 + 40)), widthWithoutMargin);
	            axeMaxP.create((int) ((60 + abs.get(i)) - (maxP * 5 + 40)), true);
	            axeMaxP.paint(canvas3, "rgb(81,110,176)", "1", "axeMaxP_"+abs.get(i));
	        	
	        	// Strand -
	        	Abs axe1 = new Abs(bc, 30, ((60 + abs.get(i)) + 100 - 52), widthWithoutMargin);
	            axe1.create(((60 + abs.get(i)) + 100 - 52), true);
	            axe1.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe1_"+abs.get(i));
	            Abs axe2 = new Abs(bc, 30, ((60 + abs.get(i)) + 100 - 57), widthWithoutMargin);
	            axe2.create(((60 + abs.get(i)) + 100 - 57), true);
	            axe2.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe2_"+abs.get(i));
	            Abs axe3 = new Abs(bc, 30, ((60 + abs.get(i)) + 100 - 40), widthWithoutMargin);
	            axe3.create(((60 + abs.get(i)) + 100 - 40), true);
	            axe3.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe3_"+abs.get(i));
	            // Strand +
	            Abs axe4 = new Abs(bc, 30, ((60 + abs.get(i)) - 52), widthWithoutMargin);
	            axe4.create(((60 + abs.get(i)) - 52), true);
	            axe4.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe4_"+abs.get(i));
	            Abs axe5 = new Abs(bc, 30, ((60 + abs.get(i)) - 57), widthWithoutMargin);
	            axe5.create(((60 + abs.get(i)) - 57), true);
	            axe5.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe5_"+abs.get(i));
	            Abs axe6 = new Abs(bc, 30, ((60 + abs.get(i)) - 40), widthWithoutMargin);
	            axe6.create(((60 + abs.get(i)) - 40), true);
	            axe6.paint(canvas3, "rgb(126,88,53)", "5,5", "rhoaxe6_"+abs.get(i));
	        }
    	}
    	
        final AsyncCallback<List<Experience>> callback1 = new AsyncCallback<List<Experience>>() {
        	
            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("No experience found !!!");
            }

            @Override
            public void onSuccess(List<Experience> result) {
            	
            	Genoscapist.appControler.setListExpRho(result);

            	for ( int i = 0 ; i <= result.size() ; i++ ) {
            		
            		final Experience experience = result.get(i);
            		final int test = i;
            		
            		final AsyncCallback<Expression> callback = new AsyncCallback<Expression>() {

                        @Override
                        public void onFailure(Throwable caught) {
                        	
                        	Window.alert("No expression found for " + experience.getExp() + "!!!");
                        }

                        @Override
                        public void onSuccess(Expression result) {
                        	
                        	if ( result == null ) {
                        		
                        		Window.alert("No expression found for " + experience.getExp() + "!!!");
                        	}
                        	
                        	else {
                        		     		
                                Expression exp = result;
                                String nameExp = experience.getExp();
                                
                                startY = 60;
                                
                                // Default color
                                String color = "rgb(0,0,0)";
                                
                                if ( nameExp.contains("rho_RPMI_expo") == true ) {
                                	color = "rgb(0,139,0)";
                                }
                                else if ( nameExp.contains("wt_RPMI_expo") == true ) {	
//                                	startY += 180;
                                }
                                else if ( nameExp.contains("rho_RPMI_t4")== true ) {
                                	color = "rgb(0,0,255)";
                                	startY += 220;
                                }
                                else if ( nameExp.contains("wt_RPMI_t4") == true ) {
                                	
                                	startY += 220;
                                }
                                else if ( nameExp.contains("rho_TSB_expo") == true ) {
                                	color = "rgb(255,64,64)";
                                	startY += 440;
                                }
                                else if ( nameExp.contains("wt_TSB_expo") == true ) {
                                	startY += 440;
                                }
                                else if ( nameExp.contains("rho_TSB_t4") == true ) {
                                	
                                	color = "rgb(255,255,0)";
                                	startY += 660;
                                }
                                else if ( nameExp.contains("wt_TSB_t4") == true) {
                                	startY += 660;
                                }
                                else if ( nameExp.contains("Drho_") == true ) {
                                	color = "rgb(255,64,64)";
                                }
                                
                                final Curve curve = new Curve(exp.getSens(), exp.getPosition(), exp.getSignal(), from, widthWithoutMargin, startY, currentBasePerLine, canvas3, bc, color, nameExp);

                                final MyPopup pCurve = new MyPopup(experience.getExp());
                                pCurve.getElement().getStyle().setColor(color);
                                
                                curve.getCurve().addMouseOutHandler(new MouseOutHandler() {
        							
        							@Override
        							public void onMouseOut(MouseOutEvent event) {
        								pCurve.hide();
        							}
        						});
                                
                                curve.getCurve().addMouseDownHandler(new MouseDownHandler() {
        							
        							@Override
        							public void onMouseDown(MouseDownEvent event) {
        							}
        						});
                                
                                curve.getCurve().addMouseOverHandler(new MouseOverHandler() {
        							
        							@Override
        							public void onMouseOver(MouseOverEvent event) {
                                      
        								pCurve.setPopupPosition(event.getClientX(), event.getClientY()-40);
        								pCurve.show();
        							}
        						});
                                
                                canvas3.appendChild(curve.getCurve());
                                
                                if ( test == Genoscapist.appControler.getListExpRho().size() -1 ) {
                                	getRegionWithoutDataRho(abs);
                                }
                        	}
                        }
                	};

            		try {
                		callForBDInfoService.getExpression(experience, (int) from, (int) (from + currentBasePerLine - 1), Genoscapist.appControler.getSeqLen(), callback);

                	} catch (Exception e1) {
                		Window.alert("ERROR callForDBInfoService.getExpression : " + e1.getMessage());
                	}
            	}
            }
        };
        
        try {
    		callForBDInfoService.getAllExperienceRho(normalization, callback1);

    	} catch (Exception e1) {
    		Window.alert("ERROR callForDBInfoService.getAllExperienceRho : " + e1.getMessage());
    	}

        return true;
    }
    
    
    /** Get TranstermHP **/
    public boolean getComponentTransterm() {
     
      final AsyncCallback<List<Transterm>> callback = new AsyncCallback<List<Transterm>>() {

          @Override
          public void onFailure(Throwable caught) {
          	
          	Window.alert("No TranstermHP found !!!");
          }
          
          @Override
          public void onSuccess(List<Transterm> result) {
        	  
        	  ArrayList<Transterm> tmp = new ArrayList<Transterm>();
              Genoscapist.appControler.setListTransterm(tmp);
        	  
        	  for (final Transterm term : result) {
        		  
        		  Genoscapist.appControler.listTransterm.add(term);
        		  
        		  // If Multiline
        		  // int start = term.getStart();
        		  // int line = whichLine(start);
        		  int line = 0;
        		  float featStartY = 60;
        		  
        		  final Cube cube = new Cube(term, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas, bc);
                  
                  canvas.appendChild(cube.getCube());
                  
                  cube.getCube().addClickHandler(new ClickHandler() {
          			
          				@Override
          				public void onClick(ClickEvent event) {
          				}
          			});
                  
                  cube.getCube().addMouseOutHandler(new MouseOutHandler() {

                          @Override
                          public void onMouseOut(MouseOutEvent event) {
                          	
                        	  if ( Config.abbr == "aeb" ) {
                        		  canvas.getElementById("name_"+term.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
                        		  canvas.getElementById("name_"+term.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
                        	  }
                          }
                  });

                  cube.getCube().addMouseDownHandler(new MouseDownHandler() {
                  	
                          @Override
                          public void onMouseDown(MouseDownEvent event) {
                          }
                  });
                 
                  cube.getCube().addMouseOverHandler(new MouseOverHandler() {

                      @Override
                      public void onMouseOver(MouseOverEvent event) {
                    	  
                    	  if ( Config.abbr == "aeb" ) {
                    		  
                    		  canvas.getElementById("name_"+term.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, term.getColor());
                        	  canvas.getElementById("name_"+term.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, term.getColor());
                    	  }
                      }
                  });
        	  }
          }
      };
      
      try {
  		callForBDInfoService.getTransterm(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

  	  } catch (Exception e1) {
  		Window.alert("ERROR callForDBInfoService.getTransterm  : " + e1.getMessage());
  	  }

      return true;
    }
    
    
    /** Get Regulators **/
    public boolean getComponentRegulator() {
    	
        final AsyncCallback<List<Regulator>> callback = new AsyncCallback<List<Regulator>>() {

            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("No Regulator found !!!");
            }
            
            @Override
            public void onSuccess(List<Regulator> result) {
            	
            	ArrayList<Regulator> tmp = new ArrayList<Regulator>();
                Genoscapist.appControler.setListRegulator(tmp);
          	  
          	  	for (final Regulator regulator : result) {
          		  
          	  		Genoscapist.appControler.listRegulator.add(regulator);
          		  
          	  		// If Multiline
          	  		// int start = regulator.getStart();
          	  		// int line = whichLine(start);
          	  		int line = 0;
          	  		float featStartY = 170;
          		  
          	  		final Cube cube = new Cube(regulator, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas, bc);

          	  		canvas.appendChild(cube.getCube());
                    
          	  		cube.getCube().addClickHandler(new ClickHandler() {
            			
          	  			@Override
            			public void onClick(ClickEvent event) {
            			}
          	  		});
                    
                    cube.getCube().addMouseOutHandler(new MouseOutHandler() {

                    	@Override
                    	public void onMouseOut(MouseOutEvent event) {

                        	if ( Genoscapist.appControler.addFeatureNameActive == false ) {
                  				canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
                          		canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
                  			}
                        }
                    });

                    cube.getCube().addMouseDownHandler(new MouseDownHandler() {
                    	
                    	@Override
                    	public void onMouseDown(MouseDownEvent event) {
                    	}
                    });
                   
                    cube.getCube().addMouseOverHandler(new MouseOverHandler() {

                        @Override
                        public void onMouseOver(MouseOverEvent event) {
                        	
                        	if ( Genoscapist.appControler.addFeatureNameActive == false ) {
                        		
                        		if ( regulator.getColor().compareTo("cyan") == 0 ) {
                    				canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_CYAN_VALUE);
                        			canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_CYAN_VALUE);
                       			}
                       			
                       			else if ( regulator.getColor().compareTo("magenta") == 0 ) {
                       				canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_MAGENTA_VALUE);
                        			canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_MAGENTA_VALUE);
                       			}
                        	}
                        }
                    });
          	  }
            }
        };
        
        try {
    		callForBDInfoService.getRegulator(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

    	} catch (Exception e1) {
    		Window.alert("ERROR callForDBInfoService.getRegulator  : " + e1.getMessage());
    	}

        return true;
    }
    
    
    /** Get Segments **/
    public boolean getComponentSegment() {

        final AsyncCallback<List<Segment>> callback = new AsyncCallback<List<Segment>>() {

            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("No Segment found !!!");
            }
            
            @Override
            public void onSuccess(List<Segment> result) {
            	
            	ArrayList<Segment> tmp = new ArrayList<Segment>();
                Genoscapist.appControler.setListSegment(tmp);
          	  
          	  for (final Segment segment : result) {
          		  
          		  Genoscapist.appControler.listSegment.add(segment);
          		  
          		  // If Multiline
          		  // int start = segment.getStart();          		  
          		  // int line = whichLine(start);
          		  int line = 0;
          		  
          		  float featStartY = 220;
          		            		  
          		  final Cube cube = new Cube(segment, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas, bc);
                    
                  canvas.appendChild(cube.getCube());
                    
                  cube.getCube().addClickHandler(new ClickHandler() {
            			
            			@Override
            			public void onClick(ClickEvent event) {
            			}
            		});
                    
                  cube.getCube().addMouseOutHandler(new MouseOutHandler() {

                  		@Override
                  		public void onMouseOut(MouseOutEvent event) {

                        	if ( Genoscapist.appControler.addFeatureNameActive == false ) {
                            		canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
                        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
                            }
                        }
                  });

                  cube.getCube().addMouseDownHandler(new MouseDownHandler() {
                    	
                  		@Override
                  		public void onMouseDown(MouseDownEvent event) {
                            	
                  			// No name and no link with feature
                        	if ( segment.getName().contains("__") == true ) {
                            
                        		// Search a feature with the same positions
                        		final AsyncCallback<GeneItem> callback = new AsyncCallback<GeneItem>() {

                                	@Override
                                    public void onFailure(Throwable caught) {
                                        	
                                    	Window.alert("No GeneItem found !!!");
                                	}
                                        
                                	@Override
                                  	public void onSuccess(GeneItem result) {
                                    	ContentView.selectGene(result.getName());
                                    }
                            	};
                            	try {
                                	callForBDInfoService.getGeneItem(segment, callback);
                            	} catch (Exception e1) {
                                	Window.alert("ERROR callForDBInfoService.getFeature  : " + e1.getMessage());
                                }
                            }
                            	
                        	// Name and link with a feature
                            else {
                            	ContentView.selectGene(segment.getName());
                            }
                       }

                    });
                   
                  cube.getCube().addMouseOverHandler(new MouseOverHandler() {

                  		@Override
                        public void onMouseOver(MouseOverEvent event) {
                        		if ( segment.getName().contains("_") == false ) {
                        	
                        			if ( Genoscapist.appControler.addFeatureNameActive == false ) {
                        				
                        				if ( segment.getColor().compareTo("darkred") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_DARKRED_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_DARKRED_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("lightblue") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_LIGHTBLUE_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_LIGHTBLUE_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("orange") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_ORANGE_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_ORANGE_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("oldyellow") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_YELLOW_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_YELLOW_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("blue") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BLUE_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BLUE_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("red") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("black") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BLACK_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BLACK_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("brown") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BROWN_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BROWN_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("darkblue") == 0 ) {
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_DARKBLUE_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_DARKBLUE_VALUE);
                        				}
                        				
                        				else if ( segment.getColor().compareTo("green") == 0 ) {
                        					
                        					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_GREEN_VALUE);
                                			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_GREEN_VALUE);
                        				}
                        			}
                        		}
                        }
                    });
          	  }
            }
        };
        
        try {
    		callForBDInfoService.getSegment(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

    	  } catch (Exception e1) {
    		Window.alert("ERROR callForDBInfoService.getSegment  : " + e1.getMessage());
    	  }

        return true;
      }
    
    
    /** Get Rho Region without data **/
    public boolean getRegionWithoutDataRho(final ArrayList<Integer> abs) {
    	
        final AsyncCallback<List<Region>> callback = new AsyncCallback<List<Region>>() {

            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("No Region found !!!");
            }
            
            @Override
            public void onSuccess(List<Region> result) {
          	  
            	if ( result.size() == 0 ) { addBlanckRegionRho(); }
            	
            	int j = 0;
            	
          	  for ( Region region : result ) {
          		  
          		  Genoscapist.appControler.setListRegionWithoutDataRho(result);
          		  
          		  j++;
          		  
          		  int start = region.getStart();          		  
          		  int line = whichLine(start);
          		  
          		  for ( int i = 0 ; i < abs.size() ; i++ ) {
          			  
              		  float featStartY = abs.get(i) - 35;
              		  final Cube cube = new Cube(region, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas3, bc, j, abs.get(i));
                      canvas3.appendChild(cube.getCube());
          		  }
          		  
          		  if ( j == result.size() ) {
          			  
          			addBlanckRegionRho();
              	  }
          	  }
            }
        };
        
        try {
    		callForBDInfoService.getRegion(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

    	  } catch (Exception e1) {
    		Window.alert("ERROR callForDBInfoService.getRegion  : " + e1.getMessage());
    	  }
        
        return true;
    }
    
    
    /** Get Rho Region Up Expressed **/
    public boolean getComponentRegionUpRho() {
    	
      final AsyncCallback<List<RegionUpRho>> callback = new AsyncCallback<List<RegionUpRho>>() {

          @Override
          public void onFailure(Throwable caught) {
          	
          	Window.alert("No RegionUpRho found !!!");
          }
          
          @Override
          public void onSuccess(List<RegionUpRho> result) {
        	  
        	  if ( result.size() == 0 ) {
        		  
        		  // Test
        		  if ( Genoscapist.appControler.getVbarPosition() > -1)	{ createRepere(Genoscapist.appControler.getVbarPosition(), true); }
        		  
        		  ContentView.createSVGImage();
        	  }
        	  
        	  Genoscapist.appControler.setListRegionRho(result);
        	  
        	  int j = 0;
        	  
        	  for (final RegionUpRho region : result) {
        		  
        		  j++;
        		  
        		  // If Multiline
        		  // int start = region.getStart();
        		  // int line = whichLine(start);
        		  int line = 0;
        		  
        		  float featStartY = 0;
        		  
        		  if ( region.getName().contains("RPMI_expo") == true ) 	{ featStartY = 215; }
        		  else if ( region.getName().contains("RPMI_t4") == true )	{ featStartY = 435; }
        		  else if ( region.getName().contains("TSB_expo") == true ) { featStartY = 655; }
        		  else 														{ featStartY = 875; }
        		  
        		  final Cube cube = new Cube(region, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas3, bc);
        		  
        		  final MyPopup p = new MyPopup(region.getName());
        		  p.getElement().getStyle().setColor(region.getColor());
        		  p.getElement().getStyle().setFontWeight(FontWeight.BOLD);
                  
                  canvas3.appendChild(cube.getCube());
                  
                  if ( j == result.size() ) {
                	  
                	  // Test
            		  if ( Genoscapist.appControler.getVbarPosition() > -1)	{ createRepere(Genoscapist.appControler.getVbarPosition(), true); }
            		  
                	  ContentView.createSVGImage();
                  }
        	  }
          }
      };
      
      try {
  		callForBDInfoService.getRegionUpRho(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

  	  } catch (Exception e1) {
  		Window.alert("ERROR callForDBInfoService.getRegionUpRho  : " + e1.getMessage());
  	  }
      
      return true;
    }
    
    /** Get Region without data **/
    public boolean getComponentRegion() {
    	     
    	// If Multiline
    	// int start = region.getStart();
    	// int line = whichLine(start);
		  int line = 0;
		  float featStartY = 30;
       
        final AsyncCallback<List<Region>> callback = new AsyncCallback<List<Region>>() {

            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("No Region found !!!");
            	
            	addBlanckRegion();
            	
            	// Vertical Bar
		      	if ( Genoscapist.appControler.getVbarPosition() > -1)	{ createRepere(Genoscapist.appControler.getVbarPosition(), true); }
            	
              	if ( Genoscapist.appControler.rhoActive == false ) {
              		
              		ContentView.createSVGImage();
              	}
            }
            
            @Override
            public void onSuccess(List<Region> result) {
          	  
            	int i = 0;
            	
          	  for ( Region region : result ) {
          		  
          		  i++;
          		  
          		  final Cube cube = new Cube(region, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas2, bc, i, 0);
                    
          		  canvas2.appendChild(cube.getCube());
          	  }
          	  
          	  addBlanckRegion();
          	  
          	  if ( i == result.size() ) {
          		  
          		// Vertical Bar
			    if ( Genoscapist.appControler.getVbarPosition() > -1)	{ createRepere(Genoscapist.appControler.getVbarPosition(), true); }
            	  
              	if ( Genoscapist.appControler.rhoActive == false ) {
              		
              		ContentView.createSVGImage();
              	}
          	  }
            }
        };
        
        try {
    		callForBDInfoService.getRegion(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

    	  } catch (Exception e1) {
    		Window.alert("ERROR callForDBInfoService.getRegion  : " + e1.getMessage());
    	  }

        return true;
      } 
    
    /** Get Promoters **/
    public boolean getComponentPromoter() {
      	
    	final AsyncCallback<List<Promoter>> callback = new AsyncCallback<List<Promoter>>() {

              @Override
              public void onFailure(Throwable caught) {
              	
              	Window.alert("No Promoter found !!!");
              }
              
              @Override
              public void onSuccess(List<Promoter> result) {
            	  
            	  ArrayList<Promoter> tmp = new ArrayList<Promoter>();
                  Genoscapist.appControler.setListPromoter(tmp);
            	  
            	  for (final Promoter promoter : result) {
            		  
            		  Genoscapist.appControler.listPromoter.add(promoter);
            		  
            		  // If Multiline
            		  // int start = promoter.getStart();
            		  // int line = whichLine(start);
            		  int line = 0;
            		  float featStartY = 320;
            		  
            		  final Flag flag = new Flag(promoter, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas, bc);
                      
                      canvas.appendChild(flag.getFlag());
                      
                      flag.getFlag().addClickHandler(new ClickHandler() {
              			
                    	  @Override
                    	  public void onClick(ClickEvent event) {
                    	  }
                      });
                      
                      flag.getFlag().addMouseOutHandler(new MouseOutHandler() {

                    	  @Override
                    	  public void onMouseOut(MouseOutEvent event) {
                          
                    		  if ( Genoscapist.appControler.addFeatureNameActive == false ) {
                              		canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
                          			canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
                              }
                          }
                      });

                      flag.getFlag().addMouseDownHandler(new MouseDownHandler() {
                      	
                      		@Override
                            public void onMouseDown(MouseDownEvent event) {
                            }
                      });
                     
                      flag.getFlag().addMouseOverHandler(new MouseOverHandler() {

                          	@Override
                          	public void onMouseOver(MouseOverEvent event) {
                        	  
                          		if ( Genoscapist.appControler.addFeatureNameActive == false ) {
                  					canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
                          			canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
                          		}
                          	}
                      });
            	  }
              }
          };
          
          try {
      		callForBDInfoService.getPromoter(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

      	  } catch (Exception e1) {
      		Window.alert("ERROR callForDBInfoService.getPromoter  : " + e1.getMessage());
      	  }

          return true;
        }
    
    
    /** Get Terminators **/
    public boolean getComponentTerminator() {
      	       
        final AsyncCallback<List<Terminator>> callback = new AsyncCallback<List<Terminator>>() {

            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("No Terminator found !!!");
            }
            
            @Override
            public void onSuccess(List<Terminator> result) {
            	
            	ArrayList<Terminator> tmp = new ArrayList<Terminator>();
                Genoscapist.appControler.setListTerminator(tmp);
          	  
          	  	for (final Terminator terminator : result) {
          		  
          	  		Genoscapist.appControler.listTerminator.add(terminator);
          		  
          	  		// If multiline
          	  		// int start = terminator.getStart();
          	  		// int line = whichLine(start);
          	  		int line = 0;
          	  		float featStartY = 320;
          		  
          	  		final Flag flag = new Flag(terminator, from, to, widthWithoutMargin, featStartY, currentBasePerLine, line, canvas, bc);
                    
          	  		canvas.appendChild(flag.getFlag());
                    
          	  		flag.getFlag().addClickHandler(new ClickHandler() {
            			
            			@Override
            			public void onClick(ClickEvent event) {
            			}
            		});
                    
                    flag.getFlag().addMouseOutHandler(new MouseOutHandler() {

                    	@Override
                    	public void onMouseOut(MouseOutEvent event) {
                            	
                        	if ( Genoscapist.appControler.addFeatureNameActive == false ) {
                  				canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
                          		canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
                  			}
                     	}
                    });

                    flag.getFlag().addMouseDownHandler(new MouseDownHandler() {
                    	
                    	@Override
                    	public void onMouseDown(MouseDownEvent event) {
                        }
                    });
                   
                    flag.getFlag().addMouseOverHandler(new MouseOverHandler() {

                        @Override
                        public void onMouseOver(MouseOverEvent event) {
                        	
                        	if ( Genoscapist.appControler.addFeatureNameActive == false ) {
              					canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
                      			canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
              			  }
                        }
                    });
          	  }
            }
        };
        
        try {
    		callForBDInfoService.getTerminator(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

    	  } catch (Exception e1) {
    		Window.alert("ERROR callForDBInfoService.getTerminator  : " + e1.getMessage());
    	  }

        return true;
      }
    
    
    /** Get Axis **/
    public boolean getComponentAxis() {

        final AsyncCallback<List<Axis>> callback = new AsyncCallback<List<Axis>>() {

            @Override
            public void onFailure(Throwable caught) {
            	
            	Window.alert("No Axis found !!!");
            }
            
            @Override
            public void onSuccess(List<Axis> result) {
          	  
          	  for (Axis axis : result) {
          		  
          		  float start = (float) axis.getX1();
          		  float stop = (float) axis.getX2();
          		  
          		  // If multiline
          		  // int line = whichLine(start);
          		  int line = 0;
          		  
          		  float featStartY = 320;
          		  
          		  float finFenetre = (from + (currentBasePerLine * line)) + currentBasePerLine - 1;
          		  float startX = 0;
          		  float stopX = 0;
          		  
          		  // 1st case: start in & stop out
		          if ( stop > finFenetre && start <= finFenetre && start >= from ) {
		        	  stopX = 1140;
		        	  startX = ((start - from) * widthWithoutMargin) / currentBasePerLine;
		          }
          		  // 2nd case: start out & stop in
		          else if ( start < from && stop <= finFenetre && stop >= from) {
		        	  startX = 0;
		        	  stopX = ((stop - from) * widthWithoutMargin)  / currentBasePerLine;
		          }
		          // 3rd case: start & stop out
		          else if ( stop > finFenetre && start < from ) {
		        	  startX = 0;
		        	  stopX = 1140;
		          }
		          // 4th case: start & stop in
		          else {
		        	  startX = ((start - from) * widthWithoutMargin) / currentBasePerLine;
		        	  stopX = ((stop - from) * widthWithoutMargin)  / currentBasePerLine;
		          }
		          
		          // Strand +
          		  if ( axis.getStrand() == 1 ) {
          			  
          			  Abs axe = new Abs(bc, startX+30, featStartY-25-((float)axis.getY1()*50), (stopX-startX+1));
          			  axe.create(featStartY-25-((float)axis.getY1()*50), true); 
          			  String color = null;
          			  if ( axis.getColor().compareTo("red") == 0 ) 			{ color = "rgb(255,0,0)"; }
          			  else if ( axis.getColor().compareTo("green") == 0 )	{ color = "rgb(0,255,0)"; }
            		  axe.paint(canvas, color);
          		  }
          		  // Strand -
          		  else {
          			  
          			  Abs axe = new Abs(bc, startX+30, featStartY+25-((float)axis.getY1()*50), (stopX-startX+1));
          			  axe.create(featStartY+25-((float)axis.getY1()*50), true);
          			  String color = null;
        			  if ( axis.getColor().compareTo("red") == 0 ) 			{ color = "rgb(255,0,0)"; }
        			  else if ( axis.getColor().compareTo("green") == 0 )	{ color = "rgb(0,255,0)"; }
            		  axe.paint(canvas, color);
          		  }          		  
          	  }
            }
        };
        
        try {
    		callForBDInfoService.getAxis(sequenceToShow, from, (from + currentBasePerLine - 1), callback);

    	  } catch (Exception e1) {
    		Window.alert("ERROR callForDBInfoService.getAxis  : " + e1.getMessage());
    	  }
        
        return true;
      }
    
    
    /** Adding first and last block without expression profile **/
    public boolean addBlanckRegion() {
    	
    	float featStartY = 30;
    	
        final Cube cubeFirst = new Cube("first", featStartY, bc);
        canvas2.appendChild(cubeFirst.getCube());
    	  
        final Cube cubeLast = new Cube("last", featStartY, bc);
  	  	canvas2.appendChild(cubeLast.getCube());
  	  	
  	  	return true;
    }
    
    /** Adding first and last block without Rho expression profile **/
    public boolean addBlanckRegionRho() {
    	    	
    	final ArrayList<Integer> abs = new ArrayList<Integer>();
    	abs.add(60);
    	if ( Config.abbr == "aeb" ) {
    		abs.add(280); abs.add(500); abs.add(720);
    	}
    	    	
    	for ( int i = 0 ; i < abs.size() ; i++ ) {
    		
    		float featStartY = abs.get(i) - 30;
        	
            final Cube cubeFirst = new Cube("first_rho", featStartY, bc);
            canvas3.appendChild(cubeFirst.getCube());
        	  
            final Cube cubeLast = new Cube("last_rho", featStartY, bc);
      	  	canvas3.appendChild(cubeLast.getCube());
    	}
    	
    	getComponentRegionUpRho();
    	
    	ContentView.createSVGImage();
    	
    	return true;
    }
    
    /** PopUp Name **/
    class MyPopup extends PopupPanel {
    
    	public MyPopup(String string) {
  		
    		super(true);

    	    HTML contents = new HTML(string);
    	    contents.setWidth("128px");
    	    setWidget(contents);

    	    setStyleName("popups-Popup");
    	}
    }
    
    /** Adding of features names **/
    public void addFeaturesNames() {
    	
    	// CDS names
    	for ( Feature cds : Genoscapist.appControler.listCDS ) {
    		
    		if ( Genoscapist.appControler.getGeneSelected() != null && cds.getId_feat().contains(Genoscapist.appControler.getGeneSelected().getLocusTag()) ) {
    			canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "#8462B1");
    			canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "#8462B1");
    		}
    		else {
    			canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "#1EB7D2");
    			canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "#1EB7D2");
    		}
    	}
    	
    	// Segments names
    	ArrayList<Segment> segtmp = new ArrayList<Segment>();
   		
   		for ( Segment segment : Genoscapist.appControler.listSegment ) {
   			
   			if ( segment.getName().contains("_") == false ) {
   				
   				if ( segment.getColor().compareTo("darkred") == 0 ) {
					
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_DARKRED_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_DARKRED_VALUE);
				}
				
				else if ( segment.getColor().compareTo("lightblue") == 0 ) {
					
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_LIGHTBLUE_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_LIGHTBLUE_VALUE);
				}
				
				else if ( segment.getColor().compareTo("orange") == 0 ) {
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_ORANGE_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_ORANGE_VALUE);
				}
				
				else if ( segment.getColor().compareTo("oldyellow") == 0 ) {
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_YELLOW_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_YELLOW_VALUE);
				}
				
				else if ( segment.getColor().compareTo("blue") == 0 ) {
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BLUE_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BLUE_VALUE);
				}
				
				else if ( segment.getColor().compareTo("red") == 0 ) {
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
				}
				
				else if ( segment.getColor().compareTo("black") == 0 ) {
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BLACK_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BLACK_VALUE);
				}
				
				else if ( segment.getColor().compareTo("brown") == 0 ) {
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_BROWN_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_BROWN_VALUE);
				}
				
				else if ( segment.getColor().compareTo("darkblue") == 0 ) {
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_DARKBLUE_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_DARKBLUE_VALUE);
				}
				
				else if ( segment.getColor().compareTo("green") == 0 ) {
					
					canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_GREEN_VALUE);
        			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_GREEN_VALUE);
				}
   				
   				segtmp.add(segment);
   			}
   		}

   		Genoscapist.appControler.setListSegment(null);
		Genoscapist.appControler.setListSegment(segtmp);
		
		// Regulators names
		for ( Regulator regulator : Genoscapist.appControler.listRegulator ) {
			
			if ( regulator.getColor().compareTo("cyan") == 0 ) {
				
				canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_CYAN_VALUE);
    			canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_CYAN_VALUE);
   			}
   			
   			else if ( regulator.getColor().compareTo("magenta") == 0 ) {
   				
   				canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_MAGENTA_VALUE);
    			canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_MAGENTA_VALUE);
   			}
   		}
		
		// Promoters names
   		for ( Promoter promoter : Genoscapist.appControler.listPromoter ) {
   			
   			canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
			canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
   		}
   		
   		// Terminators names
   		for ( Terminator terminator : Genoscapist.appControler.listTerminator ) {
   			
   			canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, SVGConstants.CSS_RED_VALUE);
			canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, SVGConstants.CSS_RED_VALUE);
   		}
   		
   		ContentView.createSVGImage();
    }
    
    /** Remove Features Names **/
    public void removeFeaturesNames() {
		
    	// CDS names
   		for ( Feature cds : Genoscapist.appControler.listCDS ) {
   			
   			canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
        	canvas.getElementById("name_"+cds.getId_feat()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
   		}
   		
   		// Segments
   		for ( Segment segment : Genoscapist.appControler.listSegment ) {
   			
   			canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
        	canvas.getElementById("name_"+segment.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
   		}
   		
   		// Regulators
   		for ( Regulator regulator : Genoscapist.appControler.listRegulator ) {
   		
   			canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
        	canvas.getElementById("name_"+regulator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
   		}
   		
   		// Promoters
   		for ( Promoter promoter : Genoscapist.appControler.listPromoter ) {
   			
   			canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
        	canvas.getElementById("name_"+promoter.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
   		}
   		
   		// Terminators
   		for ( Terminator terminator : Genoscapist.appControler.listTerminator ) {
   			
   			canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_STROKE_VALUE, "none");
        	canvas.getElementById("name_"+terminator.getName()).getElement().getStyle().setProperty(SVGConstants.CSS_FILL_VALUE, "none");
   		}
	}
    
    public synchronized void addFeatureListener(FeatureListener fl) {
        
    	featureListeners.add(fl);
    }    
}