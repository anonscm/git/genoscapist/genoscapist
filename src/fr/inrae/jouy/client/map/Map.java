/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client.map;

import org.vectomatic.dom.svg.OMSVGDocument;
import org.vectomatic.dom.svg.OMSVGGElement;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.utils.OMSVGParser;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;

import fr.inrae.jouy.client.Config;
import fr.inrae.jouy.client.Genoscapist;
import fr.inrae.jouy.gfx.bio.Rectangle;

public abstract class Map {
	
    // x position for the first graphic element
    public float startX;
    // y position for the first graphic element
    public float startY;
    // Image width
    protected int realWidth = 10;
    // Image height
    protected int realHeight = 10;

    // Image size
    protected Rectangle size;
    // Image position
    protected Rectangle bounds;

    public OMSVGSVGElement canvas;
    public OMSVGSVGElement canvas2;
    public OMSVGSVGElement canvas3;
    protected OMSVGDocument doc;
    protected OMSVGGElement xformGroup;
    
    public Map() {
    }
    
    /**
     * Display zone
     *
     * @param g
     *            rectangle
     **/
    public void setBounds(Rectangle g) {
    	
            setBounds(g.getX(), g.getY(), g.getWidth(), g.getHeight());
    }
    
    /**
     * Display zone
     *
     * @param x
     *            x position
     * @param y
     *            y position
     * @param w
     *            width
     * @param h
     *            height
     **/
    public void setBounds(int x, int y, int w, int h) {
    	
            if (w < 10)	{ w = 10; }
            if (h < 10)	{ h = 10; }

            doc = OMSVGParser.currentDocument();
            canvas = doc.createSVGSVGElement();

            xformGroup = new OMSVGGElement();
            xformGroup.getTransform().getBaseVal().appendItem(canvas.createSVGTransform());

            canvas.getStyle().setWidth(w, Unit.PX);
            canvas.getStyle().setHeight(h, Unit.PX);

            xformGroup.appendChild(canvas);
            
            bounds = new Rectangle(0,0,w,h);
            realWidth = w ;
            realHeight = h ;
    }
    
    /** Display zone **/
    public void setBounds(int x, int y, int w, int h, OMSVGSVGElement canvasN) {

    	if (w < 10)	{ w = 10; }
        if (h < 10)	{ h = 10; }

        doc = OMSVGParser.currentDocument();
        canvasN = doc.createSVGSVGElement();

        xformGroup = new OMSVGGElement();
        xformGroup.getTransform().getBaseVal().appendItem(canvasN.createSVGTransform());

        canvasN.getStyle().setWidth(w, Unit.PX);
        canvasN.getStyle().setHeight(h, Unit.PX);

        xformGroup.appendChild(canvasN);
        
        bounds = new Rectangle(0,0,w,h);
        realWidth = w;
        realHeight = h;
}
    
    public void paint() {

    	setBounds(0, 0, 1200, 400, canvas);
        canvas =  doc.createSVGSVGElement();
        canvas.setViewBox(0f, 0f, bounds.getWidth(), bounds.getHeight());
        canvas.getWidth().getBaseVal().newValueSpecifiedUnits(Unit.PX, bounds.getWidth());
        canvas.getHeight().getBaseVal().newValueSpecifiedUnits(Unit.PX, bounds.getHeight());
        //GWT.log("Size : 300x200/"+bounds.getWidth()+"/"+bounds.getHeight());

        canvas.setId("canvasPaint");
        createMap(canvas, bounds.getWidth(), bounds.getHeight());
    }
    
    public void paintCourbe() {
        
    	if ( Config.abbr == "seb_min" ) {
    		setBounds(0, 0, 1200, 550, canvas2);
    	}
    	else {
    		setBounds(0, 0, 1200, 250, canvas2);
    	}
        canvas2 =  doc.createSVGSVGElement();
        canvas2.setViewBox(0f, 0f, bounds.getWidth(), bounds.getHeight());
        canvas2.getWidth().getBaseVal().newValueSpecifiedUnits(Unit.PX, bounds.getWidth());
        canvas2.getHeight().getBaseVal().newValueSpecifiedUnits(Unit.PX, bounds.getHeight());
        //GWT.log("taille : 300x200/"+bounds.getWidth()+"/"+bounds.getHeight());       
        canvas2.setId("CanvasPaintCourbe");
        
        canvas.addMouseDownHandler(new MouseDownHandler() {
			
			@Override
			public void onMouseDown(MouseDownEvent event) {
					translatePosition(event.getX());
					Genoscapist.appControler.setVbarPosition(event.getX());
			}
		});
    	canvas2.addMouseDownHandler(new MouseDownHandler() {
			
			@Override
			public void onMouseDown(MouseDownEvent event) {
					translatePosition(event.getX());
					Genoscapist.appControler.setVbarPosition(event.getX());
			}
		});
        
        createMapCourbe(canvas2, bounds.getWidth(), bounds.getHeight());
    }
    
    public void paintCourbeRho() {
        
    	int h = 300;    	
    	if ( Config.abbr == "aeb" ) { h = 1000; }
    	setBounds(0, 0, 1200, h, canvas3);
    	
        canvas3 =  doc.createSVGSVGElement();
        canvas3.setViewBox(0f, 0f, bounds.getWidth(), bounds.getHeight());
        canvas3.getWidth().getBaseVal().newValueSpecifiedUnits(Unit.PX, bounds.getWidth());
        canvas3.getHeight().getBaseVal().newValueSpecifiedUnits(Unit.PX, bounds.getHeight());
        GWT.log("taille : 300x200/"+bounds.getWidth()+"/"+bounds.getHeight());       
        canvas3.setId("CanvasPaintCourbe");
        
        canvas3.addMouseDownHandler(new MouseDownHandler() {
			
			@Override
			public void onMouseDown(MouseDownEvent event) {
					translatePosition(event.getX());
					Genoscapist.appControler.setVbarPosition(event.getX());
			}
		});

        createMapCourbeRho(canvas3, bounds.getWidth(), bounds.getHeight());
    }

    public String download() {
        return getGfx().getMarkup();
    }

    abstract void translatePosition(int x);    
    abstract void createMap(OMSVGSVGElement canvas, int width, int height);
    abstract void createMapCourbe(OMSVGSVGElement canvas2, int width, int height);
    abstract void createMapCourbeRho(OMSVGSVGElement canvas, int width, int height);

    public OMSVGSVGElement getGfx() {

        return canvas;
    }
    
    public OMSVGSVGElement getGfx(String canvasName) {

    	if ( canvasName.contains("canvas") == true ) {
    		
    		return canvas;
    	}
    	
    	else {
    		
    		return canvas2;
    	}
    }
}