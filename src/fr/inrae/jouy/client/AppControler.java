/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.client;

import java.util.ArrayList;
import java.util.List;

import fr.inrae.jouy.shared.Cluster;
import fr.inrae.jouy.shared.DeletedRegion;
import fr.inrae.jouy.shared.Experience;
import fr.inrae.jouy.shared.Feature;
import fr.inrae.jouy.shared.GeneItem;
import fr.inrae.jouy.shared.Promoter;
import fr.inrae.jouy.shared.Region;
import fr.inrae.jouy.shared.RegionUpRho;
import fr.inrae.jouy.shared.Regulator;
import fr.inrae.jouy.shared.Segment;
import fr.inrae.jouy.shared.Terminator;
import fr.inrae.jouy.shared.Transterm;

public class AppControler {
	
	/** Sequence size **/
	private int seqLen = -1;
	public int getSeqLen() {
		return seqLen;
	}
	public void setSeqLen(int seqLen) {
		this.seqLen = seqLen;
	}

	/** Vertical Bar Position **/
	private int vbarPosition;
	public int getVbarPosition() {
		return vbarPosition;
	}
	public void setVbarPosition(int vbarPosition) {
		this.vbarPosition = vbarPosition;
	}

	/** Rho regions without data list **/
	private List<Region> listRegionWithoutDataRho;
	public List<Region> getListRegionWithoutDataRho() {
		return listRegionWithoutDataRho;
	}
	public void setListRegionWithoutDataRho(List<Region> listRegionWithoutDataRho) {
		this.listRegionWithoutDataRho = listRegionWithoutDataRho;
	}
	
	/** Rho regions list **/
	private List<RegionUpRho> listRegionRho = null;
	public List<RegionUpRho> getListRegionRho() {
		return listRegionRho;
	}
	public void setListRegionRho(List<RegionUpRho> listRegionRho) {
		this.listRegionRho = listRegionRho;
	}
	
	/** Rho experiences list **/
	private List<Experience> listExpRho;
	public List<Experience> getListExpRho() {
		return listExpRho;
	}
	public void setListExpRho(List<Experience> result) {
		this.listExpRho = result;
	}

	/** SVG **/
	private String svg = null;
	public String getSvg() {
		return svg;
	}
	public void setSvg(String svg) {
		this.svg = svg;
	}

	/** Start position of the genomic view **/
	private int start_view = 1; // 0;
	public int getStart_view() {
		return start_view;
	}
	public void setStart_view(int start_view) {
		this.start_view = start_view;
	}

	/** Genes list **/
    private ArrayList<GeneItem> listAllGeneItem = new ArrayList<GeneItem>();
    public ArrayList<GeneItem> getListAllGeneItem() {
		return listAllGeneItem;
	}
    public void setListAllGeneItem(ArrayList<GeneItem> listAllGeneItem) {
		this.listAllGeneItem = listAllGeneItem;
	}
	
	/** Clusters list **/
	private ArrayList<Cluster> listAllCluster = new ArrayList<Cluster>();
	public ArrayList<Cluster> getListAllCluster() {
		return listAllCluster;
	}
	public void setListAllCluster(ArrayList<Cluster> listAllCluster) {
		this.listAllCluster = listAllCluster;
	}

	/** Selected gene **/
    private GeneItem geneSelected = null;
    public GeneItem getGeneSelected() {
		return geneSelected;
	}
    public void setGeneSelected(GeneItem geneSelected) {
		this.geneSelected = geneSelected;
	}
	
	/** Normalization **/
	public String normalization = null;
	public String getNormalization() {
		return normalization;
	}
	public void setNormalization(String normalization) {
		this.normalization = normalization;
	}
	
	/** Rho **/
	public boolean rhoActive = false;
	public boolean isRhoActive() {
		return rhoActive;
	}
	public void setRhoActive(boolean rhoActive) {
		this.rhoActive = rhoActive;
	}
	
	/** Features names **/
	public boolean addFeatureNameActive = false;
	public boolean isAddFeatureNameActive() {
		return addFeatureNameActive;
	}
	public void setAddFeatureNameActive(boolean addFeatureNameActive) {
		this.addFeatureNameActive = addFeatureNameActive;
	}
	
	/** All experiences list **/
	public List<Experience> listAllExperience = new ArrayList<>();
	public List<Experience> getListAllExperience() {
		return listAllExperience;
	}
	public void setListAllExperience(List<Experience> listAllExperience) {
		this.listAllExperience = listAllExperience;
	}
	
	/** All default experiences list **/
	public List<Experience> listAllDefaultExperience = new ArrayList<>();
	public List<Experience> getListAllDefaultExperience() {
		return listAllDefaultExperience;
	}
	public void setListAllDefaultExperience(List<Experience> listAllDefaultExperience) {
		this.listAllDefaultExperience = listAllDefaultExperience;
	}
	
	/** CDS list **/
	public List<Feature> listCDS = new ArrayList<Feature>();
	public List<Feature> getListCDS() {
		return listCDS;
	}
	public void setListCDS(List<Feature> listCDS) {
		this.listCDS = listCDS;
	}
	
	/** Transterm list **/
	public List<Transterm> listTransterm = new ArrayList<Transterm>();
	public List<Transterm> getListTransterm() {
		return listTransterm;
	}
	public void setListTransterm(List<Transterm> listTransterm) {
		this.listTransterm = listTransterm;
	}
	
	/** Segments list **/
	public List<Segment> listSegment = new ArrayList<Segment>();
	public List<Segment> getListSegment() {
		return listSegment;
	}
	public void setListSegment(List<Segment> listSegment) {
		this.listSegment = listSegment;
	}
	
	/** Regulators list **/
	public List<Regulator> listRegulator = new ArrayList<Regulator>();
	public List<Regulator> getListRegulator() {
		return listRegulator;
	}
	public void setListRegulator(List<Regulator> listRegulator) {
		this.listRegulator = listRegulator;
	}
	
	/** Promotors list **/
	public List<Promoter> listPromoter = new ArrayList<Promoter>();
	public List<Promoter> getListPromoter() {
		return listPromoter;
	}
	public void setListPromoter(List<Promoter> listPromoter) {
		this.listPromoter = listPromoter;
	}
	
	/** Terminators list **/
	public List<Terminator> listTerminator = new ArrayList<Terminator>();
	public List<Terminator> getListTerminator() {
		return listTerminator;
	}
	public void setListTerminator(List<Terminator> listTerminator) {
		this.listTerminator = listTerminator;
	}

	/** Selected experiences **/
	public List<Experience> listAllSelectedExperience = new ArrayList<>();
	public List<Experience> getListAllSelectedExperience() {
		return listAllSelectedExperience;
	}
	public void setListAllSelectedExperience(List<Experience> listAllSelectedExperience) {
		this.listAllSelectedExperience = listAllSelectedExperience;
	}	
	public void addExperienceToAllSelectedExperience(Experience experience) {
		this.listAllSelectedExperience.add(experience);
	}
	
	/** Deleted regions list **/
	public List<DeletedRegion> listAllDeletedRegion = new ArrayList<DeletedRegion>();
	public List<DeletedRegion> getListAllDeletedRegion() {
		return listAllDeletedRegion;
	}
	public void setListAllDeletedRegion(List<DeletedRegion> listAllDeletedRegion) {
		this.listAllDeletedRegion = listAllDeletedRegion;
	}
}