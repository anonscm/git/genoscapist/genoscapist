/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Feature implements IsSerializable {

	protected long start=1;
    protected long stop=150;
    protected String id_feat;
    protected long id=-1;
    protected short sens=1;
    protected String location;
    protected int color;
    private Sequence sequence;
    
    protected String type;
    
    public Feature() {
    	
    }

    public String getType() {
    	return type;
    }
    
    public void setType(String type) {
    	this.type = type;
    }
    
	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getStop() {
		return stop;
	}

	public void setStop(long stop) {
		this.stop = stop;
	}

	public String getId_feat() {
		return id_feat;
	}

	public void setId_feat(String id_feat) {
		this.id_feat = id_feat;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public short getSens() {
		return sens;
	}

	public void setSens(short sens) {
		this.sens = sens;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public Sequence getSequence() {
		return sequence;
	}

	public void setSequence(Sequence sequence) {
		this.sequence = sequence;
	}
    
	public String toString() {
        return "Feature : "+ this.id+" "+this.id_feat+" "+this.start+" "+this.stop;
	}
	
	public long length() {
		return stop-start+1;
	}
}