/** Copyright INRAE contributor(s) : Sandra Dérozier (11 May 2020) **/
/** sandra.derozier@inrae.fr **/
/** This software is a computer program implementing a web-tool whose purpose is to generate high-quality images for interactive visualization of hundreds of quantitative profiles along a reference genome together with various annotations in Java. **/
/** This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". **/
/** As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only  limited liability. **/
/** In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean  that it is complicated to manipulate,  and  that  also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. **/
/** Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. **/

package fr.inrae.jouy.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class GeneItem implements IsSerializable {

	private int geneId = -1;
    private String name = null;
    private String locusTag = null;
    private int start = -1;
    private int end = -1;
    private int strand = 1;
    private String clusterA = null;
    private String clusterB = null;
    private String clusterC = null;
    private String[] highexprcond = null;
    private String[] highexprval = null;
    private String[] lowexpcond = null;
    private String[] lowexpval = null;
    private String[] poscorname = null;
    private String[] poscorval = null;
    private String[] negcorname = null;
    private String[] negcorval = null;
    
    public GeneItem() {
        super();
    }

	public int getGeneId() {
		return geneId;
	}

	public void setGeneId(int geneId) {
		this.geneId = geneId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocusTag() {
		return locusTag;
	}

	public void setLocusTag(String locusTag) {
		this.locusTag = locusTag;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getStrand() {
		return strand;
	}

	public void setStrand(int strand) {
		this.strand = strand;
	}

	public String getClusterA() {
		return clusterA;
	}

	public void setClusterA(String clusterA) {
		this.clusterA = clusterA;
	}

	public String getClusterB() {
		return clusterB;
	}

	public void setClusterB(String clusterB) {
		this.clusterB = clusterB;
	}

	public String getClusterC() {
		return clusterC;
	}

	public void setClusterC(String clusterC) {
		this.clusterC = clusterC;
	}

	public String[] getHighexprcond() {
		return highexprcond;
	}

	public void setHighexprcond(String[] highexprcond) {
		this.highexprcond = highexprcond;
	}

	public String[] getHighexprval() {
		return highexprval;
	}

	public void setHighexprval(String[] highexprval) {
		this.highexprval = highexprval;
	}

	public String[] getLowexpcond() {
		return lowexpcond;
	}

	public void setLowexpcond(String[] lowexpcond) {
		this.lowexpcond = lowexpcond;
	}

	public String[] getLowexpval() {
		return lowexpval;
	}

	public void setLowexpval(String[] lowexpval) {
		this.lowexpval = lowexpval;
	}

	public String[] getPoscorname() {
		return poscorname;
	}

	public void setPoscorname(String[] poscorname) {
		this.poscorname = poscorname;
	}

	public String[] getPoscorval() {
		return poscorval;
	}

	public void setPoscorval(String[] poscorval) {
		this.poscorval = poscorval;
	}

	public String[] getNegcorname() {
		return negcorname;
	}

	public void setNegcorname(String[] negcorname) {
		this.negcorname = negcorname;
	}

	public String[] getNegcorval() {
		return negcorval;
	}

	public void setNegcorval(String[] negcorval) {
		this.negcorval = negcorval;
	}
}